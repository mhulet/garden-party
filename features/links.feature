Feature: Links
  As a member
  In order to share useful links
  I want to be able to submit new links to the community

  Background:
    Given there is an user "Bob" in the system
    And there is an user "Alice" in the system
    And there is one accepted link from "Bob" in the system
    And there is one pending link from "Bob" in the system
    And I access the site

  Scenario: Accessing links as anonymous
    Given I'm an anonymous visitor
    Given I access the public links page
    Then I see 1 link

  Scenario: Accessing links as a member
    Given I'm signed-in as "Alice"
    When I access the public links page
    Then I see 1 link

  Scenario: Accessing links as link author
    Given I'm signed-in as "Bob"
    When I access the public links page
    Then I see 2 links
    And I see 1 link with pending validation

  Scenario: Proposing link
    Given I have an account and i'm logged in
    When I propose a link named "Super resources"
    Then I see the pending link "Super resources" in the list

  Scenario: Updating link
    Given I have an account and i'm logged in
    And I have a link titled "Seeds market"
    When I change the link "Seeds market" to "Local seeds"
    Then I see the link named "Local seeds" in the list

  Scenario: Destroying link
    Given I'm signed-in as "Bob"
    And I have a link titled "Seeds market"
    And I access the public links page
    When I destroy the link "Seeds market"
    Then I don't see the link named "Seeds market" in the list

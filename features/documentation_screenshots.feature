@documentation
Feature: Take screenshots for documentation
  As a developer
  In order to quickly update the documentation
  I want to automate screenshot generation

  Scenario: Sign-up and sign-in
    Given I access the site
    And I take screenshots for the sign up and login page

  Scenario: Map management
    Given I have an account and i'm logged in
    And I take screenshots for the map management page

  Scenario: Garden management
    Given I have an account and i'm logged in
    And I have an uploaded map
    And there is a nice dataset for screenshots
    And I take screenshots for garden management: overview
    And I take screenshots for garden management: map: overview
    And I take screenshots for garden management: map: add resources
    And I take screenshots for garden management: map: draw shapes
    And I take screenshots for garden management: map: layers
    And I take screenshots for garden management: inventory
    And I take screenshots for garden management: tasks

  Scenario: Community resources
    Given I have an account and i'm logged in
    And I have an uploaded map
    And there is a nice dataset for screenshots
    And I take screenshots for community resources: genera
    And I take screenshots for community resources: resources

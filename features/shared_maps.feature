Feature: Shared maps
  As a member
  In order to showcase my garden
  I want to be able to give access to it in a read-only mode

  Scenario: Creating a shared map
    Given I have an account and i'm logged in
    And I create a shared map named "My delightful place"
    When I access the "My delightful place" map
    Then I see a link to share it

  Scenario: Sharing an existing map
    And I have an account and i'm logged in
    Given I have a map named "The peaceful zone"
    When I share the "The peaceful zone" map
    Then I see a link to share it

  Scenario: Displaying a shared map
    Given There is a shared map in the system
    When I access it with the provided link
    Then I see the map

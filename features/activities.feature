# Note: a lot of activity-related features are declared in inventory.feature.
Feature: Activities management
  As an user
  In order to track and plan what I do in my garden
  I want to perform actions on the garden's elements

  Background:
    Given I have an account and i'm logged in
    And I have a map named "My garden"
    And I have a planted tree named "John the tree"
    And I access the inventory page for "My garden"

  Scenario: See thumbnails when attaching pictures
    Given I start to take some notes for "John the tree"
    When I attach two pictures to the activity
    Then I see 2 pictures thumbnails in the form

  Scenario: Remove pictures from attachments
    Given I start to take some notes for "John the tree"
    When I attach two pictures to the activity
    And I remove the first picture
    Then I see 1 picture thumbnail in the form

  Scenario: Display uploaded pictures' thumbnails
    Given I take some notes with two attached pictures
    When I display the history for "John the tree"
    Then the first entry have 2 pictures

  Scenario: Display uploaded pictures
    Given I take some notes with two attached pictures
    When I display the history for "John the tree"
    And I display the first picture
    Then I see the first picture in full screen.

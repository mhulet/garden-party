Feature: Links management
  As an administrator
  In order to have pertinent public content
  I want to manage user submitted links

  Background:
    Given I have an administrator account and I'm logged in
    And there is an user "Alice" in the system
    And there is one accepted link from "Alice" in the system
    And there is a pending link titled "My very own link" in the system
    Given I access the site
    When I access the links page for administrators

  Scenario: Listing links
    # Re-do for clear scenario
    Given I access the site
    When I access the links page for administrators
    Then I see 2 links

  Scenario: Creating link
    Given I create a new link from administration
    Then I see 3 links

  Scenario: Updating link
    When I change the link title from "My very own link" to "Something more specific" in administration
    Then I see the link titled "Something more specific" in the list

  Scenario: Destroying link
    When I destroy the link "My very own link" from administration
    Then I don't see the link titled "My very own link" in the list

  Scenario: Approving link
    When I approve the pending link in administration
    Then I don't see any pending links

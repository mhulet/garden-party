When('I access the soil analysis tool') do
  click_on I18n.t('layouts.application.tools')
  click_on I18n.t('tools.index.soil_analysis')
end

Then('I see a page with a form and a picture') do
  expect(page).to have_css 'label', text: I18n.t('js.widgets.soil_analyzer.clay')
  expect(page).to have_css 'text', text: I18n.t('js.widgets.soil_analyzer.clay')
end

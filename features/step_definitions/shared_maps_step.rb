Given('I create a shared map named {string}') do |name|
  visit '/'
  click_on I18n.t('layouts.application.app')
  click_on I18n.t('js.maps.index.new_map')

  click_on I18n.t('js.maps.form.osm_map')
  fill_in I18n.t('activerecord.attributes.map.name'), with: name
  check I18n.t('activerecord.attributes.map.publicly_available')
  map_element = find('#map')
  scroll_to map_element, -200

  map_element.click

  click_on I18n.t('js.maps.form.save_this_map')
end

Then('I see a link to share it') do
  expect(page).to have_link I18n.t('js.layouts.garden.main_menu.display_shared_version')
end

When('I share the {string} map') do |name|
  visit '/'
  click_on I18n.t('layouts.application.app')
  click_on I18n.t('js.layouts.garden.main_menu.maps')

  within '.item.item--has-actions', text: name do
    click_on I18n.t('generic.edit')
  end

  check I18n.t('activerecord.attributes.map.publicly_available')

  click_on I18n.t('generic.save')
end

Given('There is a shared map in the system') do
  @current_map = FactoryBot.create :map, :publicly_available
end

When('I access it with the provided link') do
  visit "/shared/maps/#{@current_map.id}"
end

Then('I see the map') do
  expect(page).to have_content @current_map.name
end

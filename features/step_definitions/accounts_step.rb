Given("I have an account and i'm logged in") do
  user = FactoryBot.create :user

  step "I sign in as \"#{user.email}\""
end

Given("I have an administrator account and I'm logged in") do
  user = FactoryBot.create :user_admin

  step "I sign in as \"#{user.email}\""
end

Given('I sign in as {string}') do |email|
  @signed_in_user = User.find_by email: email

  visit '/'
  click_on I18n.t('layouts.application.sign_in')

  within '.devise-form' do
    fill_in I18n.t('activerecord.attributes.user.email'), with: email
    fill_in I18n.t('activerecord.attributes.user.password'), with: 'password'

    click_on I18n.t('devise.sessions.new.sign_in')
  end
end

Given('there is an user {string} in the system') do |name|
  FactoryBot.create :user, username: name
end

Given('I\'m an anonymous visitor') do
  buttons = find_all 'button', text: I18n.t('layouts.application.sign_out')

  buttons.first.click if buttons.count.positive?
  @signed_in_user = nil
end

Given('I\'m signed-in as {string}') do |name|
  user = User.find_by username: name

  step "I sign in as \"#{user.email}\""
end

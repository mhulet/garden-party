When('I create the {string} genus') do |name|
  click_on I18n.t('js.genera.index.new_genus')

  within '.modal form' do
    fill_in I18n.t('activerecord.attributes.genus.name'), with: name
    fill_in I18n.t('activerecord.attributes.genus.description'), with: 'Some content'
    select I18n.t('js.genera.form.kingdom_options.plant'), from: I18n.t('activerecord.attributes.genus.kingdom')

    click_on I18n.t('generic.save')
  end
end

Then('I see {string} in the library\'s genus list') do |name|
  step 'I order the library by genus'

  within '.library__sidebar__list' do
    expect(current_scope).to have_content name
  end
end

When('I rename the {string} genus to {string}') do |old_name, new_name|
  within '.library__genus-title', text: old_name do
    click_on I18n.t('generic.edit')
  end

  within '.modal form' do
    fill_in I18n.t('activerecord.attributes.genus.name'), with: new_name

    click_on I18n.t('generic.save')
  end
end

Then('I don\'t see the {string} genus anymore') do |name|
  within '.library__sidebar__list' do
    expect(current_scope).not_to have_content name
  end
end

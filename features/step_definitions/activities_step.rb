Given('I start to take some notes for {string}') do |name|
  within '.patch_overview', text: name do
    click_on I18n.t('js.elements.activities.note')
    fill_in I18n.t('activerecord.attributes.activity.notes'), with: 'It speaks!'
  end
end

When('I attach two pictures to the activity') do
  attach_file I18n.t('activerecord.attributes.activity.pictures'), [
    Rails.root.join('spec', 'fixtures', 'files', 'activity_upload.png'),
    Rails.root.join('spec', 'fixtures', 'files', 'map.jpg'),
  ]
end

Then(/^I see (\d+) pictures? thumbnails? in the form$/) do |amount|
  # Well...
  sleep 1
  expect(find_all('.thumbnail').count).to eq amount
end

When('I remove the first picture') do
  within '.thumbnails' do
    find('.thumbnail:first-child button.destructive').click
  end
end

Given('I take some notes with two attached pictures') do
  element = Element.last

  FactoryBot.create :activity, :done, name: 'note', subject: element, pictures: [
    Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'map.jpg'), 'image/jpeg'),
    Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'activity_upload.png'), 'image/png'),
  ]
end

When('I display the history for {string}') do |name|
  within '.patch_overview', text: name do
    click_on I18n.t('js.elements.action_buttons.show_activity')
  end
end

Then('the first entry have {int} pictures') do |amount|
  within('tr:first-child td:last-child') do
    expect(current_scope.text).to eq amount.to_s
  end
end

When('I display the first picture') do
  find('tr:first-child td:last-child button').click
  find('.modal .thumbnail:first-child').click
end

Then('I see the first picture in full screen.') do
  expect(page).to have_css '.image_viewer__image'
end

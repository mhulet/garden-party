Given('I have a map named {string}') do |name|
  @current_map = FactoryBot.create :map, name: name, user: @signed_in_user
end

Given('there is a resource named {string}') do |name|
  FactoryBot.create :resource, name: name
end

Given('there is a genus named {string}') do |name|
  FactoryBot.create :genus, name: name
end

Given('I access the {string} map') do |name|
  visit '/'
  click_on I18n.t('layouts.application.app')
  click_on I18n.t('js.layouts.garden.main_menu.maps')

  click_on name
end

When('I click on the inventory link') do
  click_on I18n.t('js.layouts.garden.toolbar.resource_overview')
end

Then('I see the inventory page') do
  within '.page' do
    expect(current_scope).to have_content I18n.t('js.patches.index.alone_resources')
  end
end

Given('I have a tree named {string}') do |name|
  FactoryBot.create :patch, :point, name: name, map: @current_map
end

Given('I access the inventory page for {string}') do |string|
  step "I access the \"#{string}\" map"
  step 'I click on the inventory link'
end

Then('I see {string} as a point patch') do |name|
  within('section', text: I18n.t('js.patches.index.alone_resources')) do
    expect(current_scope).to have_content name
  end
end

Given('I have a patch named {string}') do |name|
  FactoryBot.create :patch, :polygon, name: name, map: @current_map
end

Given('I have a patch named {string} with some {string} in it') do |patch_name, resource_name|
  resource = FactoryBot.create :resource, name: resource_name
  element  = FactoryBot.build :element, resource: resource, patch: nil
  FactoryBot.create :patch, :polygon, name: patch_name, map: @current_map, elements: [element]
end

Then('I see {string} as a shaped patch') do |name|
  within('section', text: I18n.t('js.patches.index.patches')) do
    expect(current_scope).to have_content name
  end
end

Then('I see that {string} is unplanted in the point patches list') do |name|
  within 'section', text: I18n.t('js.patches.index.alone_resources') do
    within '.patch_overview', text: name do
      expect(current_scope).to have_button I18n.t('js.elements.action_buttons.implant')
    end
  end
end

Then('I see that {string} is unplanted in the {string} elements list') do |resource_name, patch_name|
  within '.patch_overview', text: patch_name do
    within '.element-item', text: resource_name do
      expect(current_scope).to have_button I18n.t('js.elements.action_buttons.implant')
    end
  end
end

When('I add some {string} in the {string}') do |resource_name, patch_name|
  within '.patch_overview', text: patch_name do
    click_on I18n.t('js.patch.item.add_resource')
  end

  click_on resource_name
end

When('I place {string} now') do |name|
  within '.patch_overview', text: name do
    click_on I18n.t('js.elements.action_buttons.implant')

    click_on I18n.t('generic.save')
  end
end

Then('I see that {string} has available actions') do |name|
  within '.patch_overview', text: name do
    expect(current_scope).to have_button I18n.t('js.elements.activities.water')
  end
end

When('I plan to place {string} tomorrow') do |name|
  within '.patch_overview', text: name do
    click_on I18n.t('js.elements.action_buttons.implant')
    find_all('button.datetime_picker__day--today~button').first.click

    click_on I18n.t('generic.save')
  end
end

Then('I see that {string} should be planted tomorrow') do |name|
  within '.patch_overview', text: name do
    # Disabling Rails/Date as we need the date in the timezone of "the client"
    expect(current_scope).to have_content I18n.t('js.elements.action_buttons.waiting_for_implantation_on', date: I18n.l(Date.today + 1.day, format: '%-e %b %Y')) # rubocop:disable Rails/Date
  end
end

Given('I have a tree named {string} which I should implant tomorrow') do |name|
  patch = FactoryBot.create(:patch, :point, name: name, map: @current_map)
  patch.elements.first.update implantation_planned_for: Time.current.beginning_of_day + 1.day
end

When('I validate the implantation of {string}') do |name|
  within '.patch_overview', text: name do
    click_on I18n.t('js.views.elements.item_content.implant_now')
  end
end

Given('I have a planted tree named {string}') do |name|
  patch = FactoryBot.create :patch, :point, name: name, map: @current_map
  patch.elements.first.update(implanted_at: Time.current.beginning_of_day - 1.day)
end

When('I remove the {string} now') do |name|
  within '.patch_overview', text: name do
    click_on I18n.t('js.elements.action_buttons.action.remove')
    click_on I18n.t('generic.save')
  end
end

Given('I have a tree named {string} which I should remove tomorrow') do |name|
  patch = FactoryBot.create(:patch, :point, name: name, map: @current_map)
  patch.elements.first.update implanted_at: Time.current.beginning_of_day - 1.day, removal_planned_for: Time.current.beginning_of_day + 1.day
end

When('I validate the removal of {string}') do |name|
  within '.patch_overview', text: name do
    click_on I18n.t('js.views.elements.item_content.remove_now')
  end
end

Then("I don't see the {string} anymore") do |name|
  within '.patch_overview', text: name do
    # Disabling Rails/Date as we need the date in the timezone of "the client"
    expect(current_scope).to have_content I18n.t('js.elements.action_buttons.removed_at', date: I18n.l(Date.today, format: '%-e %b %Y')) # rubocop:disable Rails/Date
  end
end

When(/^I planned to water "(.*)" (yesterday|today|tomorrow)$/) do |patch_name, day|
  # Disabling Rails/Timezone as we need the beginning of day for the timezone of
  # "the client"
  # rubocop:disable Rails/TimeZone
  date = case day
         when 'yesterday'
           Time.now.beginning_of_day - 1.day
         when 'today'
           Time.now.beginning_of_day
         when 'tomorrow'
           Time.now.beginning_of_day + 1.day
         end
  # rubocop:enable Rails/TimeZone
  crop = Patch.find_by(name: patch_name).elements.first
  Activity.create name: 'water', subject: crop, planned_for: date
end

When('I check the watering state for {string}') do |name|
  within '.patch_overview', text: name do
    click_on I18n.t('js.elements.activities.water')
  end
end

Then('I see that {string} should be watered tomorrow') do |name|
  within '.patch_overview', text: name do
    within 'section', text: I18n.t('activerecord.attributes.activity.planned_for') do
      # Disabling Rails/Date as we need the date in the timezone of "the client"
      expect(current_scope).to have_css('.item', text: I18n.l((Date.today + 1.day), format: '%-e %b %Y')) # rubocop:disable Rails/Date
    end
  end
end
Given(/^I (fertilize|water)e?d the "(.*)" every days for (\d+) days$/) do |action, patch_name, amount|
  crop = Patch.find_by(name: patch_name).elements.first
  amount.times do |t|
    Activity.create name: action, subject: crop, done_at: (Time.current.beginning_of_day - (t + 1).day)
  end
end

Then('I see I watered the {string} the last {int} days') do |name, amount|
  within '.patch_overview', text: name do
    within 'section', text: I18n.t('activerecord.attributes.activity.done_at') do
      expect(find_all('.item.item--small').count).to eq amount
    end
  end
end

When('I see that {string} watering is overdue') do |name|
  within '.patch_overview', text: name do
    within 'section', text: I18n.t('activerecord.attributes.activity.planned_for') do
      expect(current_scope).to have_css '.item .text--danger'
    end
  end
end

When('I finish watering {string}') do |name|
  within '.patch_overview', text: name do
    within 'section', text: I18n.t('activerecord.attributes.activity.planned_for') do
      click_on I18n.t('js.elements.last_action.finish')
    end
  end
end

Then('I see that {string} was watered today') do |name|
  within '.patch_overview', text: name do
    within 'section', text: I18n.t('activerecord.attributes.activity.done_at') do
      # Disabling Rails/Date as we need the date in the timezone of "the client"
      expect(current_scope).to have_content I18n.l(Date.today, format: '%-e %b %Y')  # rubocop:disable Rails/Date
    end
  end
end

When('I check the action log for {string}') do |name|
  within '.patch_overview', text: name do
    click_on I18n.t('js.elements.action_buttons.show_activity')
  end
end

Then('I see {int} actions') do |amount|
  within('.modal') do
    expect(find_all('tbody > tr').count).to eq amount
  end
end

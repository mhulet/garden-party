require 'garden_party/tools/seeder'

Given('I take screenshots for the sign up and login page') do
  visit '/'
  inject_dots [
    find('a', text: I18n.t('layouts.application.home')),
    find('a', text: I18n.t('layouts.application.tools')),
    find('a', text: I18n.t('layouts.application.changelog')),
    find('a', text: I18n.t('layouts.application.links')),
    find('a', text: I18n.t('layouts.application.sign_in')),
  ]
  take_and_crop_screenshot 'home_page_signed_out', height: 200

  visit '/users/sign_in'
  inject_dots [
    # Sign in button
    find('input[type="submit"]'),
    find('a', text: I18n.t('devise.shared.links.sign_up')),
  ]
  take_and_crop_screenshot 'sign_in_page', height: 400
end

Given('I have an uploaded map') do
  @current_map = FactoryBot.create :map, :user_submitted_png, user: @signed_in_user
end

Given('I take screenshots for the map management page') do
  visit '/'

  inject_dots [
    find('a', text: I18n.t('layouts.application.app')),
    # Sign out button
    find('input[type="submit"]'),
  ]
  take_and_crop_screenshot 'home_page_signed_in', height: 200

  visit '/app'
  # Wait for page to be ready
  find_all 'h1', text: I18n.t('js.maps.index.title')
  take_and_crop_screenshot 'map_management_empty', height: 400

  click_on I18n.t('js.maps.index.new_map')
  inject_dots [
    find('input[type="checkbox"]'),
    find('input[type="file"]'),
  ]
  take_and_crop_screenshot 'map_management_new_map', height: 600
  remove_injected_elements

  attach_file I18n.t('js.maps.form.select_picture'), Rails.root.join('spec', 'fixtures', 'files', 'map.svg')

  fill_in I18n.t('activerecord.attributes.map.name'), with: 'Super carte'
  inject_dots [
    find('input[type="number"]'),
    find('button', text: I18n.t('generic.continue')),
  ]
  take_and_crop_screenshot 'map_management_new_map_scale', height: 600

  click_on text: I18n.t('generic.continue')
  remove_injected_elements

  # Wait for map to be ready
  find_all 'button', text: I18n.t('js.maps.form.save_this_map')
  inject_circles [find('canvas')]
  inject_dots [find('button[type="submit"]')]
  take_and_crop_screenshot 'map_management_new_map_center', height: 900
end

Given('there is a nice dataset for screenshots') do
  @current_map.update name: 'Lórien', publicly_available: true

  GardenParty::Tools::Seeder.apply Rails.root.join('db', 'seeds.yml')

  # Patch with multiple elements
  elements = [
    # Implanted
    FactoryBot.build(:element, resource: Resource.order('RANDOM()').first, patch: nil, implanted_at: (Time.zone.now - 1.year)),
    # Removed
    FactoryBot.build(:element, resource: Resource.order('RANDOM()').first, patch: nil, implanted_at: (Time.zone.now - 1.year), removed_at: (Time.zone.now - 2.hours)),
    # Removal planned
    FactoryBot.build(:element, resource: Resource.order('RANDOM()').first, patch: nil, implanted_at: (Time.zone.now - 2.weeks), removal_planned_for: (Time.zone.now + 2.days)),
  ]
  FactoryBot.create(:patch, :polygon, map: @current_map, elements: elements)
  # Overdue activity
  FactoryBot.create :activity, :planned, planned_for: (Time.zone.now - 10.days), name: 'trim', subject: elements.first

  # Unique, implanted patch
  element = FactoryBot.build(:element, resource: Resource.order('RANDOM()').first, patch: nil, implanted_at: (Time.zone.now - 4.days))
  FactoryBot.create(:patch, :point, map: @current_map, elements: [element])
  # Planned activity
  FactoryBot.create :activity, :planned, planned_for: (Time.zone.now + 2.days), name: 'water', subject: element

  # Unique, non implanted patch
  element = FactoryBot.build(:element, resource: Resource.order('RANDOM()').first, patch: nil, implantation_planned_for: (Time.zone.now + 2.hours))
  FactoryBot.create(:patch, :point, map: @current_map, elements: [element])

  # Unique, removed patch
  element = FactoryBot.build(:element, resource: Resource.order('RANDOM()').first, patch: nil, implanted_at: (Time.zone.now - 4.years), removed_at: (Time.zone.now - 1.year))
  FactoryBot.create(:patch, :point, map: @current_map, elements: [element])
end

Given('I take screenshots for garden management: overview') do
  step "I access the \"#{@current_map.name}\" map"

  inject_dots [
    find('.button', text: I18n.t('js.layouts.garden.toolbar.map')),
    find('.button', text: I18n.t('js.layouts.garden.toolbar.resource_overview')),
    find('.button', text: I18n.t('js.layouts.garden.toolbar.todo')),
  ]
  take_and_crop_screenshot 'garden_management__overview', height: 150
end

Given('I take screenshots for garden management: map: overview') do
  # We're still on the right page
  remove_injected_elements

  inject_outlined_dots [
    find('.toolbar', text: I18n.t('js.layouts.garden.toolbar.review')),
    find('.toolbar', text: I18n.t('js.layouts.garden.toolbar.add')),
    find('.toolbar', text: I18n.t('js.layouts.garden.toolbar.draw')),
    find('aside.side_panel'),
  ]
  inject_dots [
    find('#map'),
    find(".button[title=\"#{I18n.t('js.layouts.garden.main_menu.display_shared_version')}\"]"),
  ], counter: 5
  take_and_crop_screenshot 'garden_management__map__overview', height: 600
end

Given('I take screenshots for garden management: map: add resources') do
  # We're still on the right page
  remove_injected_elements

  within('.toolbar', text: I18n.t('js.layouts.garden.toolbar.add')) do
    inject_dots [
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_point')}\""),
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_polygon')}\""),
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_circle')}\""),
    ]
  end
  take_and_crop_screenshot 'garden_management__map__add_resources', top: 110, left: 110, width: 260, height: 60
end

Given('I take screenshots for garden management: map: draw shapes') do
  # We're still on the right page
  remove_injected_elements

  within('.toolbar', text: I18n.t('js.layouts.garden.toolbar.draw')) do
    inject_dots [
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_line')}\""),
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_polygon')}\""),
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_circle')}\""),
    ]
  end
  take_and_crop_screenshot 'garden_management__map__draw_shapes', top: 110, left: 205, width: 260, height: 60

  remove_injected_elements
  within('.toolbar', text: I18n.t('js.layouts.garden.toolbar.draw')) do
    click_on I18n.t('js.layouts.garden.side_panel.button_draw_polygon')
  end

  inject_dots [
    find_all('.garden__content__side_panel .dropdown').first,
    find_all('.garden__content__side_panel .dropdown').last,
    find('.garden__content__side_panel .centered'),
  ]
  take_and_crop_screenshot 'garden_management__map__draw_shapes__config', top: 90, left: 0, width: 300, height: 600
end

Given('I take screenshots for garden management: map: layers') do
  # We're still on the right page
  remove_injected_elements

  inject_dots [
    find('aside.side_panel button.block'),
    # Reorder
    find('aside.side_panel .item:first-child .item__actions:first-child button:first-child'),
    # Toggle
    find('aside.side_panel .item:first-child .item__actions:first-child button:nth-child(2)'),
    # Edit
    find('aside.side_panel .item:first-child .item__actions:first-child button:nth-child(3)'),
    # Destroy
    find('aside.side_panel .item:first-child .item__actions:last-child button'),
  ]
  take_and_crop_screenshot 'garden_management__map__layers', height: 400, left: 350
end

Given('I take screenshots for garden management: inventory') do
  remove_injected_elements
  # Wait for content
  find_all 'a.button', text: I18n.t('js.layouts.garden.toolbar.resource_overview')
  click_on I18n.t('js.layouts.garden.toolbar.resource_overview')

  inject_dots [
    find("dd.clickable > button[title=\"#{I18n.t('js.views.elements.item_content.remove_now')}\"]"),
    find("dd.clickable > button[title=\"#{I18n.t('js.views.elements.item_content.implant_now')}\"]"),
    find('.element-item.removed'),
  ]

  take_and_crop_screenshot 'garden_management__inventory', height: 600
end

Given('I take screenshots for garden management: tasks') do
  # We're OK with the state of previous step
  remove_injected_elements
  click_on I18n.t('js.layouts.garden.toolbar.todo')

  inject_dots [
    find('h2.text--danger'),
    find('h2:not(.text--danger)'),
  ]

  take_and_crop_screenshot 'garden_management__tasks', height: 300
end

Given('I take screenshots for community resources: genera') do
  step "I access the \"#{@current_map.name}\" map"
  click_on I18n.t('js.layouts.garden.main_menu.library')

  inject_dots [
    find('.main_menu__links > a', text: I18n.t('js.layouts.garden.main_menu.library')),
    find('button', text: I18n.t('js.genera.index.new_genus')),
  ]
  take_and_crop_screenshot 'community_resources__genera__access_create_form', height: 200

  remove_injected_elements
  click_on I18n.t('js.genera.index.genus')
  # Select first resource available
  find('div:first-child > a.item:nth-child(2)').click
  inject_dots [
    find('.main_menu__links > a', text: I18n.t('js.layouts.garden.main_menu.library')),
    find('button', text: I18n.t('js.genera.index.genus')),
    find('div:first-child > .library__genus-title .actions button'),
    find('button.button--text'),
  ]

  take_and_crop_screenshot 'community_resources__genera__access_edit_form', height: 400
end

Given('I take screenshots for community resources: resources') do
  remove_injected_elements
  click_on I18n.t('js.genera.index.genus')

  inject_dots [
    find('.main_menu__links > a', text: I18n.t('js.layouts.garden.main_menu.library')),
    find('button', text: I18n.t('js.genera.index.new_resource')),
  ]
  take_and_crop_screenshot 'community_resources__resources__access_create_form', height: 200

  remove_injected_elements
  # Select first resource available
  find('div:first-child > a.item:nth-child(2)').click
  inject_dots [find('button', text: I18n.t('generic.edit'))]
  take_and_crop_screenshot 'community_resources__resources__access_edit_form', height: 400
end

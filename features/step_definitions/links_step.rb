Given('there is one accepted link from {string} in the system') do |name|
  user = User.find_by username: name

  FactoryBot.create :link, :approved, user: user
end

Given('there is one pending link from {string} in the system') do |name|
  user = User.find_by username: name

  FactoryBot.create :link, :pending, user: user
end

Given('I access the public links page') do
  click_on I18n.t('layouts.application.links')
end

Then(/^I see (\d+) links? with pending validation$/) do |amount|
  expect(find_all('.link.link--pending').count).to eq amount
end

Then(/^I see (\d+) links?$/) do |amount|
  expect(find_all('.link').count).to eq amount
end

When('I propose a link named {string}') do |title|
  step 'I access the public links page'

  click_on I18n.t('links.index.new')

  fill_in I18n.t('activerecord.attributes.link.title'), with: title
  fill_in I18n.t('activerecord.attributes.link.description'), with: 'What an useful resource!'
  fill_in I18n.t('activerecord.attributes.link.url'), with: Faker::Internet.url

  click_on I18n.t('generic.save')
end

Then('I see the pending link {string} in the list') do |title|
  expect(page).to have_css('.link.link--pending', text: title)
end

Given('I have a link titled {string}') do |title|
  FactoryBot.create :link, title: title, user: @signed_in_user
end

When('I change the link {string} to {string}') do |old_title, new_title|
  step 'I access the public links page'

  within '.link', text: old_title do
    click_on I18n.t('generic.edit')
  end

  fill_in I18n.t('activerecord.attributes.link.title'), with: new_title

  click_on I18n.t('generic.save')
end

Then('I see the link named {string} in the list') do |title|
  expect(page).to have_css '.link', text: title
end

When('I destroy the link {string}') do |title|
  within '.link', text: title do
    click_on I18n.t('generic.destroy')

    accept_browser_alert
  end
end

Then('I don\'t see the link named {string} in the list') do |title|
  expect(page).not_to have_css('.link', text: title)
end

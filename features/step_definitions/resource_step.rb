When('I create the {string} resource') do |name|
  genus = Genus.first

  click_on I18n.t 'js.genera.index.new_resource'

  within '.modal form' do
    fill_in I18n.t('activerecord.attributes.resource.name'), with: name
    select genus.name, from: I18n.t('activerecord.attributes.resource.genus')
    fill_in I18n.t('activerecord.attributes.resource.description'), with: 'Some content'

    click_on I18n.t('generic.save')
  end
end

Then('I see {string} in the library') do |name|
  within '.library__sidebar__list' do
    expect(current_scope).to have_content name
  end
end

When('I rename resource {string} to {string}') do |old_name, new_name|
  within '.library__sidebar__list' do
    click_on old_name
  end

  within '.library__content' do
    click_on I18n.t('generic.edit')
  end

  within '.modal form' do
    fill_in I18n.t('activerecord.attributes.resource.name'), with: new_name

    click_on I18n.t('generic.save')
  end
end

Then('I don\'t see {string} in the library') do |name|
  within '.library__sidebar__list' do
    expect(current_scope).not_to have_content name
  end
end

Given('I access the library page') do
  click_on I18n.t('js.layouts.garden.main_menu.library')
end

Given('I display the library page for {string}') do |resource_name|
  within '.library__sidebar__list' do
    click_on resource_name
  end
end

Given('I order the library by genus') do
  within '.library__toolbar' do
    click_on I18n.t('js.genera.index.genus')
  end
end

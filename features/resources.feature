Feature: Resources management
  As an user
  In order to improve resources for the community
  I want to be able to create some ne ones
  And I want to be able to edit existing ones

  Background:
    Given I have an account and i'm logged in
    And there is a resource named "Tomato"
    And I have a map named "My garden"

  Scenario: Create new resource
    Given I access the "My garden" map
    And I access the library page
    When I create the "Oak" resource
    Then I see "Oak" in the library

  Scenario: Edit existing resource
    Given I access the "My garden" map
    And I access the library page
    When I rename resource "Tomato" to "Red thing"
    Then I see "Red thing" in the library
    And I don't see "Tomato" in the library

Feature: Resources management
  As an user
  In order to improve resources for the community
  I want to be able to create some new genera
  And I want to be able to edit existing ones

  Background:
    Given I have an account and i'm logged in
    And there is a resource named "Tomato"
    And there is a genus named "Solanum"
    And I have a map named "My garden"

  Scenario: Create new genus
    Given I access the "My garden" map
    And I access the library page
    When I create the "Allium" genus
    Then I see "Allium" in the library's genus list

  Scenario: Edit existing genus
    Given I access the "My garden" map
    And I access the library page
    And I order the library by genus
    When I rename the "Solanum" genus to "Something else"
    Then I see "Something else" in the library's genus list
    And I don't see the "Solanum" genus anymore

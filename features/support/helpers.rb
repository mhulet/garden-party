module Helpers
  def accept_browser_alert
    page.driver.browser.switch_to.alert.accept
  end
end

World(Helpers)

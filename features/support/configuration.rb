require 'simplecov'
require 'capybara-screenshot/cucumber'
require 'selenium-webdriver'

browser = ENV['CAPYBARA_DRIVER'] || 'firefox'

Capybara.register_driver(:selenium) do |app|
  if %w[chrome chrome-headless].include? browser
    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    # Chrome 75 defaults to W3C mode, which doesn't specify a way to get log access.
    # We need to fallback to "old" mode
    # FIXME: Remove this when Chrome supports log access
    caps[:chromeOptions] = { w3c: false }
    # More on the logging prefs levels: http://www.tjmaher.com/2018/12/capybara-gauge-6.html
    caps[:logging_prefs] = { browser: 'ALL' }
  else
    # FIXME: Find a way to log JS from Firefox
    caps = Selenium::WebDriver::Remote::Capabilities.firefox
  end

  # Custom Firefox profile to use light theme.
  firefox_profile                           = Selenium::WebDriver::Firefox::Profile.new
  firefox_profile['ui.systemUsesDarkTheme'] = 0

  browser_options = {
    'chrome'           => { browser: :chrome, desired_capabilities: caps },
    'chrome-headless'  => { browser: :chrome, desired_capabilities: caps, options: Selenium::WebDriver::Chrome::Options.new(args: %w[headless no-sandbox]) },
    'firefox'          => { browser: :firefox, desired_capabilities: caps, options: Selenium::WebDriver::Firefox::Options.new(profile: firefox_profile) },
    'firefox-headless' => { browser: :firefox, desired_capabilities: caps, options: Selenium::WebDriver::Firefox::Options.new(args: %w[--headless]) },
  }

  Capybara::Selenium::Driver.new(app, browser_options[browser])
end

Capybara.default_driver    = :selenium
Capybara.javascript_driver = :selenium
Capybara.save_path         = Rails.root.join('tmp', 'capybara_screenshots')

Capybara::Screenshot.prune_strategy = :keep_last_run

# Only log console output when using Chrome
if %w[chrome chrome-headless].include? browser
  AfterStep do |_scenario, _step|
    if page.driver.browser.respond_to? :manage
      logs = page.driver.browser.manage.logs.get(:browser)

      failures = 0

      logs.each do |log|
        failures += 1 if log.level != 'INFO'
        puts "[JS] #{log.message}\n\n"
      end

      raise DriverJSError, "#{failures} JS errors occured" if failures.positive?
    end
  end
end

class DriverJSError < StandardError
end

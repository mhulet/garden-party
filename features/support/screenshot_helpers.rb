module ScreenshotHelpers
  module Constants # rubocop:disable Metrics/ModuleLength
    DOTS_STYLE = <<~CSS.squish
      .documentation-dot{
        background-color: #EF2929;
        border-radius: 0.75rem;
        color: white;
        display: block;
        font-family: monospace;
        font-size: 1rem;
        font-variant: none;
        height: 1.5rem;
        line-height: 1rem;
        padding: 0.3rem 0;
        position: absolute;
        text-align: center;
        text-decoration: none;
        transform: translateX(-33%) translateY(-33%);
        width: 1.5rem;
        z-index:1000;
      }
    CSS

    CIRCLES_STYLE = <<~CSS.squish
      .documentation-circle{
        border: 2px solid #EF2929;
        border-radius: 1rem;
        display: block;
        height: 2rem;
        width: 2rem;
        padding: 0.3rem 0;
        position: absolute;
        transform: translateX(-50%) translateY(-50%);
        z-index:1000;
      }
    CSS

    OUTLINES_STYLE = <<~CSS.squish
      .documentation-outline{
        border: 2px solid #EF2929;
        display: block;
        position: absolute;
        z-index:1000;
      }
    CSS

    COMMON_JAVASCRIPT = <<~JS.squish
      if(!window.test_getElementPosition) {
        window.test_getElementPosition = function(element) {
          var width = element.offsetWidth;
          var height = element.offsetHeight;
          var top = 0, left = 0;
          do {
            top += element.offsetTop  || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
          } while(element);

          return {top: top,left: left, width: width, height: height};
        };
      }
    JS

    DOTS_JAVASCRIPT = <<~JS.squish
      if(!window.test_addDot){
        window.test_addDot = function(target, counter){
          var position = test_getElementPosition(target);
          var element = document.createElement('div');
          element.innerHTML = counter;
          element.classList.add("documentation-dot");
          element.style.top = position.top + 'px';
          element.style.left = position.left + 'px';
          document.body.appendChild(element);
        };

        var style = document.createElement('style');
        style.innerHTML = "#{DOTS_STYLE}";
        document.body.appendChild(style);
      }
    JS

    CIRCLES_JAVASCRIPT = <<~JS.squish
      if(!window.test_addCircle) {
        window.test_addCircle = function(target){
          var position = test_getElementPosition(target);
          var element = document.createElement('div');
          element.classList.add("documentation-circle");
          element.style.top = position.top + (position.height/2) + 'px';
          element.style.left = position.left + (position.width/2) + 'px';
          document.body.appendChild(element);
        };

        var style = document.createElement('style');
        style.innerHTML = "#{CIRCLES_STYLE}";
        document.body.appendChild(style);
      }
    JS

    OUTLINES_JAVASCRIPT = <<~JS.squish
      if(!window.test_addOutline) {
        window.test_addOutline = function(target){
          var position = test_getElementPosition(target);
          var element = document.createElement('div');
          element.classList.add("documentation-outline");
          element.style.top = position.top - 2 + 'px';
          element.style.left = position.left - 2 + 'px';
          element.style.width = position.width + 4 + 'px';
          element.style.height = position.height + 4 + 'px';
          document.body.appendChild(element);
        };

        var style = document.createElement('style');
        style.innerHTML = "#{OUTLINES_STYLE}";
        document.body.appendChild(style);
      }
    JS
  end

  def inject_dots(nodes = [], counter: 1)
    execute_script Constants::COMMON_JAVASCRIPT
    execute_script Constants::DOTS_JAVASCRIPT

    nodes.each do |node|
      node = find(node) if node.is_a? String
      node.execute_script "test_addDot(this, #{counter})"
      counter += 1
    end
  end

  def inject_circles(nodes = [])
    execute_script Constants::COMMON_JAVASCRIPT
    execute_script Constants::CIRCLES_JAVASCRIPT

    nodes.each do |node|
      node = find(node) if node.is_a? String
      node.execute_script 'test_addCircle(this)'
    end
  end

  def inject_outlines(nodes = [])
    execute_script Constants::COMMON_JAVASCRIPT
    execute_script Constants::OUTLINES_JAVASCRIPT

    nodes.each do |node|
      node = find(node) if node.is_a? String
      node.execute_script 'test_addOutline(this)'
    end
  end

  def inject_outlined_dots(nodes = [])
    inject_outlines nodes
    inject_dots nodes
  end

  def remove_injected_elements
    execute_script <<~JS.squish
      document.querySelectorAll('.documentation-dot').forEach(function(element){element.remove()});
      document.querySelectorAll('.documentation-circle').forEach(function(element){element.remove()});
      document.querySelectorAll('.documentation-outline').forEach(function(element){element.remove()});
    JS
  end

  def take_and_crop_screenshot(name, top: 0, left: 0, width: nil, height: nil)
    name += "_#{I18n.locale}"

    screenshot_and_save_page prefix: name, html: false

    path = File.join(Capybara::Screenshot.capybara_tmp_path, "#{name}.png")
    processed = ImageProcessing::MiniMagick.source(path).crop!(left, top, width, height)
    FileUtils.mv processed, path
  end

  def reload_page
    execute_script 'window.location.reload()'
  end
end

World(ScreenshotHelpers)

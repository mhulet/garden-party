Feature: Soil analysis
  As an user
  In order to know my soil better
  I want to be able to have simple information on it

  Scenario: Access the page
    Given I access the site
    When I access the soil analysis tool
    Then I see a page with a form and a picture

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GardenParty
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    config.garden_party = config_for(:garden_party_defaults)
    config.garden_party.deep_merge! config_for(:garden_party_instance) if File.exist? Rails.root.join('config', 'garden_party_instance.yml')

    config.action_mailer.default_url_options = config.garden_party.default_url_options
    config.action_mailer.default_options = config.garden_party.default_email_options

    # Generate JS translations
    config.middleware.use I18n::JS::Middleware
  end
end

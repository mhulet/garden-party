Rails.application.routes.draw do
  devise_for :users

  root to: 'app#home'

  get '/app', to: 'app#app'
  get '/shared/maps/:map_id', to: 'app#shared', as: :shared_map
  get '/changelog', to: 'app#changelog'

  # Tools
  get '/tools', to: 'tools#index'
  get '/tools/soil-analysis', to: 'tools#soil_analysis'

  resources :links, except: [:show]

  namespace :api do
    api_routes_exceptions = [:new, :edit]
    no_index_api_routes_exceptions = [:index, :new, :edit]

    with_options except: api_routes_exceptions do |api_route|
      # Community-related endpoints
      api_route.resources :genera
      api_route.resources :resource_interactions
      api_route.resources :resources do
        api_route.resources :resource_interactions, only: [:index], path: 'interactions'
      end

      # Map-related endpoint, for updates and display
      api_route.resources :activities, except: no_index_api_routes_exceptions do
        get '/pictures/:picture_id', to: 'activities#picture', on: :member, as: :picture
        get '/pictures/:picture_id/thumbnail', to: 'activities#picture_thumbnail', on: :member, as: :picture_thumbnail
      end
      api_route.resources :elements, except: no_index_api_routes_exceptions do
        api_route.resources :activities, only: [:index]
      end
      api_route.resources :layers, except: no_index_api_routes_exceptions
      api_route.resources :patches, except: no_index_api_routes_exceptions do
        api_route.resources :elements, only: [:index]
      end
      api_route.resources :paths, except: no_index_api_routes_exceptions

      # Maps and their own data, for listings
      api_route.resources :maps do
        get :picture, on: :member
        api_route.resources :activities, only: [:index] do
          get '/names', to: 'activities#names', on: :collection
        end
        api_route.resources :elements, only: [:index]
        api_route.resources :layers, only: [:index]
        api_route.resources :patches, only: [:index]
        api_route.resources :paths, only: [:index]
      end
      get 'shared/maps/:id', to: 'maps#shared', as: :shared_map
    end
  end

  namespace :admin do
    resources :genera
    resources :resource_interactions
    resources :resources
    resources :links, except: [:show] do
      put :approve, on: :member
    end
  end
end

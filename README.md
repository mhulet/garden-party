# Readme

- [Online instance](https://garden-party.experimentslabs.com)
- [User documentation](https://garden-party.io)

## Getting started

### Pre-requisites

You will need a server with the following executable/libraries

- `libpq-dev` (needed by `pg` gem, for database access)
- `svgo`, version 1.3.2 (needed by `image_optim` gem, for cleaning
  SVGs). Version 2 is not yet supported by image_optim.

### Database configuration

We use PostgreSQL for this project.

To get started, copy the configuration files from their defaults and
modify them to match your setup.

```sh
cp config/database.default.yml config/database.yml
cp config/garden_party_defaults.yml config/garden_party_instance.yml
```

Prepare the _credentials_ file: run `bundle exec rails credentials:edit`
and complete with content from `config/credentials.default.yml`.
This command generates `config/master.key` and `config/credential.yml.enc`.

**These two files should not be versioned** but must be deployed in
production.

`config/credential.yml.enc` contains secrets as the salt used for
passwords and secure cookies.

Create database and run the migrations and seeds:

```sh
bundle exec rails db:create
bundle exec rails db:migrate
bundle exec rails db:seed
```

Drop the database:

```sh
bundle exec rails db:drop
```

### Useful rake tasks

- `stats:show` - Displays statistics about the instance.

Other tasks are described in their own sections.

## Updates

To update Garden Party, follow the [migration guide](MIGRATION_GUIDE.md).

## Development

### Rake tasks

- `js:generate` - generates js models and VueX modules to use in the front.
- `uml:models` - outputs PlantUML diagram of tables and relations

Other tasks are described in their own sections.

### GeoJSON features

Patches represents areas in the garden; they have a `geometry` attribute
corresponding to the shape and position of the patch (`Point` or
`Polygon`). As we want to represent circles on the map, we add a `radius`
property to a `Point` geoJSON object.

Compared to a full geoJSON `Feature`, we don't support bounding boxes on
any property, so setting one will trigger a validation error.

Additionally, no other property than `radius` are allowed, triggering
validation errors.

### FactoryBot - [site](https://github.com/thoughtbot/factory_bot)

FactoryBot Rails allows to quickly create entities. It's useful in
tests, as creating fixtures can be a real pain.

They are located in `spec/factories`, and are available in Rails
console, RSpec and Cucumber. It's also used for development seeds.

Note that FactoryBot is not available in production.

FactoryBot is configured to create related entities by default, instead
of only building them (`FactoryBot.use_parent_strategy = false`)

#### Check the factories

A rake task can be used to test the ability to run a factory multiple
times:

```sh
rake factory_bot:lint
```

This task is executed in CI

### Faker - [site](https://github.com/faker-ruby/faker)

Faker is used during development to generate fake data. It should be
used in new factories.

### Authentication

We use [Devise](https://github.com/plataformatec/devise) for
authentication.

These FactoryBot factories will help you during the development:

- `user` for a random user
- `user_known` for an user with email "user@example.com"
- `user_admin` for an admin
- `user_admin_known` for an admin with email "admin@example.com"

All created user are created with the `password` password unless
you specify a custom one.

### Authorization

Authorization is managed with
[Pundit](https://github.com/varvet/pundit).

Usage of `authorize xxx` and `policy_scope` are not enforced in
controllers, but in RSpec tests (check `spec/rails_helper.rb`).

### Roles namespaces

Roles have their own controller namespaces (while models have not):

- `app/controllers` is for public controllers
- `app/controllers/admin` is for admins

### Internationalization

Internationalization is made as in "traditional" Rails applications, and
is managed with [i18n-tasks](https://github.com/glebm/i18n-tasks)

`i18n-tasks` helps to check if your translations are up-to-date.

A custom rake task exists to add missing translations, prefixed with
`TRANSLATE_ME` to find them easily, and another one adds the model
attributes in a dummy file, so they can be translated.

```sh
# Check
i18n-tasks health
# Add missing model attributes in `app/i18n/model_attributes.i18n`
rake i18n:add-models-attributes
# Add missing translations
rake i18n:add-missing
# Remove unused translations
i18n-tasks remove-unused
```

Check [config/i18n-tasks.yml](config/i18n-tasks.yml) for configuration.

An RSpec test checks for missing/unused translations, and a CI job tests
for missing model attributes translations.

While the site structure is translated in english and other languages,
the development seeds are in french. It's up to you to provide localized
seeds; maybe a file like `db/seeds.yml` for your locale; we'll sort this
out how it can be integrated.

#### Javascript

Thanks to [i18n-js](http://rubygems.org/gems/i18n-js), scripts can use
an `I18n` global to translate strings.

The only keys exported are `js.*` and `generic.*`. Check
[config/i18n-js.yml](config/i18n-js.yml) for configuration.

### Haml views - [site](http://haml.info/)

This project uses HAML views, except for text emails.

To help having a consistent formatting, `haml_lint` checks files in CI.

To run it, simply execute:

```sh
haml-lint
```

### VueJS

This project uses VueJS.

The VueJS files should be organized this way:

```text
app/javascript/
├── channels/        # Not used yet (ActionCable)
├── helpers/         # JS helpers, unrelated to a particular pack
├── jest_utils/      # Jest configuration and custom loaders
├── packs/           # Application entrypoints to use with "javascript_load_pack"
│   ├── locales/         # Locales, generated during asset compilation
│   ├── vue-app1.js      # App 1
│   ├── vue-app2.js      # ...
│   └── ...
└── vue/             # Vue applications and components
    ├── classes/         # Instantiable classes. Vue/VueX/... dependents
    │   └── models/          # Models, related to Rails models
    ├── helpers/         # Helpers, Vue/VueX/...dependents
    ├── store_modules/   # VueX modules
    └── apps/            # Applications
        └── app1/
            ├── components
            │   └── Posts/       # Type, category...
            │       ├── _Post.vue    # Partials are prefixed with an underscore
            │       ├── Index.vue    # Full pages are not
            │       └── Form.vue     # ...
            ├── mixins/       # Mixins, if any
            ├── App.vue       # Entry point to load in pack
            ├── router.js     # Router, if needed
            └── store.js      # VueX store, if needed
```

### Style

Stylesheets are located in `app/assets/stylesheets/`, as usual. We use
SCSS language.

### Seeds

There are 3 seeds files available in the project:

- `db/seeds_development.rb`
- `db/seeds_production.rb`
- `db/seeds.rb` for shared seeds.

When you seed the database with `rails db:seed`, shared seeds are run
first, then one of the other files is executed, depending on the
environment.

### Continuous integration

CI jobs are configured for Gitlab. Check
`[.gitlab-ci.yml](.gitlab-ci.yml)` to see the list.

### Writing documentation

The
[official documentation repo](https://gitlab.com/experimentslabs/garden-party-docs/)
is on Gitlab. When you create a feature that needs to be documented,
documentation _should_ be updated too.

Creating screenshots is a cumbersome experience, so there is a Cucumber
profile to generate them:

```shell
bundle exec cucumber --profile documentation
```

Screenshots will be saved in `tmp/capybara_screenshots`, with the current
locale appended to the screenshot name, so you'll have to generate them
for every language manually. It still better than doing it by hand...

Other documentation details are specified
[in its own README](https://gitlab.com/experimentslabs/garden-party-docs/-/blob/next/README.md).

## Testing

### Overcommit

[Overcommit](https://github.com/sds/overcommit) is configured (but not
enabled if you don't use it personally) to run Rubocop and RSpec before
every commit.

### Rubocop - [site](https://rubocop.org/)

Rubocop checks for coding standards, allowing us to have consistent
coding style in the project. Configuration is in
[.rubocop.yml](.rubocop.yml).

Enabled plugins:

- [rubocop-performance](https://docs.rubocop.org/projects/performance),
- [rubocop-rails](https://docs.rubocop.org/projects/rails/) for common
  errors in Rails projects
- [rubocop-rspec](https://github.com/rubocop-hq/rubocop-rspec)

Run it with:

```sh
bundle exec rubocop
# To fix automatically what can be fixed:
bundle exec rubocop -a
```

### RSpec - [site](https://github.com/rspec/rspec)

RSpec examples are in `spec/`. Run the suite with:

```sh
bundle exec rspec
```

To debug step by step:
```sh
# Run this once
bundle exec rspec
# Then run this to replay failed examples
bundle exec rspec --only-failures
```

#### Acceptance tests

JSON responses are tested with
[rspec-rails-api](https://gitlab.com/experimentslabs/rspec-rails-api).

This gem is configured to generate swagger documentation in
`public/swagger.json`.

As values are changing on every RSpec runs, the following tasks were
created in order to handle the generated file:

- `rake swagger:normalize` sorts all keys in file, so diffs are easier
  to compare
- `rake swagger:changed` tells you if you need to commit or checkout the
  newly generated file (it removes all examples and compares what remains
  with `HEAD~1`). Run `normalize` before, as array order matters.

#### Shared contexts

As the project uses Devise for authentication, some shared contexts are
available to use in the specs:

- 'with authenticated user'
- 'with authenticated admin'

### Cucumber - [site](https://github.com/cucumber/cucumber-rails)

Cucumber is configured with
[capybara-screenshot](http://github.com/mattheworiordan/capybara-screenshot),
which makes HTML and png screenshots of pages when a step fails. Both HTML
and images screenshots are saved in `tmp/capybara_screenshots`.

By default, Cucumber will use Firefox to run the tests, but this can be
changed with the `CAPYBARA_DRIVER` environment variable:

```sh
# Default with firefox
bundle exec cucumber
# Variants
CAPYBARA_DRIVER=firefox-headless bundle exec cucumber
CAPYBARA_DRIVER=chrome bundle exec cucumber
CAPYBARA_DRIVER=chrome-headless bundle exec cucumber
```

When using Chrome/Chromium, Cucumber steps will fail on Javascript
errors.

The project uses the
[webdrivers](https://github.com/titusfortner/webdrivers) gem, which
manage the browsers respective drivers.

### Code coverage

When using RSpec or Cucumber, code coverage summary is generated in
`coverage/`. Don't hesitate to open it and check by yourself.

### Brakeman - [site](https://brakemanscanner.org/)

Brakeman is a "security scanner" for common mistakes leading to security
issues.

It can be launched with:

```sh
bundle exec brakeman
```


## Develop another client

GardenParty is mainly an API which validates, store data and give it back
(we're kind...). The web frontend has all the logic to display the data on
a map (with OpenLayers) and only has the logic related to the
_graphical interactions_ with the map, and a bit of logic to prevent user
to submit incorrect data (which will be rejected by the server).

You're welcome to develop your own client if you want to, event if it's to
feed the API with incorrect positions, sizes etc...

**If it's the case, let us know so we can add a flag on those maps to
differentiate them from ours, and also list your awesome client in our
docs.**

Anyway, have fun.

### Notes on planning-related dates

There is something to be aware of when using the API about
planning-related dates:

- Planning dates (`implantation_planned_for`, `removal_planned_for`,
  `*_planned_for` in general) represents the day to perform a task.
- Dates are stored as UTC `datetime`s

So, provided dates for these fields _MUST_ be the beginning of the day
for the action. Which means, an
[ISO8601](https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations)
date. I.e.: with a client in UTC+1, for the beginning of 2020/12/09:

- '2021-01-08T23:00:00.000Z' is valid
- '2021-01-09T00:00:00.000+01:00' is valid too
- '2021-01-09T00:00:00.000Z' is _not_

But the backend will accept all of these, not being aware of the actual
client's timezone.

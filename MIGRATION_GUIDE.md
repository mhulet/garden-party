# Garden Party migration guide

## General steps

- Pull the last version, using tags.
- Check if there are new configuration options and override them to your
  needs
- Install dependencies, as for deployment

  ```shell
  bundle install
  yarn install
  ```
- Run the migrations:

  ```shell
  RAILS_ENV=production bundle exec rails db:migrate`
  ```

In production, you have these extra steps to do:

- Export new javascript translations:

  ```shell
  rake i18n:js:export
  ```
- Precompile assets:

  ```shell
  bundle exec rails assets:precompile
  ```
- Restart the server:

  ```shell
  touch tmp/restart
  ```

Don't forget to run this with the appropriate user and the correct Rails
environment.

We use this small script to update our demo instance, feel free to
inspire yourself from it:

```shell
#!/bin/sh
# Updates the Garden Party instance.
#
# Usage:
#    gp-update <branch>
# Example:
#    gp-update origin/master
#    gp-update 0.0.14


set -xe

cd /path/to/instance

sudo -u deploy -H RAILS_ENV=production git fetch --prune
sudo -u deploy -H RAILS_ENV=production git reset --hard $1
sudo -u deploy -H yarn install --ignore-engines --check-files
sudo -u deploy -H RAILS_ENV=production bundle install
sudo -u deploy -H RAILS_ENV=production bundle exec rails db:migrate
sudo -u deploy -H RAILS_ENV=production rake i18n:js:export
sudo -u deploy -H RAILS_ENV=production bundle exec rails assets:precompile
sudo -u deploy -H touch tmp/restart.txt
```

## 0.2.0 to 0.3.0

- Follow the general migration steps

## 0.1.0 to 0.2.0

- Follow the general migration steps
- At the end, run the rake task to re-analyze SVG pictures used as maps
  backgrounds:
  ```shell
  bundle exec rake maps:analyze_svg
  ```
  This will fix picture sizes and allow proper map scaling.

## 0.0.16 to 0.0.17

- Follow the general migration steps
- The layers system changed; resources now have colors. If you prefer
  resources to have distinct colors instead of the one from their old
  layer:
  ```shell
  bundle exec rake resources:reset_colors
  ```
  Or you can only re-generate pictures:
  ```shell
  bundle exec rake resources:generate_pictures
  ```
- You can safely delete `plants` and all numerical directories in
  `public/pictures/resources`

## 0.0.15 to 0.0.16

- Follow the general migration steps, everything should be fine.

## 0.0.14 to 0.0.15

- Configuration files changed; you need to rename
  `config/garden_party.yml` to `config/garden_party_instance.yml`. This
  file is now merged with `config/garden_party_defaults.yml`.

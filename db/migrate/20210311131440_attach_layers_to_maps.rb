class AttachLayersToMaps < ActiveRecord::Migration[6.1]
  BACKUP_FILE = Rails.root.join('db', 'backups', 'attach_layers_to_map_migration_backup.yml')

  def change # rubocop:disable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
    reversible do |dir|
      dir.up do
        old_layers = select_all 'SELECT * FROM layers'

        # Backup data
        old_resources = select_all 'SELECT id, layer_id from resources'
        backups       = {
          layers:    old_layers.to_a,
          resources: old_resources.to_a,
        }
        File.write BACKUP_FILE, <<~YAML
          # This file was created when running "AttachLayersToMaps" migration. You should
          # keep it somewhere in case you have to rollback: it contains destroyed data.
          #
          # When rolling back, put it back in "db/backups" directory so it can be picked up.
          #
          # An additional layer will be created on rollback, in order to fill newly created
          # resources with a layer.
          #{backups.to_yaml}
        YAML

        # Rename old layers table for now
        rename_table :layers, :layers_deprecated

        # Create new layers table
        create_table :layers do |t|
          t.string :name, null: false
          t.boolean :visible_by_default, null: false, default: true
          t.references :map, foreign_key: true, null: false

          t.timestamps
        end
        # Foreign keys
        add_reference :patches, :layer, foreign_key: true, null: true
        add_reference :paths, :layer, foreign_key: true, null: true
        # Add color to resources
        add_column :resources, :color, :string

        # Create layers for existing maps and attach new layers to patches
        maps = select_all 'SELECT * FROM maps'
        maps.each do |map|
          # Hash like {old_id: new_id} for reference
          new_layers_ids = {}

          # Create layers
          old_layers.each do |old_layer|
            layer_bindings = [
              [nil, old_layer['name']],
              [nil, map['id'].to_i],
              [nil, old_layer['created_at']],
              [nil, old_layer['updated_at']],
            ]
            new_layer = exec_insert 'INSERT INTO layers (name, map_id, created_at, updated_at) VALUES ($1,$2,$3,$4)', 'INSERT_LAYER', layer_bindings
            # Add correspondence between old layer id and new layer id
            new_layers_ids[old_layer['id']] = new_layer.first['id'].to_i

            # Set resources color
            exec_update 'UPDATE resources SET color=$1 WHERE layer_id=$2', 'UPDATE_RESOURCES', [[nil, old_layer['color']], [nil, old_layer['id'].to_i]]
          end

          # Attach patches to new layers
          patches = select_all 'SELECT * FROM patches WHERE map_id=$1', 'SELECT_PATCHES', [[nil, map['id'].to_i]]
          patches.each do |patch|
            # Get the first element to find its layer, from its resource...
            element = select_one 'SELECT elements.id, resources.layer_id from elements, resources WHERE elements.resource_id = resources.id AND elements.patch_id=$1', 'SELECT_ELEMENT', [[nil, patch['id']]]
            # Update patch with new layer ID (using correspondence hash). Fallback to first layer if no element found
            new_id = element.nil? ? new_layers_ids[new_layers_ids.keys.first] : new_layers_ids[element['layer_id']]
            exec_update 'UPDATE patches SET layer_id=$1 WHERE id=$2', 'UPDATE_PATCH', [[nil, new_id], [nil, patch['id'].to_i]]
          end

          # Create layer for existing paths
          paths = select_all 'SELECT * FROM paths WHERE map_id=$1', 'SELECT_PATHS', [[nil, map['id'].to_i]]
          next unless paths.count.positive?

          now = Time.current
          layer_bindings = [
            [nil, 'Path layer'],
            [nil, map['id'].to_i],
            [nil, now],
          ]
          new_layer = exec_insert 'INSERT INTO layers (name, map_id, created_at, updated_at) VALUES ($1,$2,$3,$3)', 'INSERT_LAYER', layer_bindings
          exec_update 'UPDATE paths SET layer_id=$1 WHERE map_id=$2', 'UPDATE_PATHS', [[nil, new_layer.first['id'].to_i], [nil, map['id'].to_i]]
        end

        # Harden data integrity
        change_column_null :patches, :layer_id, false
        change_column_null :paths, :layer_id, false
        change_column_null :resources, :color, false

        # Cleanup
        remove_column :resources, :layer_id
        drop_table :layers_deprecated
      end
      dir.down do
        # Read backup if it exists
        backup = YAML.load_file BACKUP_FILE if File.exist? BACKUP_FILE
        backup ||= nil

        remove_column :patches, :layer_id
        remove_column :paths, :layer_id
        remove_column :resources, :color
        drop_table :layers

        create_table :layers do |t|
          t.string :name, null: false
          t.text :description, null: false
          t.string :color, null: false

          t.timestamps
        end

        # Add reference back in resources
        add_reference :resources, :layer, foreign_key: true, null: true

        # Restore backup if any
        # Hash like {new_id: old_id}
        old_ids_references = {}
        if backup
          backup[:layers].each do |layer|
            bindings = [
              [nil, layer['name']],
              [nil, layer['description']],
              [nil, layer['color']],
              [nil, layer['created_at']],
              [nil, layer['updated_at']],
            ]
            new_layer = exec_insert 'INSERT INTO layers (name, description, color, created_at, updated_at) VALUES($1,$2,$3,$4,$5)', 'CREATE_LAYER', bindings
            old_ids_references[new_layer.first['id'].to_i] = layer['id'].to_i
          end
          backup[:resources].each do |resource|
            bindings = [
              [nil, old_ids_references[resource['layer_id'].to_i]],
              [nil, resource['id'].to_i],
            ]
            exec_update 'UPDATE resources SET layer_id=$1 WHERE id=$2 IS NULL', 'UPDATE_RESOURCES', bindings
          end
        end

        # Insert additional layer to fill newly created data
        layer = exec_insert "INSERT INTO layers (name, description, color, created_at, updated_at) VALUES ('Default layer', 'Change this', '0,0,0', $1,$1)", 'INSERT_LAYER', [[nil, Time.current]]
        # Fill remaining resources
        exec_update 'UPDATE resources SET layer_id=$1 WHERE layer_id IS NULL', 'UPDATE_RESOURCES', [[nil, layer.first['id'].to_i]]
      end
    end
  end
end

class AddExtentSizeToMaps < ActiveRecord::Migration[6.1]
  def change
    change_table :maps, bulk: true do |t|
      t.float :extent_width, default: nil
      t.float :extent_height, default: nil
    end

    stored_maps = ActiveStorage::Attachment.where(record_type: 'Map')
    stored_maps.each do |attachment|
      attachment.analyze unless attachment.analyzed?

      metadata = attachment.blob.metadata

      bindings = [
        [nil, metadata['height']],
        [nil, metadata['width']],
        [nil, attachment.record_id],
      ]

      update 'UPDATE maps SET extent_height=$1, extent_width=$2 WHERE id=$3', 'UPDATE', bindings
    end
  end
end

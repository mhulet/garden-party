class AddSourceToTables < ActiveRecord::Migration[6.0]
  def change
    add_column :resources, :source, :string
    add_column :genera, :source, :string
  end
end

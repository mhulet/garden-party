class CreatePaths < ActiveRecord::Migration[6.0]
  def change
    create_table :paths do |t|
      t.string :name, null: false, default: nil
      t.integer :stroke_width, null: false, default: 1
      t.string :stroke_color, null: false, default: '0,0,0'
      t.string :background_color, null: false, default: '0,0,0,0.2'
      t.jsonb :geometry, null: false
      t.references :map, null: false, foreign_key: true

      t.timestamps
    end
  end
end

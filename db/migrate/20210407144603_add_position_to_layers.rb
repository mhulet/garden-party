class AddPositionToLayers < ActiveRecord::Migration[6.1]
  def change
    add_column :layers, :position, :integer, default: nil, after: :name

    maps = select_all 'SELECT id FROM maps'
    maps.each do |map|
      map_layers = select_all 'SELECT * FROM layers WHERE map_id=$1 ORDER BY id ASC', 'SELECT_LAYERS', [[nil, map['id'].to_i]]

      position = 1
      map_layers.each do |layer|
        update 'UPDATE layers SET position=$1 WHERE id=$2', 'UPDATE_LAYER', [[nil, position], [nil, layer['id'].to_i]]
        position += 1
      end
    end

    change_column_null :layers, :position, false
  end
end

class MakeActivitiesPolymorphic < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        add_reference :activities, :subject, polymorphic: true
        execute "UPDATE activities SET subject_id = element_id, subject_type = 'Element'"

        change_table :activities, bulk: true do |t|
          t.change :subject_id, :bigint, null: false
          t.change :subject_type, :string, null: false
          t.remove :element_id
        end
      end

      dir.down do
        add_reference :activities, :element, foreign_key: true

        execute 'UPDATE activities SET element_id = subject_id'

        change_table :activities, bulk: true do |t|
          t.change :element_id, :bigint, null: false
          t.remove :subject_id, :subject_type
        end
      end
    end
  end
end

class AddPubliclyAvailableOnMaps < ActiveRecord::Migration[6.1]
  def change
    add_column :maps, :publicly_available, :boolean, null: false, default: false
  end
end

class CreatePatches < ActiveRecord::Migration[6.0]
  def change
    create_table :patches do |t|
      t.string :name
      t.jsonb  :geometry, null: false, default: nil

      t.references :map, null: false, foreign_key: true

      t.timestamps
    end
  end
end

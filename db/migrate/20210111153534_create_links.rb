class CreateLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :links do |t|
      t.string :title,       null: false, default: nil
      t.string :url,         null: false, default: nil
      t.string :description, null: false, default: nil
      t.references :user, null: false, foreign_key: true
      t.references :approved_by, foreign_key: { to_table: :users }

      t.timestamps
    end
  end
end

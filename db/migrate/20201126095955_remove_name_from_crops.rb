class RemoveNameFromCrops < ActiveRecord::Migration[6.0]
  def change
    remove_column :crops, :name, :string
  end
end

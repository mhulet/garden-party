class CreateResources < ActiveRecord::Migration[6.0]
  def change
    create_table :resources do |t|
      t.string :name,        null: false, default: nil
      t.string :description, null: false, default: nil
      t.boolean :edible, default: false

      t.references :parent, foreign_key: { to_table: :resources }
      t.references :layer, null: false, foreign_key: true

      t.timestamps
    end
  end
end

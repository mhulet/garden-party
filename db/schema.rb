# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_21_120053) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "activities", force: :cascade do |t|
    t.string "name", null: false
    t.text "notes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "done_at"
    t.datetime "planned_for"
    t.string "subject_type", null: false
    t.bigint "subject_id", null: false
    t.index ["subject_type", "subject_id"], name: "index_activities_on_subject_type_and_subject_id"
  end

  create_table "elements", force: :cascade do |t|
    t.datetime "implanted_at"
    t.bigint "resource_id", null: false
    t.bigint "patch_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "removed_at"
    t.datetime "implantation_planned_for"
    t.datetime "removal_planned_for"
    t.index ["patch_id"], name: "index_elements_on_patch_id"
    t.index ["resource_id"], name: "index_elements_on_resource_id"
  end

  create_table "genera", force: :cascade do |t|
    t.string "name", null: false
    t.text "description", null: false
    t.integer "kingdom", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "source"
  end

  create_table "layers", force: :cascade do |t|
    t.string "name", null: false
    t.boolean "visible_by_default", default: true, null: false
    t.bigint "map_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "position", null: false
    t.index ["map_id"], name: "index_layers_on_map_id"
  end

  create_table "links", force: :cascade do |t|
    t.string "title", null: false
    t.string "url", null: false
    t.string "description", null: false
    t.bigint "user_id", null: false
    t.bigint "approved_by_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["approved_by_id"], name: "index_links_on_approved_by_id"
    t.index ["user_id"], name: "index_links_on_user_id"
  end

  create_table "maps", force: :cascade do |t|
    t.string "name", null: false
    t.point "center", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "extent_width"
    t.float "extent_height"
    t.boolean "publicly_available", default: false, null: false
    t.index ["user_id"], name: "index_maps_on_user_id"
  end

  create_table "patches", force: :cascade do |t|
    t.string "name"
    t.jsonb "geometry", null: false
    t.bigint "map_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "layer_id", null: false
    t.index ["layer_id"], name: "index_patches_on_layer_id"
    t.index ["map_id"], name: "index_patches_on_map_id"
  end

  create_table "paths", force: :cascade do |t|
    t.string "name", null: false
    t.integer "stroke_width", default: 1, null: false
    t.string "stroke_color", default: "0,0,0", null: false
    t.string "background_color", default: "0,0,0,0.2", null: false
    t.jsonb "geometry", null: false
    t.bigint "map_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "layer_id", null: false
    t.string "stroke_style_preset"
    t.index ["layer_id"], name: "index_paths_on_layer_id"
    t.index ["map_id"], name: "index_paths_on_map_id"
  end

  create_table "resource_interactions", force: :cascade do |t|
    t.integer "nature", null: false
    t.string "notes"
    t.bigint "subject_id", null: false
    t.bigint "target_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["subject_id", "target_id"], name: "index_resource_interactions_on_subject_id_and_target_id", unique: true
    t.index ["subject_id"], name: "index_resource_interactions_on_subject_id"
    t.index ["target_id"], name: "index_resource_interactions_on_target_id"
  end

  create_table "resources", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", null: false
    t.boolean "edible", default: false
    t.bigint "parent_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "genus_id", null: false
    t.jsonb "common_names", default: [], null: false
    t.string "source"
    t.string "color", null: false
    t.index ["genus_id"], name: "index_resources_on_genus_id"
    t.index ["parent_id"], name: "index_resources_on_parent_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "role", default: "user", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "username", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "elements", "patches"
  add_foreign_key "elements", "resources"
  add_foreign_key "layers", "maps"
  add_foreign_key "links", "users"
  add_foreign_key "links", "users", column: "approved_by_id"
  add_foreign_key "maps", "users"
  add_foreign_key "patches", "layers"
  add_foreign_key "patches", "maps"
  add_foreign_key "paths", "layers"
  add_foreign_key "paths", "maps"
  add_foreign_key "resource_interactions", "resources", column: "subject_id"
  add_foreign_key "resource_interactions", "resources", column: "target_id"
  add_foreign_key "resources", "genera"
  add_foreign_key "resources", "resources", column: "parent_id"
end

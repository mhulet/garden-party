require 'garden_party/tools/seeder'

# Put your development seeds here
FactoryBot.create :user_known
FactoryBot.create :user_admin_known

# NOTE: on models with 'has_many :xxx, class_name: "Yyy"', there is an issue
# with Spring causing the classes to be somehow reloaded. To avoid
# "ActiveRecord::AssociationTypeMismatch", errors during seeding, we always
# recreate the map instance whenever we need one.
FactoryBot.create :map, :user_submitted, name: 'My first map', center: [31.35, 33.65], extent_width: 55.4036223867884, extent_height: 78.36053896771206, user: User.first

GardenParty::Tools::Seeder.apply(File.join(__dir__, 'seeds.yml'))

module GardenParty
  module Tools
    class Seeder
      class << self
        def apply(yaml_file)
          raise "Don't seed in production!" if Rails.env.production?

          @seeds = YAML.load_file yaml_file

          seed_genera
          seed_resources
          seed_parent_resources
        end

        private

        def seed_genera
          @seeds[:genera].each do |genus|
            Genus.create! name:        genus[:name],
                          description: genus[:description],
                          source:      genus[:source],
                          kingdom:     genus[:kingdom]
          end
        end

        def seed_resources
          @seeds[:resources].each do |resource|
            Resource.create! name:         resource[:name],
                             description:  resource[:description],
                             source:       resource[:source],
                             edible:       resource[:edible],
                             common_names: resource[:common_names],
                             genus:        Genus.find_by(name: resource[:genus])
          end
        end

        def seed_parent_resources
          @seeds[:resources].each do |resource|
            next if resource[:parent].blank?

            Resource.find_by(name: resource[:name]).update parent: Resource.find_by(name: resource[:parent])
          end
        end
      end
    end
  end
end

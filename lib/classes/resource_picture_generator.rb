class ResourcePictureGenerator
  OUTPUT_BASE = Rails.root.join('public', 'pictures').freeze

  def initialize(resource)
    @resource         = resource
    @color            = "rgb(#{resource.color})"
    @base_file_name   = "#{resource.picture_name}_#{resource.id}"
    @output_directory = Rails.root.join(OUTPUT_BASE, 'resources')
    @main_picture     = File.join(@output_directory, "#{@base_file_name}.svg")
    @pattern_picture  = File.join(@output_directory, "#{@base_file_name}_pattern.svg")
    FileUtils.mkdir_p @output_directory unless File.directory? @output_directory
  end

  def generate
    create_main_picture
    create_pattern_picture
  end

  def destroy_pictures
    FileUtils.rm @main_picture if File.exist? @main_picture
    FileUtils.rm @pattern_picture if File.exist? @pattern_picture
  end

  private

  def create_main_picture
    data = <<~HTML.squish
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 6.35 6.35">
        <path d="M6.35 3.175A3.175 3.175 0 013.175 6.35 3.175 3.175 0 010 3.175 3.175 3.175 0 013.175 0 3.175 3.175 0 016.35 3.175z" stroke-width="0.1" fill="#{@color}" fill-opacity="0.2" stroke="#{@color}" />
        <text style="line-height:1.25" x="1.161" y="5.007" font-weight="400" font-size="5.325" font-family="sans-serif" stroke-width="0.1" fill="black" fill-opacity="0.5">
          <tspan x="1.161" y="5.000">#{@resource.name.first}</tspan>
        </text>
      </svg>
    HTML
    File.write @main_picture, data
  end

  def create_pattern_picture
    data = <<~HTML.squish
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 6.35 6.35">
        <path d="M0 0h6.35v6.35H0z" stroke="transparent" fill="#{@color}" fill-opacity="0.2" />
        <text style="line-height:1.25" x="1.161" y="5.007" stroke="transparent" font-weight="400" font-size="2.5" font-family="sans-serif" stroke-width="0.1" fill="black" fill-opacity="0.5">
          <tspan x="1.161" y="5.000">#{@resource.name.first}</tspan>
        </text>
      </svg>
    HTML
    File.write @pattern_picture, data
  end
end

class FrontGenerator
  EXCLUDED_MODELS = [
    'ApplicationRecord',
    'Link', # No API for this
    'GeometryRecord',
  ].freeze

  FIELDS_META = YAML.load_file(File.join(__dir__, 'front_fields.yml'))

  TYPES = {
    types:   {
      'Boolean'           => 'boolean',
      'EnumType'          => 'string',
      'Integer'           => 'number',
      'Float'             => 'number',
      'Jsonb'             => 'object',
      'Point'             => '{x:number, y:number}',
      'String'            => 'string',
      'Text'              => 'string',
      'TimeZoneConverter' => 'Date|string',
    },
    setters: {
      'Point'             => ->(field) { "[#{field}.x, #{field}.y]" },
      'TimeZoneConverter' => ->(field) { "#{field} ? new Date(#{field}) : null" },
    },
  }.freeze

  def generate(*types)
    load_models
    load_attributes
    render 'module', File.join('stores', 'modules'), 'Module' if types.include? :modules
    render 'model', File.join('classes', 'models') if types.include? :models
  end

  private

  def load_models
    return @models if @models

    @models = {}
    Dir.glob(Rails.root.join('app', 'models', '*.rb')).each do |filename|
      class_name = File.basename(filename, '.rb').classify
      next if EXCLUDED_MODELS.include? class_name

      @models[class_name] = {}
    end
  end

  def load_attributes
    raise 'Models not loaded' unless @models

    @models.each_key do |class_name|
      complete_attributes class_name
      add_extra_attributes class_name
    end
  end

  def render(template_type, js_output_directory, suffix = '')
    template = File.read(File.join(__dir__, 'front_generator', 'templates', "#{template_type}.js.erb"))
    renderer = ERB.new(template)
    # "attributes" is used in templates
    @models.each do |model_name, attributes|
      File.write Rails.root.join('app', 'javascript', 'vue', js_output_directory, "#{model_name}#{suffix}.js"), renderer.result(binding)
    end
  end

  def attribute_type(attribute, details)
    type = TYPES.dig :types, details[:type]
    type ||= details[:type]
    type = "null|#{type}" if attribute == 'id'

    type
  end

  def attribute_setter(attribute, details)
    setter = TYPES.dig :setters, details[:type]
    return setter.call(attribute) if setter

    attribute
  end

  def complete_attributes(class_name)
    fields_meta = FIELDS_META[class_name] || {}
    class_name.constantize.attribute_types.each do |attribute, type|
      next if fields_meta['excluded']&.include? attribute

      @models[class_name][attribute] = { type: type.class.name.demodulize, limit: type.limit }
    end
  end

  def add_extra_attributes(class_name)
    fields_meta = FIELDS_META[class_name] || {}
    (fields_meta['additional'] || {}).each_pair do |attribute, type|
      @models[class_name][attribute.to_s] = { type: type }
    end
  end
end

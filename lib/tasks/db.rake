def export_layers
  Layer.all.map do |layer|
    {
      name:        layer.name,
      description: layer.description,
      color:       layer.color,
    }
  end
end

def export_genera
  Genus.order(:name).all.map do |genus|
    {
      name:        genus.name,
      kingdom:     genus.kingdom,
      description: genus.description,
      source:      genus.source,
    }
  end
end

def export_resources
  Resource.order(:name).all.map do |resource|
    {
      name:         resource.name,
      description:  resource.description,
      edible:       resource.edible,
      layer:        resource.layer.name,
      genus:        resource.genus.name,
      common_names: resource.common_names,
      parent:       resource.parent&.name,
      source:       resource.source,
    }
  end
end

def export_interactions
  ResourceInteraction.all.map do |interaction|
    {
      subject: interaction.subject.name,
      target:  interaction.target.name,
    }
  end
end

namespace :db do
  desc 'Exports database in YAML (layers, genera, resources and interactions'
  task :export, [:output] => :environment do |_task, args|
    output = ENV['OUTPUT'] || args[:output].presence || Rails.root.join('db', 'seeds.yml')
    seeds  = {
      genera:       export_genera,
      layers:       export_layers,
      resources:    export_resources,
      interactions: export_interactions,
    }
    File.write output, seeds.to_yaml
  end
end

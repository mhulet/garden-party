namespace :stats do
  def red(string)
    "\e[31m#{string}\e[0m"
  end

  def green(string)
    "\e[32m#{string}\e[0m"
  end

  def underline(string, char: '=', level: 0)
    padding = ' ' * (level * 2)
    puts "#{padding}#{string}\n#{padding}#{char * (string.size + 1)}"
  end

  def pad(string, level: 0)
    level += 1
    padding = ' ' * (level * 2)
    puts string.split("\n").map { |line| "#{padding}#{line}" }.join("\n")
  end

  def good_if_greater_than(value, amount)
    value >= amount ? green(value) : red(value)
  end

  def good_if_less_than(value, amount)
    value <= amount ? green(value) : red(value)
  end

  desc 'Display instance statistics'
  task show: :environment do
    now        = Time.current
    last_week  = now - 1.week
    last_month = now - 1.month

    users = User.count
    underline 'Users'
    pad <<~TXT
      - total         : #{users}
      - new (7 days)  : #{good_if_greater_than(User.where('created_at >= ? ', last_week).count, 0)}
      - new (a month) : #{good_if_greater_than(User.where('created_at >= ? ', last_month).count, 0)}
    TXT

    maps = Map.count
    underline 'Gardens'
    pad <<~TXT
      - total         : #{maps}
      - users average : #{good_if_greater_than(maps / users.to_f, 0.8)}
    TXT
    elements                 = Element.count
    elements_planned         = Element.planned.count
    elements_still_implanted = Element.still_implanted.count
    elements_removed         = Element.removed.count
    underline 'Elements', char: '-', level: 1
    pad <<~TXT, level: 1
      - total          : #{elements}
      - garden average : #{elements / maps.to_f}
    TXT
    underline 'Statuses', char: '~', level: 2
    pad <<~TXT, level: 2
      - planned   : #{elements_planned}
      - implanted : #{elements_still_implanted}
      - removed   : #{elements_removed}
    TXT
    underline 'Per garden', char: '~', level: 2
    pad <<~TXT, level: 2
      - planned   : #{elements_planned / maps.to_f}
      - implanted : #{elements_still_implanted / maps.to_f}
      - removed   : #{elements_removed / maps.to_f}
    TXT
    paths = Path.count
    underline 'Paths', char: '-', level: 1
    pad <<~TXT, level: 1
      - total          : #{paths}
      - garden average : #{paths / maps.to_f}
    TXT

    resources = Resource.count
    underline 'Resources'
    pad <<~TXT
      - total           : #{resources}
      - new (7 days)    : #{good_if_greater_than(Resource.where('created_at >= ?', last_week).count, 0)}
      - new (month)     : #{good_if_greater_than(Resource.where('created_at >= ?', last_month).count, 0)}
      - edited (7 days) : #{Resource.where('created_at != updated_at AND updated_at >= ?', last_week).count}
      - edited (month)  : #{Resource.where('created_at != updated_at AND updated_at >= ?', last_month).count}
    TXT

    activities         = Activity.count
    activities_overdue = Activity.where('planned_for <= ?', now).count
    activities_todo    = Activity.where('planned_for >= ?', now).count
    activities_done    = Activity.where.not(done_at: nil).count
    underline 'Activities'
    pad <<~TXT
      - total        : #{activities}
      - new (7 days) : #{Activity.where('created_at >= ?', last_week).count}
      - new (month)  : #{Activity.where('created_at >= ?', last_month).count}
    TXT
    underline 'Statuses', char: '-', level: 1
    pad <<~TXT, level: 1
      - overdue            : #{good_if_less_than(activities_overdue, activities / users.to_f)}
      - todo (not overdue) : #{good_if_greater_than(activities_todo, 0)}
      - done               : #{good_if_greater_than(activities_done, activities_done / users.to_f)}
    TXT
    underline 'Per garden', char: '-', level: 1
    pad <<~TXT, level: 1
      - overdue            : #{activities_overdue / maps.to_f}
      - todo (not overdue) : #{activities_todo / maps.to_f}
      - done               : #{activities_done / maps.to_f}
    TXT
    pictures = ActiveStorage::Attachment.where(record_type: 'Activity').count
    underline 'Pictures', char: '-', level: 1
    pad <<~TXT, level: 1
      - total              : #{pictures}
      - average per action : #{pictures / activities.to_f}
    TXT
  end
end

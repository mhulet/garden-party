require 'classes/front_generator'

namespace :js do
  desc 'Generate javascript models and VueX modules'
  task generate: [:generate_models, :generate_modules] do
    generator = FrontGenerator.new
    generator.generate(:models, :modules)
  end

  desc 'Generate javascript models'
  task generate_models: :environment do
    generator = FrontGenerator.new
    generator.generate(:models)
  end

  desc 'Generate javascript VueX modules'
  task generate_modules: :environment do
    generator = FrontGenerator.new
    generator.generate(:modules)
  end
end

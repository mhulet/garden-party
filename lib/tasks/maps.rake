require 'garden_party/colors'

namespace :maps do
  desc 'Re-analyzes SVG pictures and fixes size issues leading to incorrect scales'
  task analyze_svg: :environment do
    Map.find_each do |map|
      next unless map.picture.attached? && map.picture.blob.content_type == 'image/svg+xml'

      map.picture.analyze
    end
  end
end

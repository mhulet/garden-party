require 'nokogiri'

CONFIGURATION_FILE = Rails.root.join('config', 'icons.yml')
CONFIG             = YAML.load_file CONFIGURATION_FILE
SVG_STRING         = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  width="0" height="0" style="display:none;"/>'.freeze

namespace :icons do
  desc 'Generate icon font defined in config/icons.yml'
  task generate: :environment do
    # Prepare definition file content
    svg_definitions    = Nokogiri::XML.fragment SVG_STRING
    xml_namespace      = svg_definitions.children.first.namespace.href
    defs_node          = svg_definitions.xpath('./ns:svg', ns: xml_namespace).first
    defs_node['class'] = 'icons-definition'

    CONFIG['icons'].each do |name, path|
      puts "Processing #{name}"
      source_path = icon_path path

      svg = Nokogiri::XML.fragment File.read(source_path)
      svg_viewbox = svg.xpath('./ns:svg', ns: xml_namespace).attribute('viewBox')&.value
      svg_content = svg.xpath('./ns:svg/*', ns: xml_namespace)
      defs_node.add_child create_icon_def(name, svg_content, svg_viewbox)
    end

    File.write(Rails.root.join(CONFIG['output']), svg_definitions)
  end

  def icon_path(icon)
    chunks = icon.split(':')
    path = Rails.root.join CONFIG['paths'][chunks.first], "#{chunks.last}.svg"
    raise "Missing icon at #{path}" unless File.exist? path

    path
  end

  def create_icon_def(name, content, svg_viewbox)
    unless svg_viewbox
      svg_viewbox = CONFIG['viewbox']
      puts "No viewbox provided for #{name}. Using '#{svg_viewbox}'"
    end

    id       = "#{CONFIG['prefix']}#{name}"
    fragment = Nokogiri::XML.fragment "<symbol id='#{id}' viewBox='#{svg_viewbox}'/>"
    fragment.xpath('./symbol').first.add_child content
    fragment
  end
end

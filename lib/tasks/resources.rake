require 'garden_party/colors'
namespace :resources do
  desc 'Re-generate resources pictures.'
  task generate_pictures: :environment do
    Resource.all.each(&:generate_pictures)
  end

  desc 'Re-set colors.'
  task reset_colors: [:generate_pictures] do
    Resource.all.each { |resource| resource.update color: (GardenParty::Colors.from_string resource.name) }
  end
end

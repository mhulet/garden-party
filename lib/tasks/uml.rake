namespace :uml do
  desc 'Outputs PlantUML diagram of tables and relations'
  task models: :environment do
    system 'rails runner lib/runners/generate_table_associations.rb'
  end
end

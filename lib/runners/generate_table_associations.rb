EXCLUDED_MODELS = %w[
  ApplicationRecord
  GeometryRecord
].freeze
EXCLUDED_TABLES = [
  /^versions$/,
  /_blobs$/,
  /_attachments$/,
].freeze

RAILS_ATTRIBUTES = %w[
  created_at
  updated_at
  id
].freeze

ASSOCIATION_ATTRIBUTES = [
  /_id$/,
  /_type$/,
].freeze

tables    = {}
relations = []

def determine_attribute_section(attribute)
  if ASSOCIATION_ATTRIBUTES.find { |r| r.match? attribute }
    :associations
  elsif RAILS_ATTRIBUTES.include? attribute
    :common
  else
    :default
  end
end

def determine_attributes(model)
  attributes = { common: {}, associations: {}, default: {} }
  model.attribute_types.each do |attribute, type|
    type_name = type.class.name.demodulize&.underscore

    attributes[determine_attribute_section(attribute)][attribute] = type_name == 'time_zone_converter' ? 'datetime' : type_name
  end
  attributes
end

def determine_association_target(association)
  return association.options[:class_name].constantize.table_name if association.options[:class_name]

  association.plural_name.to_s
end

def determine_alias(association)
  as = nil
  as = association.name if association.options[:class_name]
  as = association.options[:as] if association.options[:as]

  as
end

def determine_amounts(association)
  if association.macro == :has_many
    from_amount = '"0..n" '
    to_amount   = '"1" '
  else
    from_amount = '"1" '
    to_amount   = '"0..n" '
  end

  { from: from_amount, to: to_amount }
end

def output(tables, relations)
  relations.sort!

  out = "@startuml\n"
  out += "' Generated on #{Time.zone.now}.Use the runner, Luke.\n"
  out += "' Some indices may be incorrect, this file is for a general comprehension only.\n\n"
  tables.each_pair do |name, fields|
    out += output_table(name, fields)
  end

  out += relations.join("\n")

  "#{out}\n\n@enduml"
end

def output_table(name, fields)
  attributes = output_table_attributes fields[:default]
  attributes += "  .. associations ..\n#{output_table_attributes(fields[:associations])}" if fields[:associations].keys.count.positive?
  attributes += "  .. common fields ..\n#{output_table_attributes(fields[:common])}" if fields[:common].keys.count.positive?
  "object #{name} {\n#{attributes}}\n\n"
end

def output_table_attributes(attributes)
  output = ''
  attributes.each_pair { |k, t| output += "  #{k} <#{t}>\n" }

  output
end

Dir.glob(Rails.root.join('app', 'models', '*.rb')).sort.each do |f|
  class_name = File.basename(f, '.rb').camelize
  next if EXCLUDED_MODELS.include? class_name

  # Standard attributes
  model              = class_name.constantize
  table_name         = model.table_name
  tables[table_name] = determine_attributes model

  # Relations
  model.reflect_on_all_associations.each do |association|
    next if association.macro == :has_many && association.type.blank? || association.options[:polymorphic]
    next if EXCLUDED_TABLES.find { |r| r.match? association.name.to_s }

    to_table = determine_association_target association
    next if EXCLUDED_TABLES.find { |r| r.match? to_table }

    as      = determine_alias association
    amounts = determine_amounts association

    relations << "#{table_name} #{amounts[:from]}--> #{amounts[:to]}#{to_table}#{as ? " : #{as}" : ''}"
  end
end

puts output tables, relations # rubocop:disable Rails/Output

<!--
Thank you for your participation!

Please, fill the sections or remove if unnecessary.
Note that this formalism only helps us to manage the MRs easily.
-->

<!-- Describe your merge request here -->

## Checks

<!--
If you're unsure ~~strike through~~ an element.
Delete useless ones if you know what you're doing
-->

- [ ] I created/updated FactoryBot factories (preferably using Faker)
- [ ] I created/updated RSpec tests:
  - [ ] Acceptance
  - [ ] Channel
  - [ ] Class (lib/...)
  - [ ] Mailer
  - [ ] Model
  - [ ] Policy
  - [ ] Request
- [ ] I created/updated JS tests according to my changes
- [ ] I created/updated Cucumber scenarios
- [ ] My commits follows the [conventional commits format](https://www.conventionalcommits.org)
- [ ] I updated the readme to reflect my changes
<!-- Changelogs are in app/views/app/_changelog.*.html.haml -->
- [ ] I updated the changelogs to reflect my changes, in a separate commit

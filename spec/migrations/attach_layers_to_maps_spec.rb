require 'rails_helper'

require Rails.root.join 'db', 'migrate', '20210311131440_attach_layers_to_maps.rb'
AttachLayersToMaps::BACKUP_FILE = Rails.root.join('tmp', 'rspec_backup_attach_layers_to_maps.yml')

RSpec.describe AttachLayersToMaps, type: :migration do
  let(:layers) { table(:layers) }
  let(:genera) { table(:genera) }
  let(:users) { table(:users) }
  let(:maps) { table(:maps) }
  let(:patches) { table(:patches) }
  let(:elements) { table(:elements) }
  let(:resources) { table(:resources) }

  after do
    FileUtils.rm AttachLayersToMaps::BACKUP_FILE if File.exist? AttachLayersToMaps::BACKUP_FILE
  end

  def fill_database
    # Before getting to this point, RSpec already migrated once, up and down.
    # The migration we're testing creates a layer on down, so we need to clean
    # this table first.
    layers.destroy_all

    user = users.create! role: 'user', email: 'john.doe@example.com', encrypted_password: 'abc123', username: 'John'
    map  = maps.create! name: 'A map', center: '0.0,0.0', user_id: user.id
    maps.create! name: 'Another map', center: '0.0,0.0', user_id: user.id

    genera.create!(name: 'Some kind of plant', description: 'Some string', kingdom: 0).tap do |genus|
      5.times do |index|
        layers.create!(name: "layer#{index}", description: 'Some description', color: '122,122,122').tap do |layer|
          resources.create!(name: "Plant #{index}", description: 'something', layer_id: layer.id, genus_id: genus.id).tap do |resource|
            patches.create!(map_id: map.id, geometry: '{}').tap do |patch|
              elements.create! patch_id: patch.id, resource_id: resource.id
            end
          end
        end
      end
    end
  end

  def delete_first_resource
    resources.first.tap do |resource|
      resource_elements = elements.where(resource_id: resource.id)
      patch_ids = resource_elements.map(&:patch_id)
      resource_elements.destroy_all
      patches.where(id: patch_ids).destroy_all
      resource.destroy
    end
  end

  def delete_backup_file
    FileUtils.rm AttachLayersToMaps::BACKUP_FILE if File.exist? AttachLayersToMaps::BACKUP_FILE
  end

  context 'with a backup file' do
    context 'when resources were unchanged' do
      it 'is reversible' do
        fill_database
        expect(layers.count).to eq 5

        migrate!
        reset_column_in_all_models
        expect(layers.count).to eq 10

        schema_migrate_down!
        # Old ones + the new
        expect(layers.count).to eq 6
      end
    end

    context 'when resources changed' do
      it 'is reversible' do
        fill_database
        expect(layers.count).to eq 5

        migrate!
        reset_column_in_all_models
        expect(layers.count).to eq 10

        delete_first_resource
        resources.create! name: 'Something', genus_id: genera.first.id, color: '0,0,0', description: 'Something too'

        schema_migrate_down!
        # Old ones + the new
        expect(layers.count).to eq 6
      end
    end
  end

  context 'without a backup file' do
    context 'when resources were unchanged' do
      it 'is reversible' do
        fill_database
        expect(layers.count).to eq 5

        migrate!
        reset_column_in_all_models
        expect(layers.count).to eq 10

        delete_backup_file

        schema_migrate_down!
        # Old ones + the new
        expect(layers.count).to eq 1
      end
    end

    context 'when resources changed' do
      it 'is reversible' do
        fill_database
        expect(layers.count).to eq 5

        migrate!
        reset_column_in_all_models
        expect(layers.count).to eq 10

        delete_backup_file

        delete_first_resource
        resources.create! name: 'Something', genus_id: genera.first.id, color: '0,0,0', description: 'Something too'

        schema_migrate_down!
        # Old ones + the new
        expect(layers.count).to eq 1
      end
    end
  end
end

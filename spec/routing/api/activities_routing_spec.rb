require 'rails_helper'

RSpec.describe Api::ActivitiesController, type: :routing do
  describe 'routing' do
    it 'routes to #index via elements' do
      expect(get: '/api/elements/1/activities').to route_to('api/activities#index', element_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/activities/1').to route_to('api/activities#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/activities').to route_to('api/activities#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/activities/1').to route_to('api/activities#update', id: '1')
    end

    it 'routes to #picture' do
      expect(get: '/api/activities/10/pictures/1').to route_to('api/activities#picture', id: '10', picture_id: '1')
    end

    it 'routes to #picture_thumbnail' do
      expect(get: '/api/activities/10/pictures/1/thumbnail').to route_to('api/activities#picture_thumbnail', id: '10', picture_id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/activities/1').to route_to('api/activities#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/activities/1').to route_to('api/activities#destroy', id: '1')
    end
  end
end

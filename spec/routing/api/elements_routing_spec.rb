require 'rails_helper'

RSpec.describe Api::ElementsController, type: :routing do
  describe 'routing' do
    it 'routes to #index via a map' do
      expect(get: '/api/maps/1/elements').to route_to(controller: 'api/elements', action: 'index', map_id: '1')
    end

    it 'routes to #index via a patch' do
      expect(get: '/api/patches/1/elements').to route_to(controller: 'api/elements', action: 'index', patch_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/elements/1').to route_to('api/elements#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/elements').to route_to('api/elements#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/elements/1').to route_to('api/elements#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/elements/1').to route_to('api/elements#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/elements/1').to route_to('api/elements#destroy', id: '1')
    end
  end
end

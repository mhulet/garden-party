require 'rails_helper'

RSpec.describe Api::PathsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/maps/1/paths').to route_to('api/paths#index', map_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/paths/1').to route_to('api/paths#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/paths').to route_to('api/paths#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/paths/1').to route_to('api/paths#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/paths/1').to route_to('api/paths#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/paths/1').to route_to('api/paths#destroy', id: '1')
    end
  end
end

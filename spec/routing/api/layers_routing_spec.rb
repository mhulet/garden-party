require 'rails_helper'

RSpec.describe Api::LayersController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/maps/1/layers').to route_to('api/layers#index', map_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/layers/1').to route_to('api/layers#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/layers').to route_to('api/layers#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/layers/1').to route_to('api/layers#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/layers/1').to route_to('api/layers#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/layers/1').to route_to('api/layers#destroy', id: '1')
    end
  end
end

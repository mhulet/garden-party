require 'rails_helper'

RSpec.describe AppController, type: :routing do
  describe 'routing' do
    it 'routes to #home' do
      expect(get: '/').to route_to('app#home')
    end

    it 'routes to #app' do
      expect(get: '/app').to route_to('app#app')
    end

    it 'routes to #shared' do
      expect(get: '/shared/maps/1').to route_to('app#shared', map_id: '1')
    end
  end
end

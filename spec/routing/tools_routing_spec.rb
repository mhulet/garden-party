require 'rails_helper'

RSpec.describe ToolsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/tools').to route_to('tools#index')
    end

    it 'routes to #app' do
      expect(get: '/tools/soil-analysis').to route_to('tools#soil_analysis')
    end
  end
end

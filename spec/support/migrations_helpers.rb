# frozen_string_literal: true

# Original source: https://gitlab.com/gitlab-org/gitlab/-/blob/c4088b82ec76ff0adf1852b9dea18d6830d48e9e/spec/support/helpers/migrations_helpers.rb
#
# License: MIT Expat
# License at time of copy: https://gitlab.com/gitlab-org/gitlab/-/blob/c4088b82ec76ff0adf1852b9dea18d6830d48e9e/LICENSE

module MigrationsHelpers
  def active_record_base
    ActiveRecord::Base
  end

  def table(name)
    Class.new(active_record_base) do
      self.table_name         = name
      self.inheritance_column = :_type_disabled

      def self.name
        table_name.singularize.camelcase
      end
    end
  end

  def migrations_paths
    ActiveRecord::Migrator.migrations_paths
  end

  def migration_context
    ActiveRecord::MigrationContext.new(migrations_paths, ActiveRecord::SchemaMigration)
  end

  delegate :migrations, to: :migration_context

  def clear_schema_cache!
    active_record_base.connection_pool.connections.each do |conn|
      conn.schema_cache.clear!
    end
  end

  def foreign_key_exists?(source, target = nil, column: nil)
    ActiveRecord::Base.connection.foreign_keys(source).any? do |key|
      if column
        key.options[:column].to_s == column.to_s
      else
        key.to_table.to_s == target.to_s
      end
    end
  end

  def reset_column_in_all_models
    clear_schema_cache!

    # Reset column information for the most offending classes **after** we
    # migrated the schema up, otherwise, column information could be
    # outdated.
    active_record_base.descendants.each { |param| reset_column_information(param) }
  end

  def reset_column_information(klass)
    klass.reset_column_information
  end

  def previous_migration
    migrations.each_cons(2) do |previous, migration|
      break previous if migration.name == described_class.name
    end
  end

  def migration_schema_version
    metadata_schema = self.class.metadata[:schema]

    if metadata_schema == :latest
      migrations.last.version
    else
      metadata_schema || previous_migration.version
    end
  end

  def schema_migrate_down!
    disable_migrations_output do
      migration_context.down(migration_schema_version)
    end

    reset_column_in_all_models
  end

  def schema_migrate_up!
    reset_column_in_all_models

    disable_migrations_output do
      migration_context.up
    end

    reset_column_in_all_models
  end

  def disable_migrations_output
    ActiveRecord::Migration.verbose = false

    yield
  ensure
    ActiveRecord::Migration.verbose = true
  end

  def migrate!
    migration_context.up do |migration|
      migration.name == described_class.name
    end
  end

  class ReversibleMigrationTest
    attr_reader :before_up, :after_up

    def initialize
      @before_up = -> {}
      @after_up  = -> {}
    end

    def before(expectations)
      @before_up = expectations

      self
    end

    def after(expectations)
      @after_up = expectations

      self
    end
  end

  def reversible_migration
    tests = yield(ReversibleMigrationTest.new)

    tests.before_up.call

    migrate!

    tests.after_up.call

    schema_migrate_down!

    tests.before_up.call
  end
end

RSpec.configure do |config|
  config.include MigrationsHelpers, type: :migration

  # Each example may call `migrate!`, so we must ensure we are migrated down every time
  config.before(:each, type: :migration) do
    schema_migrate_down!
  end

  config.after(:context, type: :migration) do
    schema_migrate_up!
  end
end

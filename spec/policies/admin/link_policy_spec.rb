require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Admin::LinkPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:user_admin) { FactoryBot.create :user_admin }
  let(:link) { FactoryBot.create :link, user: user }

  permissions '.scope' do
    before do
      FactoryBot.create :link, :pending
      FactoryBot.create :link, :approved
    end

    context 'with authenticated admin' do
      it 'returns all the links' do
        expect(Pundit.policy_scope!(user_admin, [:admin, Link]).all.count).to eq 2
      end
    end
  end

  permissions :index?, :show?, :new?, :create?, :edit?, :update?, :destroy?, :approve? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, link)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, link)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, link)
      end
    end
  end
end

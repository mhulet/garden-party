require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Admin::ResourceInteractionPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:user_admin) { FactoryBot.create :user_admin }
  let(:resource_interaction) { FactoryBot.create :resource_interaction }

  permissions '.scope' do
    before { FactoryBot.create_list :resource_interaction, 2 }

    context 'with authenticated admin' do
      it 'returns all the resource interactions' do
        expect(Pundit.policy_scope!(user_admin, [:admin, ResourceInteraction]).all.count).to eq 2
      end
    end
  end

  permissions :index?, :new?, :create? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, ResourceInteraction)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, ResourceInteraction)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, ResourceInteraction)
      end
    end
  end

  permissions :show?, :edit?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, resource_interaction)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, resource_interaction)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, resource_interaction)
      end
    end
  end
end

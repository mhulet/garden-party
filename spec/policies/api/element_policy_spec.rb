require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::ElementPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:element) do
    map = FactoryBot.create :map, user: user
    patch = FactoryBot.create :patch, map: map
    patch.elements.first
  end
  let(:other_user) { FactoryBot.create :user }
  let(:other_patch) { FactoryBot.create :patch }

  permissions '.scope' do
    before do
      element
      FactoryBot.create_list :element, 2, patch: other_patch
    end

    context 'with no authenticated user' do
      it 'returns no elements' do
        expect(Pundit.policy_scope!(nil, [:api, Element]).all.count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the owned elements' do
        expect(Pundit.policy_scope!(user, [:api, Element]).all.count).to eq 1
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, element)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, element)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, element)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Element)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Element)
      end
    end
  end
end

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::PathPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:path) do
    map = FactoryBot.create :map, user: user
    FactoryBot.create :path, map: map
  end
  let(:other_user) { FactoryBot.create :user }
  let(:other_path) { FactoryBot.create :path }

  permissions '.scope' do
    before do
      path
      FactoryBot.create_list :path, 2
    end

    context 'with no authenticated user' do
      it 'returns no paths' do
        expect(Pundit.policy_scope!(nil, [:api, Path]).all.count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the owned paths' do
        expect(Pundit.policy_scope!(user, [:api, Path]).all.count).to eq 1
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, path)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, path)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, path)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Path)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Path)
      end
    end
  end
end

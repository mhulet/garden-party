require 'rails_helper'
require 'pundit/rspec'

# FIXME: Set real policies
RSpec.describe Api::ResourcePolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:user_admin) { FactoryBot.create :user_admin }
  let(:resource) { FactoryBot.create :resource }

  permissions '.scope' do
    before { FactoryBot.create_list :resource, 2 }

    context 'with no authenticated user' do
      it 'returns no resources' do
        expect(Pundit.policy_scope!(nil, [:api, Resource]).all.count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the resources' do
        expect(Pundit.policy_scope!(user, [:api, Resource]).all.count).to eq 2
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Resource)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Resource)
      end
    end
  end

  permissions :show?, :update? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, resource)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, resource)
      end
    end
  end

  permissions :create? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Resource)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Resource)
      end
    end
  end

  permissions :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, resource)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, resource)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, resource)
      end
    end
  end
end

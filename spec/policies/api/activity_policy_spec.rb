require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::ActivityPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:activity) do
    map = FactoryBot.create :map, user: user
    patch = FactoryBot.create :patch, map: map
    element = patch.elements.first
    FactoryBot.create :activity, subject: element
  end
  let(:other_user) { FactoryBot.create :user }
  let(:other_activity) { FactoryBot.create :activity }

  permissions '.scope' do
    before do
      activity
      FactoryBot.create_list :activity, 2
    end

    context 'with no authenticated user' do
      it 'returns all activities' do
        expect(Pundit.policy_scope!(nil, [:api, Activity]).all.count).to eq 3
      end
    end

    context 'with authenticated user' do
      it 'returns all activities' do
        expect(Pundit.policy_scope!(user, [:api, Activity]).all.count).to eq 3
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy?, :picture?, :picture_thumbnail? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, activity)
      end
    end

    context 'with authenticated user' do
      context 'when user is not activity owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, activity)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, activity)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Activity)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Activity)
      end
    end
  end
end

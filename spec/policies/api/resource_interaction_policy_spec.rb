require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::ResourceInteractionPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:user_admin) { FactoryBot.create :user_admin }
  let(:resource_interaction) { FactoryBot.create :resource_interaction }

  permissions '.scope' do
    before { FactoryBot.create_list :resource_interaction, 2 }

    context 'with no authenticated user' do
      it 'returns no resource interactions' do
        expect(Pundit.policy_scope!(nil, [:api, ResourceInteraction]).all.count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the resource interactions' do
        expect(Pundit.policy_scope!(user, [:api, ResourceInteraction]).all.count).to eq 2
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, ResourceInteraction)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, ResourceInteraction)
      end
    end
  end

  permissions :show?, :update? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, resource_interaction)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, resource_interaction)
      end
    end
  end

  permissions :create? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, ResourceInteraction)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, ResourceInteraction)
      end
    end
  end

  permissions :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, resource_interaction)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, resource_interaction)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, resource_interaction)
      end
    end
  end
end

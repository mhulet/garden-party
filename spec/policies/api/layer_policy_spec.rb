require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::LayerPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:layer) { FactoryBot.create(:map, user: user).layers.first }
  let(:other_layer) { FactoryBot.create :layer }
  let(:other_user) { other_layer.map.user }

  permissions '.scope' do
    before do
      layer
      other_layer
    end

    context 'with no authenticated user' do
      it 'returns no layers' do
        expect(Pundit.policy_scope!(nil, [:api, Layer]).all.count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the owned layers' do
        expect(Pundit.policy_scope!(user, [:api, Layer]).all.count).to eq 1
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Layer)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, layer)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, layer)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Layer)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Layer)
      end
    end
  end
end

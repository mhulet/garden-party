require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::MapPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:other_user) { FactoryBot.create :user }
  let(:map) { FactoryBot.create :map, user: user }
  let(:other_map) { FactoryBot.create :map, user: other_user }
  let(:other_map_publicly_available) { FactoryBot.create :map, :publicly_available, user: other_user }

  permissions '.scope' do
    before do
      map
      other_map
    end

    context 'with no authenticated user' do
      it 'returns no maps' do
        expect(Pundit.policy_scope!(nil, [:api, Map]).all.count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the owned maps' do
        expect(Pundit.policy_scope!(user, [:api, Map]).all.count).to eq 1
      end
    end
  end

  permissions :show?, :update?, :destroy?, :picture? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, map)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, map)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, map)
        end
      end
    end
  end

  permissions :shared? do
    context 'with no authenticated user' do
      context 'when map is not publicly available' do
        it 'denies access' do
          expect(described_class).not_to permit(nil, other_map)
        end
      end

      context 'when map is publicly available' do
        it 'grants access' do
          expect(described_class).to permit(nil, other_map_publicly_available)
        end
      end
    end

    context 'with authenticated user' do
      context 'when map is not publicly available' do
        it 'denies access' do
          expect(described_class).not_to permit(user, other_map)
        end
      end

      context 'when map is publicly available' do
        it 'grants access' do
          expect(described_class).to permit(user, other_map_publicly_available)
        end
      end
    end
  end

  permissions :index?, :create? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Map)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Map)
      end
    end
  end
end

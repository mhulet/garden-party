require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::PatchPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:patch) do
    map = FactoryBot.create :map, user: user
    FactoryBot.create :patch, map: map
  end
  let(:other_user) { FactoryBot.create :user }
  let(:other_patch) { FactoryBot.create :patch }

  permissions '.scope' do
    before do
      patch
      FactoryBot.create_list :patch, 2
    end

    context 'with no authenticated user' do
      it 'returns no patches' do
        expect(Pundit.policy_scope!(nil, [:api, Patch]).all.count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the owned patches' do
        expect(Pundit.policy_scope!(user, [:api, Patch]).all.count).to eq 1
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, patch)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, patch)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, patch)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Patch)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Patch)
      end
    end
  end
end

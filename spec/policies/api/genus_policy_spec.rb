require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::GenusPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:user_admin) { FactoryBot.create :user_admin }
  let(:genus) { FactoryBot.create :genus }

  permissions '.scope' do
    before { FactoryBot.create_list :genus, 2 }

    context 'with no authenticated user' do
      it 'returns no genera' do
        expect(Pundit.policy_scope!(nil, [:api, Genus]).all.count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the genera' do
        expect(Pundit.policy_scope!(user, [:api, Genus]).all.count).to eq 2
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Genus)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Genus)
      end
    end
  end

  permissions :show?, :update? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, genus)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, genus)
      end
    end
  end

  permissions :create? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Genus)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Genus)
      end
    end
  end

  permissions :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, genus)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, genus)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, genus)
      end
    end
  end
end

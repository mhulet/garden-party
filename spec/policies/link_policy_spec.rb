require 'rails_helper'
require 'pundit/rspec'

RSpec.describe LinkPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:other_user) { FactoryBot.create :user }
  let(:link) { FactoryBot.create :link, :approved, user: user }
  let(:pending_link) { FactoryBot.create :link, user: user }
  let(:other_link) { FactoryBot.create :link, :approved, user: other_user }

  permissions '.scope' do
    before do
      link
      pending_link
      other_link
    end

    context 'with no authenticated user' do
      it 'returns approved links' do
        expect(Pundit.policy_scope!(nil, Link).all.count).to eq 2
      end
    end

    context 'with authenticated user' do
      it 'returns approved links' do
        expect(Pundit.policy_scope!(other_user, Link).all.count).to eq 2
      end
    end

    context 'with authenticated author' do
      it 'returns approved and pending links' do
        expect(Pundit.policy_scope!(user, Link).all.count).to eq 3
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(nil, Link)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(other_user, Link)
      end
    end
  end

  permissions :new?, :create? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Link)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(other_user, Link)
      end
    end
  end

  permissions :edit?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, link)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(other_user, link)
      end
    end

    context 'with authenticated author' do
      it 'grants access' do
        expect(described_class).to permit(user, link)
      end
    end
  end
end

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::PathsController, type: :acceptance do
  resource 'Paths', 'Manage paths'
  entity :path,
         id:                  { type: :integer, description: 'Entity identifier' },
         name:                { type: :string, required: false, description: 'Path name' },
         stroke_width:        { type: :integer, description: 'Path stroke thickness' },
         stroke_color:        { type: :string, description: 'Path line color' },
         stroke_style_preset: { type: :string, required: false, description: 'Stroke style name' },
         background_color:    { type: :string, description: 'Path background color, for polygons and circles' },
         map_id:              { type: :integer, description: 'Map identifier' },
         layer_id:            { type: :integer, description: 'Layer identifier' },
         geometry:            { type: :object, description: 'Valid minimal geoJSON geometry (check README)' },
         geometry_type:       { type: :string, description: 'Geometry type' },
         created_at:          { type: :datetime, description: 'Creation date' },
         updated_at:          { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :create_payload,
             name:                { type: :string, description: 'Path name' },
             map_id:              { type: :string, description: 'Map identifier' },
             layer_id:            { type: :integer, description: 'Layer identifier' },
             stroke_width:        { type: :float, description: 'Path stroke thickness' },
             stroke_color:        { type: :string, description: 'Path line color' },
             stroke_style_preset: { type: :string, required: false, description: 'Stroke style name' },
             background_color:    { type: :string, description: 'Path background color, for polygons and circles' },
             geometry:            { type: :object, description: 'Valid minimal geoJSON geometry (check README)' }

  parameters :update_payload,
             name:                { type: :string, description: 'Path name' },
             stroke_width:        { type: :float, description: 'Path stroke thickness' },
             stroke_color:        { type: :string, description: 'Path line color' },
             stroke_style_preset: { type: :string, required: false, description: 'Stroke style name' },
             background_color:    { type: :string, description: 'Path background color, for polygons and circles' },
             geometry:            { type: :object, description: 'Valid minimal geoJSON geometry (check README)' },
             layer_id:            { type: :integer, description: 'Layer identifier' }

  parameters :index_path_params,
             map_id: { type: :integer, description: 'Target map identifier' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
    let(:owned_path) { FactoryBot.create :path, map: owned_map }
    let(:owned_paths) { FactoryBot.create_list :path, 2, map: owned_map }

    on_get('/api/maps/:map_id/paths', 'List paths') do
      path_params defined: :index_path_params

      for_code 200 do |url|
        owned_paths
        visit url, path_params: { map_id: owned_map.id }

        expect(response).to have_many defined :path
      end
    end

    on_get('/api/paths/:id', 'Show one path') do
      path_params defined: :path_params

      for_code 200 do |url|
        visit url, path_params: { id: owned_path.id }

        expect(response).to have_one defined :path
      end

      for_code 404 do |url|
        visit url, path_params: { id: 0 }

        expect(response).to have_one defined :error
      end
    end

    on_post('/api/paths', 'Create new path') do
      request_params defined: :create_payload
      let(:path_attributes) do
        resource               = FactoryBot.create :resource
        attributes             = FactoryBot.build(:path, map: owned_map, layer: owned_map.layers.first).attributes
        attributes['elements'] = [{ resource_id: resource.id }]
        attributes
      end

      for_code 201 do |url|
        visit url, payload: { path: path_attributes }
      end

      for_code 422 do |url|
        bad_attributes = path_attributes.merge 'geometry' => nil
        visit url, payload: { path: bad_attributes }
      end
    end

    on_put('/api/paths/:id', 'Update a path') do
      path_params defined: :path_params
      request_params defined: :update_payload

      for_code 200 do |url|
        visit url, path_params: { id: owned_path.id }, payload: { name: 'New name' }
      end

      for_code 422 do |url|
        visit url, path_params: { id: owned_path.id }, payload: { geometry: {} }
      end
    end

    on_delete('/api/paths/:id', 'Destroys a path') do
      path_params defined: :path_params

      for_code 204 do |url|
        visit url, path_params: { id: owned_path.id }
      end
    end
  end
end

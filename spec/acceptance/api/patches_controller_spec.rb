require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::PatchesController, type: :acceptance do
  resource 'Patches', 'Manage patches'
  entity :patch,
         id:            { type: :integer, description: 'Entity identifier' },
         name:          { type: :string, required: false, description: 'Patch name' },
         map_id:        { type: :integer, description: 'Map identifier' },
         layer_id:      { type: :integer, description: 'Layer identifier' },
         geometry:      { type: :object, description: 'Valid minimal geoJSON geometry (check README)' },
         geometry_type: { type: :string, description: 'Geometry type' },
         created_at:    { type: :datetime, description: 'Creation date' },
         updated_at:    { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :create_payload,
             name:     { type: :string, description: 'Patch name' },
             map_id:   { type: :integer, description: 'Map identifier' },
             layer_id: { type: :integer, description: 'Map identifier' },
             geometry: { type: :object, description: 'Valid minimal geoJSON geometry (check README)' },
             elements: { type: :array, description: 'Set of resources to create in this patch', of: {
               name:        { type: :string, description: 'Element name' },
               resource_id: { type: :string, description: 'Resource identifier' },
             } }

  parameters :update_payload,
             name:     { type: :string, description: 'Patch name' },
             geometry: { type: :object, description: 'Valid minimal geoJSON geometry (check README)' },
             layer_id: { type: :integer, description: 'Layer identifier' }

  parameters :index_path_params,
             map_id: { type: :integer, description: 'Target map identifier' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
    let(:owned_patch) { FactoryBot.create :patch, map: owned_map }
    let(:owned_patches) { FactoryBot.create_list :patch, 2, map: owned_map }

    on_get('/api/maps/:map_id/patches', 'List patches') do
      path_params defined: :index_path_params

      for_code 200 do |url|
        owned_patches
        visit url, path_params: { map_id: owned_map.id }

        expect(response).to have_many defined :patch
      end
    end

    on_get('/api/patches/:id', 'Show one patch') do
      path_params defined: :path_params

      for_code 200 do |url|
        visit url, path_params: { id: owned_patch.id }

        expect(response).to have_one defined :patch
      end

      for_code 404 do |url|
        visit url, path_params: { id: 0 }

        expect(response).to have_one defined :error
      end
    end

    on_post('/api/patches', 'Create new patch') do
      request_params defined: :create_payload
      let(:patch_attributes) do
        resource = FactoryBot.create :resource
        attributes = FactoryBot.build(:patch, map: owned_map, layer: owned_map.layers.first).attributes
        attributes['elements'] = [{ resource_id: resource.id }]
        attributes
      end

      for_code 201 do |url|
        visit url, payload: { patch: patch_attributes }
      end

      for_code 422 do |url|
        bad_attributes = patch_attributes.merge 'geometry' => nil
        visit url, payload: { patch: bad_attributes }
      end
    end

    on_put('/api/patches/:id', 'Update a patch') do
      path_params defined: :path_params
      request_params defined: :update_payload

      for_code 200 do |url|
        visit url, path_params: { id: owned_patch.id }, payload: { name: 'New name' }
      end

      for_code 422 do |url|
        visit url, path_params: { id: owned_patch.id }, payload: { geometry: {} }
      end
    end

    on_delete('/api/patches/:id', 'Destroys a patch') do
      path_params defined: :path_params

      for_code 204 do |url|
        visit url, path_params: { id: owned_patch.id }
      end
    end
  end
end

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::MapsController, type: :acceptance do
  resource 'Maps', 'Manage maps'

  entity :picture_details,
         url:  { type: :string, description: 'URL to the picture' },
         size: { type: :string, description: 'Picture size, as "1000x2000"' }

  entity :point,
         x: { type: :number, description: 'Coordinate on X axis' },
         y: { type: :number, description: 'Coordinate on Y axis' }

  entity :map,
         id:                 { type: :integer, description: 'Entity identifier' },
         name:               { type: :string, description: 'Map name' },
         center:             { type: :object, description: 'User-defined point on the map', attributes: :point },
         extent_height:      { type: :number, required: false, description: 'Map extent height. Only on picture-based maps.' },
         extent_width:       { type: :number, required: false, description: 'Map extent width. Only on picture-based maps.' },
         picture:            { type: :object, required: false, description: 'Background picture details for custom maps', attributes: :picture_details },
         publicly_available: { type: :boolean, description: 'True when the map is available to non-owner visitors' },
         created_at:         { type: :datetime, description: 'Creation date' },
         updated_at:         { type: :datetime, description: 'Update date' }

  entity :map_and_data,
         map:       { type: :object, description: 'Map configuration. See a map response for attributes' },
         patches:   { type: :array, description: 'List of associated patches. See a patch response for attributes' },
         paths:     { type: :array, description: 'List of associated paths. See a path response for attributes' },
         elements:  { type: :array, description: 'List of associated elements. See an element response for attributes' },
         layers:    { type: :array, description: 'List of associated layers. See a layer response for attributes' },
         resources: { type: :array, description: 'List of resources used on the map. See a resource response for attributes' }

  entity :error,
         error: { type: :string, description: 'The error' }

  # FIXME: Update RSpec-Rails-API to handle file upload
  parameters :create_update_payload,
             name:          { type: :string, description: 'Map name' },
             center:        { type: :array, description: 'User-defined point on the map (e.g.: [10, 20])' },
             extent_height: { type: :number, required: false, description: 'Map extent height. Required for picture-based maps.' },
             extent_width:  { type: :number, required: false, description: 'Map extent width. Required for picture-based maps.' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
    let(:owned_maps) { FactoryBot.create_list :map, 2, user: signed_in_user }

    on_get('/api/maps', 'List maps') do
      for_code 200 do |url|
        owned_maps
        visit url

        expect(response).to have_many defined :map
      end
    end

    on_get('/api/maps/:id', 'Show one map') do
      path_params defined: :path_params

      for_code 200 do |url|
        visit url, path_params: { id: owned_map.id }

        expect(response).to have_one defined :map
      end

      for_code 404 do |url|
        visit url, path_params: { id: 0 }

        expect(response).to have_one defined :error
      end
    end

    on_post('/api/maps', 'Create new map') do
      request_params defined: :create_update_payload
      let(:map_attributes) { { name: 'My first map', center: [10, 10] } }

      for_code 201 do |url|
        visit url, payload: map_attributes
      end

      for_code 422 do |url|
        visit url, payload: { name: '' }
      end
    end

    on_put('/api/maps/:id', 'Update a map') do
      path_params defined: :path_params
      request_params defined: :create_update_payload

      for_code 200 do |url|
        visit url, path_params: { id: owned_map.id }, payload: { name: 'New name' }
      end

      for_code 422 do |url|
        visit url, path_params: { id: owned_map.id }, payload: { name: '' }
      end
    end

    on_delete('/api/maps/:id', 'Destroys a map') do
      path_params defined: :path_params

      for_code 204 do |url|
        visit url, path_params: { id: owned_map.id }
      end
    end
  end

  # FIXME: https://github.com/rubocop/rubocop-rspec/issues/1126 should be merged
  # to avoid disabling Rubocop.
  describe 'shared routes' do # rubocop:disable RSpec/EmptyExampleGroup
    let(:map) { FactoryBot.create :map, :publicly_available }

    on_get('/api/shared/maps/:id', 'Show one shared map with its data') do
      path_params defined: :path_params

      for_code 200 do |url|
        visit url, path_params: { id: map.id }

        expect(response).to have_one defined :map_and_data
      end
    end
  end
end

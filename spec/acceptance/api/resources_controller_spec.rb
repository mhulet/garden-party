require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ResourcesController, type: :acceptance do
  resource 'Resources', 'Manage resources'

  entity :resource,
         id:           { type: :integer, description: 'Entity identifier' },
         name:         { type: :string, description: 'Resource name' },
         description:  { type: :string, description: 'Resource description' },
         source:       { type: :string, required: false, description: 'Link to description source' },
         edible:       { type: :boolean, description: 'Whether or not the resource is safe to eat' },
         common_names: { type: :array, required: false, description: 'Other common names' },
         picture_name: { type: :string, description: 'Base name for picture' },
         border_color: { type: :string, description: 'Color to use as border color' },
         fill_color:   { type: :string, description: 'Color to use as background color' },
         parent_id:    { type: :integer, required: false, description: 'Parent identifier, for plants with variants' },
         genus_id:     { type: :integer, description: 'Resource family identifier' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :create_update_payload,
             name:         { type: :string, description: 'Resource name' },
             description:  { type: :string, description: 'Resource description' },
             source:       { type: :string, required: false, description: 'Link to description source' },
             common_names: { type: :array, required: false, description: 'Other common names' },
             genus_id:     { type: :integer, description: 'Genus identifier' },
             parent_id:    { type: :integer, required: false, description: 'Parent resource identifier' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/resources', 'List resources') do
      for_code 200 do |url|
        FactoryBot.create_list :resource, 2
        visit url

        expect(response).to have_many defined :resource
      end
    end

    on_get('/api/resources/:id', 'Show one resource') do
      path_params defined: :path_params

      for_code 200 do |url|
        resource = FactoryBot.create :resource
        visit url, path_params: { id: resource.id }

        expect(response).to have_one defined :resource
      end

      for_code 404 do |url|
        visit url, path_params: { id: 0 }

        expect(response).to have_one defined :error
      end
    end

    on_post('/api/resources', 'Create new resource') do
      request_params defined: :create_update_payload

      for_code 201 do |url|
        resource_attributes = FactoryBot.build(:resource).attributes
        visit url, payload: resource_attributes
      end

      for_code 422 do |url|
        visit url, payload: { name: '' }
      end
    end

    on_put('/api/resources/:id', 'Update a resource') do
      path_params defined: :path_params
      request_params defined: :create_update_payload
      let(:resource) { FactoryBot.create :resource }

      for_code 200 do |url|
        visit url, path_params: { id: resource.id }, payload: { name: 'New name' }
      end

      for_code 422 do |url|
        visit url, path_params: { id: resource.id }, payload: { name: '' }
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'

    on_delete('/api/resources/:id', 'Destroys a resource') do
      path_params defined: :path_params
      let(:resource) { FactoryBot.create :resource }

      for_code 204 do |url|
        visit url, path_params: { id: resource.id }
      end
    end
  end
end

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ActivitiesController, type: :acceptance do
  resource 'Activities', 'Manage activities'

  entity :activity,
         id:           { type: :integer, description: 'Entity identifier' },
         name:         { type: :string, description: 'Activity name' },
         notes:        { type: :string, required: false, description: 'Additional notes' },
         planned_for:  { type: :datetime, required: false, description: 'Date when the activity is planned' },
         done_at:      { type: :datetime, required: false, description: 'Date when the activity was performed' },
         subject_id:   { type: :integer, description: 'Subject identifier' },
         subject_type: { type: :string, description: 'Subject type' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Update date' },
         pictures:     { type: :array, description: 'List of URLS to attached pictures', of: {
           source:    { type: :string, description: 'Full picture URL' },
           thumbnail: { type: :string, description: 'Thumbnail URL' },
         } }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :create_payload,
             name:         { type: :string, description: 'Activity name' },
             notes:        { type: :string, description: 'Additional notes' },
             planned_for:  { type: :datetime, required: false, description: 'Date when the activity is planned (required if activity is not done)' },
             done_at:      { type: :datetime, required: false, description: 'Date when the activity was performed (required if activity is not planned)' },
             subject_id:   { type: :integer, description: 'Subject identifier' },
             subject_type: { type: :string, description: 'Subject type' }
  parameters :update_payload,
             notes:       { type: :string, description: 'Additional notes' },
             planned_for: { type: :datetime, required: false, description: 'Date when the activity is planned (required if activity is not done)' },
             done_at:     { type: :datetime, required: false, description: 'Date when the activity was performed (required if activity is not planned)' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
    let(:owned_patch) { FactoryBot.create :patch, map: owned_map }
    let(:owned_activities) { FactoryBot.create_list :activity, 2, subject: owned_patch.elements.first }
    let(:owned_activity) { FactoryBot.create :activity, subject: owned_patch.elements.first }

    on_get('/api/elements/:element_id/activities', 'List activities for a element') do
      path_params fields: { element_id: { type: :integer, description: 'Target element identifier' } }

      for_code 200 do |url|
        owned_activities
        visit url, path_params: { element_id: owned_patch.elements.first.id }

        expect(response).to have_many defined :activity
      end
    end

    on_get('/api/maps/:map_id/activities', 'List activities for a map') do
      path_params fields: { map_id: { type: :integer, description: 'Target map identifier' } }

      for_code 200 do |url|
        owned_activities
        visit url, path_params: { map_id: owned_patch.map_id }

        expect(response).to have_many defined :activity
      end
    end

    on_get('/api/maps/:map_id/activities/names', 'List used activity names by user') do
      path_params fields: { map_id: { type: :integer, description: 'Target map identifier' } }

      # Only check for code
      for_code 200 do |url|
        owned_activities
        visit url, path_params: { map_id: owned_patch.map_id }

        expect(JSON.parse(response.body)).to be_a Array
      end
    end

    on_get('/api/activities/:id', 'Show one activity') do
      path_params defined: :path_params

      for_code 200 do |url|
        visit url, path_params: { id: owned_activity.id }

        expect(response).to have_one defined :activity
      end

      for_code 404 do |url|
        visit url, path_params: { id: 0 }

        expect(response).to have_one defined :error
      end
    end

    on_post('/api/activities', 'Create new activity') do
      request_params defined: :create_payload
      let(:activity_attributes) do
        FactoryBot.build(:activity, subject: owned_patch.elements.first).attributes
      end

      for_code 201 do |url|
        puts activity_attributes
        visit url, payload: activity_attributes
      end

      for_code 422 do |url|
        bad_attributes = activity_attributes.merge 'name' => ''
        visit url, payload: bad_attributes
      end
    end

    on_put('/api/activities/:id', 'Update an activity') do
      path_params defined: :path_params
      request_params defined: :update_payload

      for_code 200 do |url|
        visit url, path_params: { id: owned_activity.id }, payload: { name: 'New name' }
      end

      for_code 422 do |url|
        visit url, path_params: { id: owned_activity.id }, payload: { planned_for: nil, done_at: nil }
      end
    end

    on_delete('/api/activities/:id', 'Destroys an activity') do
      path_params defined: :path_params

      for_code 204 do |url|
        visit url, path_params: { id: owned_activity.id }
      end
    end
  end
end

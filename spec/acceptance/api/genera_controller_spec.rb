require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::GeneraController, type: :acceptance do
  resource 'Genera', 'Manage genera'

  entity :genus,
         id:          { type: :integer, description: 'Entity identifier' },
         name:        { type: :string, description: 'Genus name' },
         description: { type: :string, description: 'Genus description' },
         source:      { type: :string, required: false, description: 'Link to description source' },
         kingdom:     { type: :string, description: 'Genus kingdom' },
         created_at:  { type: :datetime, description: 'Creation date' },
         updated_at:  { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :create_update_payload,
             name:        { type: :string, description: 'Genus name' },
             description: { type: :string, description: 'Genus description' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/genera', 'List genera') do
      for_code 200 do |url|
        FactoryBot.create_list :genus, 2
        visit url

        expect(response).to have_many defined :genus
      end
    end

    on_get('/api/genera/:id', 'Show one genus') do
      path_params defined: :path_params

      for_code 200 do |url|
        genus = FactoryBot.create :genus
        visit url, path_params: { id: genus.id }

        expect(response).to have_one defined :genus
      end

      for_code 404 do |url|
        visit url, path_params: { id: 0 }

        expect(response).to have_one defined :error
      end
    end

    on_post('/api/genera', 'Create new genus') do
      request_params defined: :create_update_payload

      for_code 201 do |url|
        genus_attributes = FactoryBot.build(:genus).attributes
        visit url, payload: genus_attributes
      end

      for_code 422 do |url|
        visit url, payload: { name: '' }
      end
    end

    on_put('/api/genera/:id', 'Update a genus') do
      path_params defined: :path_params
      request_params defined: :create_update_payload
      let(:genus) { FactoryBot.create :genus }

      for_code 200 do |url|
        visit url, path_params: { id: genus.id }, payload: { name: 'New name' }
      end

      for_code 422 do |url|
        visit url, path_params: { id: genus.id }, payload: { name: '' }
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'

    on_delete('/api/genera/:id', 'Destroys a genus') do
      path_params defined: :path_params
      let(:genus) { FactoryBot.create :genus }

      for_code 204 do |url|
        visit url, path_params: { id: genus.id }
      end
    end
  end
end

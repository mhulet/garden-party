require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ResourceInteractionsController, type: :acceptance do
  resource 'Resource interactions', 'Manage resource interactions'

  entity :resource_interaction,
         id:         { type: :integer, description: 'Entity identifier' },
         notes:      { type: :string, required: false, description: 'Notes on this interaction' },
         nature:     { type: :string, description: 'Interaction type' },
         subject_id: { type: :integer, description: 'Resource identifier, from which the interaction is made' },
         target_id:  { type: :integer, description: 'Resource identifier, on which the interaction is made' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :create_update_payload,
             notes:      { type: :string, required: false, description: 'Notes on this interaction' },
             subject_id: { type: :integer, description: 'Resource identifier, from which the interaction is made' },
             target_id:  { type: :integer, description: 'Resource identifier, on which the interaction is made' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/resource_interactions', 'List resources interactions') do
      for_code 200 do |url|
        FactoryBot.create_list :resource_interaction, 2
        visit url

        expect(response).to have_many defined :resource_interaction
      end
    end

    on_get('/api/resources/:resource_id/interactions', 'List resources interactions in wich a resource is involved') do
      path_params fields: { resource_id: { type: :integer, description: 'Resource identifier' } }

      for_code 200 do |url|
        FactoryBot.create_list :resource_interaction, 2
        visit url, path_params: { resource_id: Resource.last.id }

        expect(response).to have_many defined :resource_interaction
      end
    end

    on_get('/api/resource_interactions/:id', 'Show one interaction') do
      path_params defined: :path_params

      for_code 200 do |url|
        resource_interaction = FactoryBot.create :resource_interaction
        visit url, path_params: { id: resource_interaction.id }

        expect(response).to have_one defined :resource_interaction
      end

      for_code 404 do |url|
        visit url, path_params: { id: 0 }

        expect(response).to have_one defined :error
      end
    end

    on_post('/api/resource_interactions', 'Create new interaction') do
      request_params defined: :create_update_payload

      for_code 201 do |url|
        resource_interaction_attributes = FactoryBot.build(:resource_interaction).attributes
        visit url, payload: resource_interaction_attributes
      end

      for_code 422 do |url|
        visit url, payload: { name: '' }
      end
    end

    on_put('/api/resource_interactions/:id', 'Update an interaction') do
      path_params defined: :path_params
      request_params defined: :create_update_payload
      let(:resource_interaction) { FactoryBot.create :resource_interaction }

      for_code 200 do |url|
        visit url, path_params: { id: resource_interaction.id }, payload: { notes: 'Explanation:...' }
      end

      for_code 422 do |url|
        visit url, path_params: { id: resource_interaction.id }, payload: { subject_id: 'failure' }
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'

    on_delete('/api/resource_interactions/:id', 'Destroys an interaction') do
      path_params defined: :path_params
      let(:resource_interaction) { FactoryBot.create :resource_interaction }

      for_code 204 do |url|
        visit url, path_params: { id: resource_interaction.id }
      end
    end
  end
end

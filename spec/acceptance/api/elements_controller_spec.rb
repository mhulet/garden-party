require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ElementsController, type: :acceptance do
  resource 'Elements', 'Manage elements'

  entity :element,
         id:                       { type: :integer, description: 'Entity identifier' },
         name:                     { type: :string, required: false, description: 'Element name' },
         resource_id:              { type: :integer, description: 'Resource identifier' },
         implanted_at:             { type: :datetime, required: false, description: 'Implantation date' },
         implantation_planned_for: { type: :datetime, required: false, description: 'Planned implantation date' },
         removed_at:               { type: :datetime, required: false, description: 'Removal date' },
         removal_planned_for:      { type: :datetime, required: false, description: 'Planned removal date' },
         status:                   { type: :string, description: 'Element status' },
         patch_id:                 { type: :integer, description: 'Patch identifier' },
         created_at:               { type: :datetime, description: 'Creation date' },
         updated_at:               { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :create_update_payload,
             name:                     { type: :string, description: 'Element name' },
             implanted_at:             { type: :datetime, required: false, description: 'Implantation date' },
             implantation_planned_for: { type: :datetime, required: false, description: 'Planned implantation date' },
             removed_at:               { type: :datetime, required: false, description: 'Removal date' },
             removal_planned_for:      { type: :datetime, required: false, description: 'Planned removal date' },
             resource_id:              { type: :string, description: 'Resource identifier' },
             patch_id:                 { type: :string, description: 'Patch identifier' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
    let(:owned_patch) { FactoryBot.create :patch, map: owned_map }
    let(:owned_elements) { FactoryBot.create_list :element, 2, patch: owned_patch }
    let(:owned_element) { FactoryBot.create :element, patch: owned_patch }

    on_get('/api/maps/:map_id/elements', 'List elements on a map') do
      path_params fields: { map_id: { type: :integer, description: 'Target map identifier' } }

      for_code 200 do |url|
        owned_elements
        visit url, path_params: { map_id: owned_map.id }

        expect(response).to have_many defined :element
      end
    end

    on_get('/api/patches/:patch_id/elements', 'List elements in a patch') do
      path_params fields: { patch_id: { type: :integer, description: 'Target patch identifier' } }

      for_code 200 do |url|
        owned_elements
        visit url, path_params: { patch_id: owned_patch.id }

        expect(response).to have_many defined :element
      end
    end

    on_get('/api/elements/:id', 'Show one element') do
      path_params defined: :path_params

      for_code 200 do |url|
        visit url, path_params: { id: owned_element.id }

        expect(response).to have_one defined :element
      end

      for_code 404 do |url|
        visit url, path_params: { id: 0 }

        expect(response).to have_one defined :error
      end
    end

    on_post('/api/elements', 'Create new element') do
      request_params defined: :create_update_payload
      let(:element_attributes) do
        resource = FactoryBot.create :resource
        FactoryBot.build(:element, patch: owned_patch, resource: resource).attributes
      end

      for_code 201 do |url|
        visit url, payload: element_attributes

        expect(response).to have_one defined :element
      end

      for_code 422 do |url|
        visit url, payload: { geometry: {} }
      end
    end

    on_put('/api/elements/:id', 'Update a element') do
      path_params defined: :path_params
      request_params defined: :create_update_payload

      for_code 200 do |url|
        visit url, path_params: { id: owned_element.id }, payload: { implanted_at: Time.current }
      end

      for_code 422 do |url|
        visit url, path_params: { id: owned_element.id }, payload: { geometry: {} }
      end
    end

    on_delete('/api/elements/:id', 'Destroys a element') do
      path_params defined: :path_params

      for_code 204 do |url|
        visit url, path_params: { id: owned_element.id }
      end
    end
  end
end

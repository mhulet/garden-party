require 'rails_helper'

RSpec.describe Patch, type: :model do
  context 'when creating a patch' do
    let(:map) { FactoryBot.create :map }
    let(:first_element) { FactoryBot.build :element, patch: nil }
    let(:second_element) { FactoryBot.build :element, patch: nil }

    context 'when there are no associated elements' do
      let(:geometry) { FactoryBot.build(:patch, :point).geometry }

      it 'is invalid' do
        patch = described_class.new map: map, geometry: geometry
        expect(patch).not_to be_valid
      end
    end

    context 'when it is a point with more than one associated element' do
      let(:geometry) { FactoryBot.build(:patch, :point).geometry }

      it 'is invalid' do
        patch = described_class.new map: map, geometry: geometry, elements: [first_element, second_element]
        expect(patch).not_to be_valid
      end
    end

    context 'when it is a point with one associated element' do
      let(:geometry) { FactoryBot.build(:patch, :point).geometry }
      let(:patch) { described_class.new map: map, geometry: geometry, elements: [first_element], layer: map.layers.first }

      it 'is valid' do
        expect(patch).to be_valid
      end

      it 'creates the associated elements on creation' do
        expect do
          patch.save!
        end.to change(Element, :count).by(1)
      end
    end

    context 'when it is a polygon with associated elements' do
      let(:geometry) { FactoryBot.build(:patch, :polygon).geometry }
      let(:patch) { described_class.new map: map, geometry: geometry, elements: [first_element, second_element], layer: map.layers.first }

      it 'is valid' do
        expect(patch).to be_valid
      end

      it 'creates the associated elements on creation' do
        expect do
          patch.save!
        end.to change(Element, :count).by(2)
      end
    end

    context 'when it is a circle with associated elements' do
      let(:geometry) { FactoryBot.build(:patch, :circle).geometry }
      let(:patch) { described_class.new map: map, geometry: geometry, elements: [first_element, second_element], layer: map.layers.first }

      it 'is valid' do
        expect(patch).to be_valid
      end

      it 'creates the associated elements on creation' do
        expect do
          patch.save!
        end.to change(Element, :count).by(2)
      end
    end
  end
end

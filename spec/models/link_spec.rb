require 'rails_helper'

RSpec.describe Link, type: :model do
  include ActiveJob::TestHelper

  context 'when user creates link' do
    let(:user) { FactoryBot.create :user }

    # An admin is already created by "config.before(:suite)" in rails_helper.rb
    it 'sends an email to administrators' do
      expect do
        perform_enqueued_jobs do
          FactoryBot.create :link, user: user
        end
      end.to change(ActionMailer::Base.deliveries, :count).by(1)
    end
  end

  context 'when administrator creates links' do
    let(:user) { User.admins.first }

    it 'does not send an email to other administrators' do
      expect do
        perform_enqueued_jobs do
          FactoryBot.create :link, user: user
        end
      end.to change(ActionMailer::Base.deliveries, :count).by(0)
    end

    it 'approves the link' do
      link = FactoryBot.create :link, user: user
      expect(link.approved_by).to eq user
    end
  end
end

require 'rails_helper'

RSpec.describe Map, type: :model do
  context 'when validating data' do
    context 'with a string as center' do
      let(:map) { described_class.new center: 'hello' }

      it 'does not validate center' do
        map.validate
        expect(map.errors[:center].count).to eq 1
      end

      it 'does not throw an exception' do
        expect do
          map.validate
        end.not_to raise_exception
      end
    end

    context 'with a hash as center' do
      let(:map) { described_class.new center: { x: 10, y: 10 } }

      it 'does not validate center' do
        map.validate
        expect(map.errors[:center].count).to eq 1
      end
    end
  end

  context 'when uploading a map' do
    context 'with a SVG map' do
      it 'cleans the file' do # rubocop:disable RSpec/ExampleLength
        Rails.configuration.garden_party.maps[:allowed_picture_mime_types] << 'image/svg+xml'

        expectation = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 210 297" height="297mm" width="210mm">' \
                      '<circle r="40" cy="148" cx="155" fill="#fff" stroke="red"/>' \
                      '<text x="10" y="155" font-size="10">XML entity</text>' \
                      '<foreignObject class="node" y="148" x="155" width="100" height="100">' \
                      '<body xmlns="http://www.w3.org/1999/xhtml"><a>click here</a></body>' \
                      '</foreignObject>' \
                      '</svg>'
        map         = FactoryBot.create :map, :dangerous
        map.reload

        expect(map.picture.download).to eq expectation
      end

      # NOTE: this applies to all uploaded maps, not only SVG's.
      context 'with no extent' do
        it 'fails to validate' do
          map = FactoryBot.build :map, :user_submitted, extent_height: nil, extent_width: nil
          expect(map).not_to be_valid
        end

        it 'fails because of extent' do
          map = FactoryBot.build :map, :user_submitted, extent_height: nil, extent_width: nil
          map.validate
          expect(map.errors.attribute_names).to contain_exactly :extent_width, :extent_height
        end
      end
    end

    context 'with a text file' do
      it 'does not validate' do
        map = FactoryBot.build :map, :unsupported

        expect do
          map.validate!
        end.to raise_error(/n'est pas un format autorisé/)
      end
    end

    context 'with a jpg file' do
      it 'does saves the image' do
        map = FactoryBot.build :map, :jpg
        expect do
          map.save!
        end.not_to raise_error
      end
    end
  end
end

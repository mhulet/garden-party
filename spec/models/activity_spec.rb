require 'rails_helper'

RSpec.describe Activity, type: :model do
  let(:activity_with_800px_attachments) do
    Rails.configuration.garden_party.activities[:largest_pictures_size] = 800
    FactoryBot.create :activity, :with_pictures
  end

  it 'downsizes attached pictures' do
    picture = activity_with_800px_attachments.pictures.first
    picture.analyze

    expect(picture.metadata[:width]).to eq Rails.configuration.garden_party.activities[:largest_pictures_size]
  end

  it 'does not resize previously attached pictures' do
    activity_with_800px_attachments

    Rails.configuration.garden_party.activities[:largest_pictures_size] = 1024
    activity_with_800px_attachments.pictures.attach [Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'activity_upload.png'), 'image/png')]
    activity_with_800px_attachments.pictures.each(&:analyze)

    expect(activity_with_800px_attachments.pictures.first.metadata[:width]).to eq 800
  end
end

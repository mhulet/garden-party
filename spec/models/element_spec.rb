require 'rails_helper'

RSpec.describe Element, type: :model do
  let(:yesterday) { Time.current.beginning_of_day - 1.day }

  # rubocop:disable RSpec/MultipleExpectations
  describe 'validation' do
    it 'cannot be planned for removal without being implanted' do
      element = FactoryBot.build :element, removal_planned_for: Time.current
      expect(element).not_to be_valid
      expect(element.errors[:implanted_at]).not_to be_blank
    end

    it 'cannot be removed without being implanted' do
      element = FactoryBot.build :element, removed_at: Time.current
      expect(element).not_to be_valid
      expect(element.errors[:implanted_at]).not_to be_blank
    end

    it 'cannot be planned for removal before being implanted' do
      element = FactoryBot.build :element, implanted_at: Time.current, removal_planned_for: yesterday
      expect(element).not_to be_valid
      expect(element.errors[:removal_planned_for]).not_to be_blank
    end

    it 'cannot be removed before being implanted' do
      element = FactoryBot.build :element, implanted_at: Time.current, removed_at: yesterday
      expect(element).not_to be_valid
      expect(element.errors[:removed_at]).not_to be_blank
    end
  end
  # rubocop:enable RSpec/MultipleExpectations

  describe '.status' do
    context 'when element was just added to a crop' do
      let(:element) { FactoryBot.build :element }

      it('is "planned"') { expect(element.status).to eq :planned }
    end

    context 'when element has a planned implantation date' do
      let(:element) { FactoryBot.build :element, implantation_planned_for: Time.current }

      it('is "planned"') { expect(element.status).to eq :planned }
    end

    context 'when element is implanted' do
      context 'without a planned implantation date' do
        let(:element) { FactoryBot.build :element, implanted_at: Time.current }

        it('is "implanted"') { expect(element.status).to eq :implanted }
      end

      context 'with a planned implantation date' do
        let(:element) { FactoryBot.build :element, implantation_planned_for: yesterday, implanted_at: Time.current }

        it('is "implanted"') { expect(element.status).to eq :implanted }
      end
    end

    context 'when element is planned for removal' do
      let(:element) { FactoryBot.build :element, implanted_at: yesterday, removal_planned_for: Time.current }

      it('is "implanted"') { expect(element.status).to eq :implanted }
    end

    context 'when element is removed' do
      let(:element) { FactoryBot.build :element, implanted_at: yesterday, removed_at: Time.current }

      it('is "removed"') { expect(element.status).to eq :removed }
    end
  end
end

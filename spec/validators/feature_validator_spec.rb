require 'rails_helper'

# FIXME: schema validator errors are not localized
RSpec.describe FeatureValidator do
  let(:model_all) do
    Class.new do
      include ActiveModel::Model
      attr_accessor :geometry

      validates :geometry, feature: { types: %w[Circle LineString Point Polygon] }
    end
  end
  let(:model_point) do
    Class.new do
      include ActiveModel::Model
      attr_accessor :geometry

      validates :geometry, feature: { types: %w[Point] }
    end
  end

  let(:geometry_point) { { 'type' => 'Feature', 'properties' => nil, 'geometry' => { 'type' => 'Point', 'coordinates' => [rand(100), rand(100)] } } }
  let(:geometry_circle) { { 'type' => 'Feature', 'properties' => { 'radius' => 10 }, 'geometry' => { 'type' => 'Point', 'coordinates' => [rand(100), rand(100)] } } }
  let(:geometry_polygon) do
    coordinates = []
    coordinates << [rand(100), rand(100)]
    coordinates << [coordinates[0][0] + rand(100), coordinates[0][1] + rand(10)]
    coordinates << [coordinates[0][1] - rand(50), coordinates[0][0] - rand(50)]
    # Last point MUST be at the same coordinates as the first to close the ring
    coordinates << coordinates[0]

    { 'type' => 'Feature', 'properties' => nil, 'geometry' => { 'type' => 'Polygon', 'coordinates' => [coordinates] } }
  end

  context 'when feature is invalid geoJSON feature' do
    it 'does not validate' do
      entity = model_all.new
      entity.validate

      expect(entity.errors[:geometry].first).to match(/did not match the following type: object/)
    end
  end

  context 'when feature is a Point' do
    context "when feature don't have enough coordinates" do
      it 'does not validates' do
        entity = model_all.new geometry: geometry_point
        entity.geometry['geometry']['coordinates'] = []
        entity.validate

        # FIXME:  Schema validator will fail on closest match, which brings weird errors
        expect(entity.errors[:geometry].first).to match(/did not match one of the following values: Polygon/)
      end
    end
  end

  context 'when feature is a circle' do
    context "when feature don't have enough coordinates" do
      it 'does not validates' do
        entity = model_all.new geometry: geometry_circle
        entity.geometry['geometry']['coordinates'] = []
        entity.validate

        # FIXME:  Schema validator will fail on closest match, which brings weird errors
        expect(entity.errors[:geometry].first).to match(/did not match one of the following values: Polygon/)
      end
    end
  end

  context 'when feature is a Polygon' do
    context 'when feature has a "radius" property' do
      it 'does not validates' do
        entity = model_all.new geometry: geometry_polygon
        entity.geometry['properties'] = { 'radius' => nil }
        entity.validate

        expect(entity.errors[:geometry].first).to match(/of type object did not match/)
      end
    end

    context "when feature don't have enough coordinates" do
      it 'does not validates' do
        entity = model_all.new geometry: geometry_polygon
        entity.geometry['geometry']['coordinates'] = [[[0, 0], [10, 10], [0, 0]]]
        entity.validate

        expect(entity.errors[:geometry].first).to match(/did not contain a minimum number of items 4/)
      end
    end

    context 'when feature is not closed' do
      it 'does not validates' do
        entity = model_all.new geometry: geometry_polygon
        entity.geometry['geometry']['coordinates'] = [[[0, 0], [10, 10], [20, 20], [30, 30]]]
        entity.validate

        expect(entity.errors[:geometry].first).to match(/n'est pas fermé/)
      end
    end
  end

  context 'when there is a list of allowed types' do
    context 'when the type matches' do
      it 'validates' do
        entity = model_point.new geometry: geometry_point
        entity.validate

        expect(entity.errors.count).to eq 0
      end
    end

    context 'when the type does not match' do
      it 'does not validate' do
        entity = model_point.new geometry: geometry_circle
        expect(entity).not_to be_valid
      end
    end
  end
end

require 'rails_helper'
require 'garden_party/geometry'

RSpec.describe GardenParty::Geometry do
  describe '#scale' do
    let(:entity) { { 'geometry' => { 'coordinates' => set } } }
    let(:circle) { { 'geometry' => { 'coordinates' => [10, 10] }, 'properties' => { 'radius' => 100 } } }

    context 'with an array of points' do
      let(:set) { [20, 10, 15] }

      it 'downscales values in array' do
        result = described_class.rescale(entity, 0.5)
        expect(result['geometry']['coordinates']).to eq [10.0, 5.0, 7.5]
      end
    end

    context 'with an array of arrays' do
      let(:set) { [[20, 10], [10, 30], [15, 8]] }

      it 'downscales values in arrays' do
        result = described_class.rescale(entity, 0.5)
        expect(result['geometry']['coordinates']).to eq [[10.0, 5.0], [5.0, 15.0], [7.5, 4]]
      end
    end

    context 'with a circle' do
      it 'downscales center' do
        result = described_class.rescale(circle, 0.5)
        expect(result['properties']['radius']).to eq 50
      end
    end
  end
end

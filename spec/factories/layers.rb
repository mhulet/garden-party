FactoryBot.define do
  factory :layer do
    name { Faker::Lorem.sentence(word_count: rand(1...3)).tr('.', '') }
    map
  end
end

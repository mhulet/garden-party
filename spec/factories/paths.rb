FactoryBot.define do
  factory :path do
    base_color = Faker::Color.rgb_color.join(',')

    name { 'A path' }
    stroke_color { "#{base_color},1" }
    background_color { "#{base_color},0.2" }
    stroke_width { 1.5 }
    map
    layer_id { map.layers.first.id }
    # Circle
    geometry { { type: 'Feature', properties: { radius: rand(100) }, geometry: { type: 'Point', coordinates: [rand(100), rand(100)] } } }
  end
end

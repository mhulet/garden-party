FactoryBot.define do
  factory :resource do
    name { Faker::Lorem.sentence(word_count: rand(1...3)).tr('.', '') }
    description { Faker::Markdown.sandwich }
    common_names { [] }
    genus

    trait :with_parent do
      association :parent, factory: :resource
    end
  end
end

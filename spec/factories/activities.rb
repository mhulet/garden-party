FactoryBot.define do
  factory :activity do
    name { Faker::Verb.ing_form }
    # Traits
    for_element
    done

    trait :done do
      done_at { Time.current }
    end

    trait :planned do
      planned_for { Time.current + 1.day }
      done_at { nil }
    end

    trait :for_element do
      association :subject, factory: :element
    end

    trait :for_patch do
      association :subject, factory: :patch
    end

    trait :with_pictures do
      pictures do
        [
          Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'activity_upload.png'), 'image/png'),
        ]
      end
    end
  end
end

FactoryBot.define do
  factory :patch do
    map
    # FIXME: patches factories creates elements; elements factories creates patches...
    #        It makes tests harder to setup and understand.
    elements { [build(:element, patch: nil)] }
    layer_id { map.layers.first.id }
    # default trait
    point

    trait :point do
      geometry { { type: 'Feature', properties: nil, geometry: { type: 'Point', coordinates: [rand(100), rand(100)] } } }
    end

    trait :polygon do
      geometry do
        coordinates = []
        coordinates << [rand(100), rand(100)]
        coordinates << [coordinates[0][0] + rand(100), coordinates[0][1] + rand(10)]
        coordinates << [coordinates[0][1] - rand(50), coordinates[0][0] - rand(50)]
        # Last point MUST be at the same coordinates as the first to close the ring
        coordinates << coordinates[0]

        { type: 'Feature', properties: nil, geometry: { type: 'Polygon', coordinates: [coordinates] } }
      end
    end

    trait :circle do
      geometry { { type: 'Feature', properties: { radius: rand(50) }, geometry: { type: 'Point', coordinates: [rand(100), rand(100)] } } }
    end
  end
end

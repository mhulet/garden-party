FactoryBot.define do
  factory :map do
    name { Faker::Lorem.sentence(word_count: rand(2...5)).tr('.', '') }
    center { [rand(10_000), rand(10_000)] }
    user

    trait :user_submitted do
      center { [rand(1000), rand(700)] }
      picture { Rack::Test::UploadedFile.new Rails.root.join('spec', 'fixtures', 'files', 'map.svg'), 'image/svg+xml' }
      extent_width { 1377 }
      extent_height { 1944 }
    end

    trait :user_submitted_png do
      center { [rand(1000), rand(700)] }
      picture { Rack::Test::UploadedFile.new Rails.root.join('spec', 'fixtures', 'files', 'map.png'), 'image/png' }
      extent_width { 1377 }
      extent_height { 1944 }
    end

    trait :dangerous do
      center { [rand(1000), rand(700)] }
      picture { Rack::Test::UploadedFile.new Rails.root.join('spec', 'fixtures', 'files', 'dangerous_map.svg'), 'image/svg+xml' }
      extent_width { 100 }
      extent_height { 100 }
    end

    trait :jpg do
      center { [rand(1000), rand(700)] }
      picture { Rack::Test::UploadedFile.new Rails.root.join('spec', 'fixtures', 'files', 'map.jpg'), 'image/jpeg' }
      extent_width { 240 }
      extent_height { 240 }
    end

    trait :unsupported do
      center { [rand(1000), rand(700)] }
      picture { Rack::Test::UploadedFile.new Rails.root.join('spec', 'fixtures', 'files', 'map.txt'), 'plain/text' }
      extent_width { 100 }
      extent_height { 100 }
    end

    trait :publicly_available do
      publicly_available { true }
    end
  end
end

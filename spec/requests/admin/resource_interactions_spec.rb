require 'rails_helper'

RSpec.describe '/admin/resource_interactions', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:resource_interaction).attributes
  end
  let(:invalid_attributes) do
    { nature: nil }
  end

  include_context 'with authenticated admin'

  describe 'GET /admin/resource_interactions' do
    it 'returns a success response' do
      ResourceInteraction.create! valid_attributes
      get admin_resource_interactions_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/resource_interactions/1' do
    it 'returns a success response' do
      resource_interaction = ResourceInteraction.create! valid_attributes
      get admin_resource_interaction_url(resource_interaction)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'returns a success response' do
      get new_admin_resource_interaction_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/resource_interactions/1/edit' do
    it 'returns a success response' do
      resource_interaction = ResourceInteraction.create! valid_attributes
      get edit_admin_resource_interaction_url(resource_interaction)
      expect(response).to be_successful
    end
  end

  describe 'POST /admin/resource_interactions' do
    context 'with valid params' do
      it 'creates a new ResourceInteraction' do
        expect do
          post admin_resource_interactions_url, params: { resource_interaction: valid_attributes }
        end.to change(ResourceInteraction, :count).by(1)
      end

      it 'redirects to the created ResourceInteraction' do
        post admin_resource_interactions_url, params: { resource_interaction: valid_attributes }
        expect(response).to redirect_to(admin_resource_interaction_path(ResourceInteraction.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post admin_resource_interactions_url, params: { resource_interaction: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT /admin/resource_interactions/1' do
    let(:resource_interaction) { ResourceInteraction.create! valid_attributes }

    context 'with valid params' do
      let(:new_attributes) do
        { notes: 'This actually happens' }
      end

      it 'updates the requested admin_resource_interaction' do
        put admin_resource_interaction_url(resource_interaction), params: { resource_interaction: new_attributes }
        resource_interaction.reload
        expect(resource_interaction.notes).to eq new_attributes[:notes]
      end

      it 'redirects to the admin_resource_interaction' do
        put admin_resource_interaction_url(resource_interaction), params: { resource_interaction: new_attributes }
        expect(response).to redirect_to(admin_resource_interaction_path(ResourceInteraction.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put admin_resource_interaction_url(resource_interaction), params: { resource_interaction: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /admin/resource_interactions/1' do
    it 'destroys the requested admin_resource_interaction' do
      resource_interaction = ResourceInteraction.create! valid_attributes
      expect do
        delete admin_resource_interaction_url(resource_interaction)
      end.to change(ResourceInteraction, :count).by(-1)
    end

    it 'redirects to the admin_resource_interactions list' do
      resource_interaction = ResourceInteraction.create! valid_attributes
      delete admin_resource_interaction_url(resource_interaction)
      expect(response).to redirect_to(admin_resource_interactions_url)
    end
  end
end

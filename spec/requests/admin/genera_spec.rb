require 'rails_helper'

RSpec.describe '/admin/genera', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:genus).attributes
  end
  let(:invalid_attributes) do
    { name: '' }
  end

  include_context 'with authenticated admin'

  describe 'GET /admin/genera' do
    it 'returns a success response' do
      Genus.create! valid_attributes
      get admin_genera_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/genera/1' do
    it 'returns a success response' do
      genus = Genus.create! valid_attributes
      get admin_genus_url(genus)
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_admin_genus_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/genera/1/edit' do
    it 'returns a success response' do
      genus = Genus.create! valid_attributes
      get edit_admin_genus_url(genus)
      expect(response).to be_successful
    end
  end

  describe 'POST /admin/genera' do
    context 'with valid params' do
      it 'creates a new Genus' do
        expect do
          post admin_genera_url, params: { genus: valid_attributes }
        end.to change(Genus, :count).by(1)
      end

      it 'redirects to the created admin_genus' do
        post admin_genera_url, params: { genus: valid_attributes }
        expect(response).to redirect_to(admin_genus_path(Genus.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post admin_genera_url, params: { genus: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT /admin/genera/1' do
    let(:genus) { Genus.create! valid_attributes }

    context 'with valid params' do
      let(:new_attributes) do
        { name: 'New name' }
      end

      it 'updates the requested admin_genus' do
        put admin_genus_url(genus), params: { genus: new_attributes }
        genus.reload
        expect(genus.name).to eq new_attributes[:name]
      end

      it 'redirects to the admin_genus' do
        put admin_genus_url(genus), params: { genus: new_attributes }
        expect(response).to redirect_to(admin_genus_path(Genus.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put admin_genus_url(genus), params: { genus: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /admin/genera/1' do
    it 'destroys the requested admin_genus' do
      genus = Genus.create! valid_attributes
      expect do
        delete admin_genus_url(genus)
      end.to change(Genus, :count).by(-1)
    end

    it 'redirects to the admin_genera list' do
      genus = Genus.create! valid_attributes
      delete admin_genus_url(genus)
      expect(response).to redirect_to(admin_genera_url)
    end
  end
end

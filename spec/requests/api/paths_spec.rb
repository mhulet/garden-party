require 'rails_helper'

RSpec.describe '/paths', type: :request do
  let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
  let(:valid_attributes) do
    FactoryBot.build(:path, map: owned_map).attributes
  end

  let(:invalid_attributes) do
    valid_attributes.merge 'geometry' => {}
  end

  # This should return the minimal set of values that should be in the headers
  # in order to pass any filters (e.g. authentication) defined in
  # PathsController, or in your router and rack
  # middleware. Be sure to keep this updated too.
  let(:valid_headers) do
    {}
  end

  include_context 'with authenticated member'

  describe 'GET /index' do
    it 'renders a successful response' do
      the_path = FactoryBot.create :path, map: owned_map
      get api_map_paths_url(the_path.map_id), headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      the_path = FactoryBot.create :path, map: owned_map
      get api_path_url(the_path), as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Path' do
        expect do
          post api_paths_url, params: { path: valid_attributes }, headers: valid_headers, as: :json
        end.to change(Path, :count).by(1)
      end

      it 'renders an successful response' do
        post api_paths_url, params: { path: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
      end

      it 'render the new path as JSON' do
        post api_paths_url, params: { path: valid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Path' do
        expect do
          post api_paths_url, params: { path: invalid_attributes }, as: :json
        end.to change(Path, :count).by(0)
      end

      it 'renders an error response' do
        post api_paths_url, params: { path: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'render the error as JSON' do
        post api_paths_url, params: { path: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) do
        { name: 'New path name' }
      end

      it 'updates the requested path' do
        the_path = FactoryBot.create :path, map: owned_map
        patch api_path_url(the_path), params: { path: new_attributes }, headers: valid_headers, as: :json
        the_path.reload
        expect(the_path.name).to eq new_attributes[:name]
      end

      it 'renders a JSON response with the path' do
        the_path = FactoryBot.create :path, map: owned_map
        patch api_path_url(the_path), params: { path: new_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:ok)
      end

      it 'renders the path as JSON' do
        the_path = FactoryBot.create :path, map: owned_map
        patch api_path_url(the_path), params: { path: new_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end

    context 'with invalid parameters' do
      it 'renders an error response' do
        the_path = FactoryBot.create :path, map: owned_map
        patch api_path_url(the_path), params: { path: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'render the error as JSON' do
        the_path = FactoryBot.create :path, map: owned_map
        patch api_path_url(the_path), params: { path: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested path' do
      the_path = FactoryBot.create :path, map: owned_map
      expect do
        delete api_path_url(the_path), headers: valid_headers, as: :json
      end.to change(Path, :count).by(-1)
    end
  end
end

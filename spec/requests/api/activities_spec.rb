require 'rails_helper'

RSpec.describe '/api/activities', type: :request do
  let(:owned_element) do
    map   = FactoryBot.create :map, user: signed_in_user
    patch = FactoryBot.create :patch, map: map
    patch.elements.first
  end
  let(:valid_attributes) do
    FactoryBot.build(:activity, subject: owned_element).attributes
  end

  let(:invalid_attributes) do
    valid_attributes.merge! 'planned_for' => nil, 'done_at' => nil
  end

  let(:valid_headers) do
    {}
  end

  include_context 'with authenticated member'

  describe 'GET /api/elements/1/activities' do
    it 'renders a successful response' do
      activity = Activity.create! valid_attributes
      get api_element_activities_url(element_id: activity.subject_id), headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /api/activities/1' do
    it 'renders a successful response' do
      activity = Activity.create! valid_attributes
      get api_activity_url(activity), as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /api/activities' do
    context 'with valid parameters' do
      it 'creates a new Activity' do
        expect do
          post api_activities_url, params: { activity: valid_attributes }, headers: valid_headers, as: :json
        end.to change(Activity, :count).by(1)
      end

      it 'renders an successful response' do
        post api_activities_url, params: { activity: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
      end

      it 'renders the new Activity as JSON' do
        post api_activities_url, params: { activity: valid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Activity' do
        expect do
          post api_activities_url, params: { activity: invalid_attributes }, as: :json
        end.to change(Activity, :count).by(0)
      end

      it 'renders an error response' do
        post api_activities_url, params: { activity: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'renders the error as JSON' do
        post api_activities_url, params: { activity: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'PUT /api/activities/1' do
    context 'with valid parameters' do
      let(:new_attributes) do
        { notes: 'Something new' }
      end

      it 'updates the requested Activity' do
        activity = Activity.create! valid_attributes
        patch api_activity_url(activity), params: { activity: new_attributes }, headers: valid_headers, as: :json
        activity.reload
        expect(activity.notes).to eq new_attributes[:notes]
      end

      it 'renders a success response' do
        activity = Activity.create! valid_attributes
        patch api_activity_url(activity), params: { activity: new_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:ok)
      end

      it 'renders a JSON response with the activity' do
        activity = Activity.create! valid_attributes
        patch api_activity_url(activity), params: { activity: new_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'renders an error response' do
        activity = Activity.create! valid_attributes
        patch api_activity_url(activity), params: { activity: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'renders the error as JSON' do
        activity = Activity.create! valid_attributes
        patch api_activity_url(activity), params: { activity: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'GET /api/activities/1/pictures/1' do
    context 'with an existing picture' do
      it 'redirects to the picture' do
        activity = FactoryBot.create :activity, :with_pictures, subject: owned_element
        get picture_api_activity_url(activity, activity.pictures.first)
        expect(response).to be_redirection
      end
    end
  end

  describe 'DELETE /api/activities/1' do
    it 'destroys the requested activity' do
      activity = Activity.create! valid_attributes
      expect do
        delete api_activity_url(activity), headers: valid_headers, as: :json
      end.to change(Activity, :count).by(-1)
    end
  end
end

require 'rails_helper'

RSpec.describe '/links', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:link).attributes
  end
  let(:invalid_attributes) do
    { title: nil }
  end

  describe 'GET /links' do
    it 'returns a success response' do
      Link.create! valid_attributes
      get links_url
      expect(response).to be_successful
    end
  end

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:valid_attributes) do
      FactoryBot.build(:link, user: signed_in_user).attributes
    end

    describe 'GET /new' do
      it 'returns a success response' do
        get new_link_url
        expect(response).to be_successful
      end
    end

    describe 'GET /links/1/edit' do
      it 'returns a success response' do
        link = Link.create! valid_attributes
        get edit_link_url(link)
        expect(response).to be_successful
      end
    end

    describe 'POST /links' do
      context 'with valid params' do
        it 'creates a new Link' do
          expect do
            post links_url, params: { link: valid_attributes }
          end.to change(Link, :count).by(1)
        end

        it 'redirects to the links list' do
          post links_url, params: { link: valid_attributes }
          expect(response).to redirect_to(links_path)
        end
      end

      context 'with invalid params' do
        it "returns a success response (i.e. to display the 'new' template)" do
          post links_url, params: { link: invalid_attributes }
          expect(response).to be_successful
        end
      end
    end

    describe 'PUT /links/1' do
      let(:link) { Link.create! valid_attributes }

      context 'with valid params' do
        let(:new_attributes) do
          { title: 'New link title' }
        end

        it 'updates the requested link' do
          put link_url(link), params: { link: new_attributes }
          link.reload
          expect(link.title).to eq new_attributes[:title]
        end

        it 'redirects to the links list' do
          put link_url(link), params: { link: new_attributes }
          expect(response).to redirect_to(links_url)
        end
      end

      context 'with invalid params' do
        it "returns a success response (i.e. to display the 'edit' template)" do
          put link_url(link), params: { link: invalid_attributes }
          expect(response).to be_successful
        end
      end
    end

    describe 'DELETE /links/1' do
      it 'destroys the requested link' do
        link = Link.create! valid_attributes
        expect do
          delete link_url(link)
        end.to change(Link, :count).by(-1)
      end

      it 'redirects to the links list' do
        link = Link.create! valid_attributes
        delete link_url(link)
        expect(response).to redirect_to(links_url)
      end
    end
  end
end

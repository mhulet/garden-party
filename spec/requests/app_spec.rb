require 'rails_helper'

RSpec.describe '/', type: :request do
  describe 'GET /' do
    it 'returns a success response' do
      get root_url
      expect(response).to be_successful
    end
  end

  context 'with authenticated member' do
    include_context 'with authenticated member'

    describe 'GET /app' do
      it 'returns a success response' do
        get app_url
        expect(response).to be_successful
      end
    end
  end

  describe 'GET /shared/maps/1' do
    it 'returns a success response' do
      map = FactoryBot.create :map, :publicly_available
      get shared_map_url(map)
      expect(response).to be_successful
    end
  end
end

attributes :id, :name, :description, :source, :common_names, :edible, :picture_name, :parent_id, :layer_id, :genus_id, :created_at, :updated_at

node(:fill_color) { |l| "rgba(#{l.color},0.2)" }
node(:border_color) { |l| "rgb(#{l.color})" }

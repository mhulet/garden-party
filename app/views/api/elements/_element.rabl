attributes :id, :name,
           :resource_id, :patch_id,
           :implanted_at, :implantation_planned_for,
           :removed_at, :removal_planned_for,
           :status,
           :created_at, :updated_at

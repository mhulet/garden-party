attributes :id, :name, :notes, :planned_for, :done_at, :subject_id, :subject_type, :created_at, :updated_at

node(:pictures) do |activity|
  list = []
  activity.pictures.each do |picture|
    list << {
      source:    picture_api_activity_url(activity, picture),
      thumbnail: picture_thumbnail_api_activity_url(activity, picture),
    }
  end

  list
end

object false

elements = Element.where patch: @map.patches.pluck(:id)

code :map do |_m|
  partial 'api/maps/map', object: @map
end

code :patches do |_m|
  partial 'api/patches/index', object: @map.patches
end

code :paths do |_m|
  partial 'api/paths/index', object: @map.paths
end

code :layers do |_m|
  partial 'api/layers/index', object: @map.layers
end

code :elements do |_m|
  partial 'api/elements/index', object: elements
end

code :resources do |_m|
  ids = elements.pluck(:resource_id).uniq
  partial 'api/resources/index', object: Resource.where(id: ids)
end

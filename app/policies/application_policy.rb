class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    logged_in?
  end

  def new?
    create?
  end

  def update?
    logged_in?
  end

  def edit?
    update?
  end

  def destroy?
    logged_in?
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end

  private

  def logged_in?
    @user.present?
  end

  def admin?
    logged_in? && @user.admin?
  end

  def owner?
    @record.user_id == @user&.id
  end
end

module Api
  class GenusPolicy < ApiApplicationPolicy
    def destroy?
      admin?
    end

    class Scope < Scope
      def resolve
        return Genus.none if @user.blank?

        scope.all
      end
    end
  end
end

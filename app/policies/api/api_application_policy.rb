module Api
  class ApiApplicationPolicy < ApplicationPolicy
    def index?
      logged_in?
    end

    def show?
      logged_in?
    end

    def create?
      logged_in?
    end

    def update?
      logged_in?
    end

    def destroy?
      logged_in?
    end
  end
end

module Api
  class ElementPolicy < ApiApplicationPolicy
    def create?
      map_owner?
    end

    def show?
      map_owner?
    end

    def update?
      map_owner?
    end

    def destroy?
      map_owner?
    end

    class Scope < Scope
      def resolve
        return Element.none if @user.blank?

        map_ids   = Map.where(user: @user).pluck :id
        patch_ids = Patch.where(map_id: map_ids).pluck :id
        scope.where(patch_id: patch_ids)
      end
    end

    private

    def map_owner?
      return false unless logged_in?

      @record&.patch&.map&.user == @user
    end
  end
end

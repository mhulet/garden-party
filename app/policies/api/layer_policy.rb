module Api
  class LayerPolicy < ApiApplicationPolicy
    def create?
      map_owner?
    end

    def show?
      map_owner?
    end

    def update?
      map_owner?
    end

    def destroy?
      map_owner?
    end

    class Scope < Scope
      def resolve
        return Layer.none if @user.blank?

        map_ids = Map.where(user: @user).pluck :id
        scope.where(map: map_ids)
      end
    end

    def map_owner?
      return false unless logged_in?

      @record&.map&.user == @user
    end
  end
end

module Api
  class ActivityPolicy < ApiApplicationPolicy
    def create?
      map_owner?
    end

    def show?
      map_owner?
    end

    def update?
      map_owner?
    end

    def destroy?
      map_owner?
    end

    def picture_thumbnail?
      map_owner? || publicly_available?
    end

    def picture?
      picture_thumbnail?
    end

    class Scope < Scope
      # Returning all activities by default; authorization must be made in
      # controller on an higher entity (as the map).
      def resolve
        scope.all
      end
    end

    private

    def map_owner?
      return false unless logged_in?

      @record&.subject&.map_owner == @user
    end

    def publicly_available?
      case @record.subject_type
      when 'Element'
        @record&.subject&.patch&.map&.publicly_available
      else
        false
      end
    end
  end
end

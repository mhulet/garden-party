module Api
  class ResourcePolicy < ApiApplicationPolicy
    def destroy?
      admin?
    end

    class Scope < Scope
      def resolve
        return Resource.none if @user.blank?

        scope.all
      end
    end
  end
end

module Api
  class PathPolicy < ApiApplicationPolicy
    def create?
      map_owner?
    end

    def show?
      map_owner?
    end

    def update?
      map_owner?
    end

    def destroy?
      map_owner?
    end

    class Scope < Scope
      def resolve
        return Path.none if @user.blank?

        map_ids = Map.where(user: @user).pluck :id
        scope.where(map: map_ids)
      end
    end

    private

    def map_owner?
      return false unless logged_in?

      @record&.map&.user == @user
    end
  end
end

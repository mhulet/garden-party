module Admin
  class ResourcePolicy < AdminApplicationPolicy
    class Scope < Scope
      def resolve
        scope.all
      end
    end
  end
end

module Admin
  class ResourceInteractionPolicy < AdminApplicationPolicy
    class Scope < Scope
      def resolve
        scope.all
      end
    end
  end
end

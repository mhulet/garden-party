module Api
  class PatchesController < ApiApplicationController
    before_action :set_patch, only: [:show, :update, :destroy]

    # GET /patches
    def index
      @patches = policy_scope(Patch).where(map_id: params[:map_id])

      render 'api/patches/index'
    end

    # GET /patches/1
    def show
      render 'api/patches/show'
    end

    # POST /patches
    def create
      prepare_new_patch
      authorize @patch

      if @patch.save
        render 'api/patches/show', status: :created, location: api_patch_url(@patch)
      else
        render json: @patch.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /patches/1
    def update
      if @patch.update(update_patch_params)
        render 'api/patches/show', location: api_patch_url(@patch)
      else
        render json: @patch.errors, status: :unprocessable_entity
      end
    end

    # DELETE /patches/1
    def destroy
      @patch.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_patch
      @patch = Patch.find(params[:id])

      authorize @patch
    end

    # Only allow a trusted parameter "white list" through.
    def create_patch_params
      patch_params %w[name map_id layer_id geometry elements]
    end

    def update_patch_params
      patch_params %w[name layer_id geometry]
    end

    def patch_params(allowed_fields)
      # Pick params one by one because validating geometry is an issue with polygons
      parameters = params.require(:patch).to_unsafe_hash
      values = {}
      allowed_fields.each { |key| values[key] = parameters[key] if parameters[key] }

      values
    end

    def prepare_new_patch
      parameters = create_patch_params
      parameters['elements'].map! { |element| Element.new element } if parameters['elements'].is_a? Array

      @patch = Patch.new(parameters)
    end
  end
end

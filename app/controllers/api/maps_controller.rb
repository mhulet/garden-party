module Api
  class MapsController < ApiApplicationController
    before_action :set_map, only: [:show, :picture, :update, :destroy]

    # GET /maps
    def index
      @maps = policy_scope Map

      render 'api/maps/index'
    end

    # GET /maps/1
    def show
      render 'api/maps/show'
    end

    # GET /maps/shared/1
    def shared
      @map = Map.find(params[:id])

      authorize @map
      render 'api/maps/shared'
    end

    # GET /maps/1/picture
    def picture
      redirect_to rails_blob_url(@map.picture)
    end

    # POST /maps
    def create
      authorize Map
      @map = Map.new(map_params)
      @map.user = current_user

      if @map.save
        render 'api/maps/show', status: :created, location: api_map_url(@map)
      else
        render json: @map.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /maps/1
    def update
      if @map.update(map_params)
        render 'api/maps/show', location: api_map_url(@map)
      else
        render json: @map.errors, status: :unprocessable_entity
      end
    end

    # DELETE /maps/1
    def destroy
      @map.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_map
      @map = Map.find(params[:id])

      authorize @map
    end

    # Only allow a trusted parameter "white list" through.
    def map_params
      params.require(:map).permit(:name, :picture, :extent_height, :extent_width, :publicly_available, center: [])
    end
  end
end

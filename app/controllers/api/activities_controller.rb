module Api
  class ActivitiesController < ApiApplicationController
    before_action :set_activity, only: [:show, :update, :picture, :picture_thumbnail, :destroy]

    # GET /api/activities
    def index
      authorize_map

      @activities = policy_scope(Activity)
      @activities = if params[:element_id]
                      @activities.for_element params[:element_id]
                    elsif params[:map_id]
                      @activities.for_map params[:map_id]
                    end

      @activities.where(done_at: nil) if params[:pending]

      render 'api/activities/index'
    end

    # GET /api/maps/:map_id/activities/names
    def names
      names = policy_scope(Activity).for_map(params[:map_id]).select(:name).distinct(:name).pluck :name
      render json: names
    end

    # GET /api/activities/1
    def show
      render 'api/activities/show'
    end

    # POST /api/activities
    def create
      @activity = Activity.new(create_activity_params)
      authorize @activity

      if @activity.save
        render 'api/activities/show', status: :created, location: api_activity_url(@activity)
      else
        render json: @activity.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/activities/1
    def update
      if @activity.update(update_activity_params)
        render 'api/activities/show', status: :ok, location: api_activity_url(@activity)
      else
        render json: @activity.errors, status: :unprocessable_entity
      end
    end

    # GET /api/activities/1/pictures/20
    def picture
      blob = @activity.pictures.find(params[:picture_id])

      redirect_to rails_blob_url(blob)
    end

    # GET /api/activities/1/pictures/20/thumbnail
    def picture_thumbnail
      blob = @activity.pictures.find(params[:picture_id])

      redirect_to blob.variant(resize_to_limit: [150, 85]).processed
    end

    # DELETE /api/activities/1
    def destroy
      @activity.destroy
    end

    private

    # Use callbacks to share common setup or constraints between activities.
    def set_activity
      @activity = Activity.find(params[:id])

      authorize @activity
    end

    # Only allow a trusted parameter "white list" through.
    def create_activity_params
      params.require(:activity).permit(:name, :notes, :planned_for, :done_at, :subject_id, :subject_type, pictures: [])
    end

    def update_activity_params
      params.require(:activity).permit(:notes, :planned_for, :done_at)
    end

    def authorize_map
      map = if params[:element_id]
              Element.find(params[:element_id])&.patch&.map
            elsif params[:map_id]
              Map.find params[:map_id]
            end

      authorize map, :owned_or_shared?
    end
  end
end

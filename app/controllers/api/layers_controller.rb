module Api
  class LayersController < ApiApplicationController
    before_action :set_layer, only: [:show, :update, :destroy]

    # GET /layers
    def index
      @layers = policy_scope(Layer).where map_id: params[:map_id]

      render 'api/layers/index'
    end

    # GET /layers/1
    def show
      render 'api/layers/show'
    end

    # POST /layers
    def create
      @layer = Layer.new(create_layer_params)
      authorize @layer

      if @layer.save
        render 'api/layers/show', status: :created, location: api_layer_url(@layer)
      else
        render json: @layer.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /layers/1
    def update
      if @layer.update(update_layer_params)
        render 'api/layers/show', location: api_layer_url(@layer)
      else
        render json: @layer.errors, status: :unprocessable_entity
      end
    end

    # DELETE /layers/1
    def destroy
      @layer.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_layer
      @layer = Layer.find(params[:id])

      authorize @layer
    end

    # Only allow a trusted parameter "white list" through.
    def create_layer_params
      params.require(:layer).permit(:name, :position, :map_id, :visible_by_default)
    end

    def update_layer_params
      params.require(:layer).permit(:name, :position, :visible_by_default)
    end
  end
end

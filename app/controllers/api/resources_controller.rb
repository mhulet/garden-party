module Api
  class ResourcesController < ApiApplicationController
    before_action :set_resource, only: [:show, :update, :destroy]
    before_action :set_paper_trail_whodunnit

    # GET /resources
    def index
      @resources = policy_scope Resource

      render 'api/resources/index'
    end

    # GET /resources/1
    def show
      render 'api/resources/show'
    end

    # POST /resources
    def create
      authorize Resource
      @resource = Resource.new(resource_params)

      if @resource.save
        render 'api/resources/show', status: :created, location: api_resource_url(@resource)
      else
        render json: @resource.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /resources/1
    def update
      if @resource.update(resource_params)
        render 'api/resources/show', location: api_resource_url(@resource)
      else
        render json: @resource.errors, status: :unprocessable_entity
      end
    end

    # DELETE /resources/1
    def destroy
      @resource.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource
      @resource = Resource.find(params[:id])

      authorize @resource
    end

    # Only allow a trusted parameter "white list" through.
    def resource_params
      params.require(:resource).permit(:name, :description, :color, :edible, :parent_id, :genus_id, common_names: [])
    end
  end
end

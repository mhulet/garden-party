module Api
  class PathsController < ApiApplicationController
    before_action :set_path, only: [:show, :update, :destroy]

    # GET /api/paths
    def index
      @paths = policy_scope(Path).where(map_id: params[:map_id])

      render 'api/paths/index'
    end

    # GET /api/paths/1
    def show
      render 'api/paths/show'
    end

    # POST /api/paths
    def create
      @path = Path.new(create_path_params)
      authorize @path

      if @path.save
        render 'api/paths/show', status: :created, location: api_path_url(@path)
      else
        render json: @path.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/paths/1
    def update
      if @path.update(update_path_params)
        render 'api/paths/show', location: api_path_url(@path)
      else
        render json: @path.errors, status: :unprocessable_entity
      end
    end

    # DELETE /api/paths/1
    def destroy
      @path.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_path
      @path = Path.find(params[:id])

      authorize @path
    end

    # Only allow a trusted parameter "white list" through.
    def create_path_params
      path_params [:name, :stroke_color, :background_color, :stroke_width, :stroke_style_preset, :geometry, :map_id, :layer_id]
    end

    def update_path_params
      path_params [:name, :stroke_color, :background_color, :stroke_width, :stroke_style_preset, :geometry, :layer_id]
    end

    def path_params(allowed_fields)
      # Pick params one by one because validating geometry is an issue with polygons
      parameters = params.require(:path).to_unsafe_hash
      values = {}
      allowed_fields.each { |key| values[key] = parameters[key] if parameters.key? key }

      values
    end
  end
end

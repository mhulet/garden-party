module Api
  class ElementsController < ApiApplicationController
    before_action :set_element, only: [:show, :update, :destroy]

    # GET /elements
    def index
      @elements = policy_scope(Element)
      @elements = if params[:patch_id]
                    @elements.where(patch_id: params[:patch_id])
                  else
                    @elements.where(patch: Patch.where({ map_id: params[:map_id] }))
                  end

      render 'api/elements/index'
    end

    # GET /elements/1
    def show
      render 'api/elements/show'
    end

    # POST /elements
    def create
      @element = Element.new(element_params)
      authorize @element

      if @element.save
        render 'api/elements/show', status: :created, location: api_element_url(@element)
      else
        render json: @element.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /elements/1
    def update
      if @element.update(element_params)
        render 'api/elements/show', location: api_element_url(@element)
      else
        render json: @element.errors, status: :unprocessable_entity
      end
    end

    # DELETE /elements/1
    def destroy
      @element.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_element
      @element = Element.find(params[:id])

      authorize @element
    end

    # Only allow a trusted parameter "white list" through.
    def element_params
      params.require(:element).permit(:resource_id, :patch_id, :implanted_at, :implantation_planned_for, :removed_at, :removal_planned_for)
    end
  end
end

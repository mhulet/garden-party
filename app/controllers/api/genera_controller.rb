module Api
  class GeneraController < ApiApplicationController
    before_action :set_genus, only: [:show, :update, :destroy]
    before_action :set_paper_trail_whodunnit

    # GET /genera
    def index
      @genera = policy_scope Genus

      render 'api/genera/index'
    end

    # GET /genera/1
    def show
      render 'api/genera/show'
    end

    # POST /genera
    def create
      authorize Genus
      @genus = Genus.new(genus_params)

      if @genus.save
        render 'api/genera/show', status: :created, location: api_genus_url(@genus)
      else
        render json: @genus.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /genera/1
    def update
      if @genus.update(genus_params)
        render 'api/genera/show', status: :ok, location: api_genus_url(@genus)
      else
        render json: @genus.errors, status: :unprocessable_entity
      end
    end

    # DELETE /genera/1
    def destroy
      @genus.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_genus
      @genus = Genus.find(params[:id])

      authorize @genus
    end

    # Only allow a trusted parameter "white list" through.
    def genus_params
      params.require(:genus).permit(:name, :description, :kingdom)
    end
  end
end

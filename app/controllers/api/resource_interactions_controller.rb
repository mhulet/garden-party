module Api
  class ResourceInteractionsController < ApiApplicationController
    before_action :set_resource_interaction, only: [:show, :update, :destroy]
    before_action :set_paper_trail_whodunnit

    # GET /resource_interactions
    def index
      @resource_interactions = policy_scope ResourceInteraction
      @resource_interactions = @resource_interactions.with_resource(params[:resource_id]) if params[:resource_id]

      render 'api/resource_interactions/index'
    end

    # GET /resource_interactions/1
    def show
      render 'api/resource_interactions/show'
    end

    # POST /resource_interactions
    def create
      authorize ResourceInteraction
      @resource_interaction = ResourceInteraction.new(resource_interaction_params)

      if @resource_interaction.save
        render 'api/resource_interactions/show', status: :created, location: api_resource_interaction_url(@resource_interaction)
      else
        render json: @resource_interaction.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /resource_interactions/1
    def update
      if @resource_interaction.update(resource_interaction_params)
        render 'api/resource_interactions/show', status: :ok, location: api_resource_interaction_url(@resource_interaction)
      else
        render json: @resource_interaction.errors, status: :unprocessable_entity
      end
    end

    # DELETE /resource_interactions/1
    def destroy
      @resource_interaction.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource_interaction
      @resource_interaction = ResourceInteraction.find(params[:id])

      authorize @resource_interaction
    end

    # Only allow a trusted parameter "white list" through.
    def resource_interaction_params
      params.require(:resource_interaction).permit(:subject_id, :target_id, :nature, :notes)
    end
  end
end

module Admin
  class ResourceInteractionsController < AdminApplicationController
    before_action :set_resource_interaction, only: [:show, :edit, :update, :destroy]
    before_action :set_paper_trail_whodunnit

    # GET /admin/resource_interactions
    def index
      @resource_interactions = policy_scope ResourceInteraction.all
    end

    # GET /admin/resource_interactions/1
    def show; end

    # GET /admin/resource_interactions/new
    def new
      authorize ResourceInteraction

      @resource_interaction = ResourceInteraction.new
    end

    # GET /admin/resource_interactions/1/edit
    def edit; end

    # POST /admin/resource_interactions
    def create
      authorize ResourceInteraction

      @resource_interaction = ResourceInteraction.new(resource_interaction_params)

      if @resource_interaction.save
        redirect_to admin_resource_interaction_path(@resource_interaction), notice: I18n.t('admin.resource_interactions.create.success')
      else
        render :new
      end
    end

    # PATCH/PUT /admin/resource_interactions/1
    def update
      if @resource_interaction.update(resource_interaction_params)
        redirect_to admin_resource_interaction_path(@resource_interaction), notice: I18n.t('admin.resource_interactions.update.success')
      else
        render :edit
      end
    end

    # DELETE /admin/resource_interactions/1
    def destroy
      @resource_interaction.destroy
      redirect_to admin_resource_interactions_url, notice: I18n.t('admin.resource_interactions.destroy.success')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource_interaction
      @resource_interaction = ResourceInteraction.find(params[:id])

      authorize @resource_interaction
    end

    # Only allow a trusted parameter "white list" through.
    def resource_interaction_params
      parameters = params.require(:resource_interaction).permit(:nature, :notes, :subject_id, :target_id)
      parameters[:nature] = parameters[:nature].to_i if parameters[:nature].present?
      parameters
    end
  end
end

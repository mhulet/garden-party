module Admin
  class LinksController < AdminApplicationController
    before_action :set_link, only: [:edit, :update, :approve, :destroy]

    # GET /admin/links
    def index
      @links = policy_scope Link.all
    end

    # GET /admin/links/new
    def new
      authorize Link

      @link = Link.new
    end

    # GET /admin/links/1/edit
    def edit; end

    # POST /admin/links
    def create
      authorize Link

      @link      = Link.new(link_params)
      @link.user = current_user

      if @link.save
        redirect_to admin_links_path, notice: I18n.t('admin.links.create.success')
      else
        render :new
      end
    end

    # PATCH/PUT /admin/links/1
    def update
      if @link.update(link_params)
        redirect_to admin_links_path, notice: I18n.t('admin.links.update.success')
      else
        render :edit
      end
    end

    # PATCH/PUT /admin/links/1/approve
    def approve
      @link.approved_by = current_user

      if @link.save
        redirect_to admin_links_path, notice: I18n.t('admin.links.approve.success')
      else
        redirect_to admin_links_path, alert: I18n.t('admin.links.approve.failure')
      end
    end

    # DELETE /admin/links/1
    def destroy
      @link.destroy
      redirect_to admin_links_url, notice: I18n.t('admin.links.destroy.success')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_link
      @link = Link.find(params[:id])

      authorize @link
    end

    # Only allow a trusted parameter "white list" through.
    def link_params
      params.require(:link).permit(:title, :url, :description)
    end
  end
end

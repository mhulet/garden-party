module Admin
  class ResourcesController < AdminApplicationController
    before_action :set_resource, only: [:show, :edit, :update, :destroy]
    before_action :set_paper_trail_whodunnit

    # GET /admin/resources
    def index
      @resources = policy_scope(Resource).order(:name)
    end

    # GET /admin/resources/1
    def show; end

    # GET /admin/resources/new
    def new
      authorize Resource

      @resource = Resource.new
    end

    # GET /admin/resources/1/edit
    def edit; end

    # POST /admin/resources
    def create
      authorize Resource

      @resource = Resource.new(resource_params)

      if @resource.save
        redirect_to admin_resource_path(@resource), notice: I18n.t('admin.resources.create.success')
      else
        render :new
      end
    end

    # PATCH/PUT /admin/resources/1
    def update
      if @resource.update(resource_params)
        redirect_to admin_resource_path(@resource), notice: I18n.t('admin.resources.update.success')
      else
        render :edit
      end
    end

    # DELETE /admin/resources/1
    def destroy
      @resource.destroy
      redirect_to admin_resources_url, notice: I18n.t('admin.resources.destroy.success')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource
      @resource = Resource.find(params[:id])

      authorize @resource
    end

    # Only allow a trusted parameter "white list" through.
    def resource_params
      params.require(:resource).permit(:name, :common_names, :description, :color, :edible, :source, :parent_id, :genus_id)
    end
  end
end

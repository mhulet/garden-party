class LinksController < ApplicationController
  before_action :set_link, only: [:edit, :update, :destroy]

  # GET /links
  def index
    @links = policy_scope Link.order(title: :asc)
  end

  # GET /links/new
  def new
    authorize Link

    @link = Link.new
  end

  # GET /admin/links/1/edit
  def edit; end

  # POST /links
  def create
    authorize Link

    @link = Link.new(link_params)
    @link.user = current_user

    if @link.save
      redirect_to links_path, notice: I18n.t('links.create.success')
    else
      render :new
    end
  end

  # PUT /links/1
  def update
    if @link.update(link_params)
      redirect_to links_path, notice: I18n.t('links.update.success')
    else
      render :edit
    end
  end

  # DELETE /links/1
  def destroy
    @link.destroy
    redirect_to links_path, notice: I18n.t('links.destroy.success')
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_link
    @link = Link.find(params[:id])

    authorize @link
  end

  def link_params
    params.require(:link).permit(:title, :url, :description)
  end
end

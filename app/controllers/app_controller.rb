class AppController < ApplicationController
  before_action :skip_authorization
  before_action :skip_policy_scope
  before_action :authenticate_user!, only: [:app]

  def home
    @public_maps = Map.publicly_available
  end

  def changelog; end

  def app; end

  def shared
    @map = Map.find(params[:map_id])
  end
end

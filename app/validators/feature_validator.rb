class FeatureValidator < ActiveModel::EachValidator
  # JSON Schema, from https://github.com/geojson/schema, without the bounding boxes
  # FIXME: Use a lib to validate
  JSON_SCHEMA = YAML.load_file(File.join(__dir__, 'feature_validator_format.yml'))

  def validate_each(record, attribute, value)
    validate_format record, attribute, value

    # Stop validation if format is invalid
    return if record.errors[attribute].size.positive?

    @type = type(value)

    validate_type(record, attribute) if options[:types]
    validate_ring(record, attribute, value) if polygon?
    validate_radius(record, attribute, value)
  end

  private

  def type(geometry)
    type = geometry['geometry']['type']
    return type if %w[LineString Polygon].include? type

    if type == 'Point'
      return 'Circle' if geometry['properties']&.key?('radius')

      'Point'
    else
      :unknown
    end
  end

  def polygon?
    @type == 'Polygon'
  end

  def line?
    @type == 'LineString'
  end

  def circle?
    @type == 'Circle'
  end

  def point?
    @type == 'Point'
  end

  def validate_type(record, attribute)
    record.errors.add attribute, I18n.t('activerecord.errors.feature.has_a_wrong_type') unless options[:types].include? @type
  end

  def validate_format(record, attribute, value)
    schema_errors = JSON::Validator.fully_validate(JSON_SCHEMA['FEATURE_FORMAT'], value)
    record.errors.add attribute, I18n.t('activerecord.errors.feature.bad_feature_format', errors: schema_errors.join("\n")) unless schema_errors.size.zero?
  end

  def validate_radius(record, attribute, value)
    record.errors.add attribute, I18n.t('activerecord.errors.feature.radius_not_allowed') if !circle? && value['properties']&.key?('radius')
  end

  def validate_ring(record, attribute, value)
    polygon_error = value.dig('geometry', 'coordinates', 0).first != value.dig('geometry', 'coordinates', 0).last
    record.errors.add attribute, I18n.t('activerecord.errors.feature.open_polygon') if polygon_error
  end
end

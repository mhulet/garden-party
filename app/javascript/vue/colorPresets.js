export default [
  {
    // I18n use $t('js.color_presets.light_path')
    i18n: 'js.color_presets.light_path',
    stroke: '167, 131, 43, 1',
    fill: '167, 131, 43, 0.4',
    width: 1,
    strokeStyle: null,
  },
  {
    // I18n use $t('js.color_presets.hard_path')
    i18n: 'js.color_presets.hard_path',
    stroke: '167, 131, 43, 1',
    fill: '207, 188, 138, 1',
    width: 1,
    strokeStyle: null,
  },
  {
    // I18n use $t('js.color_presets.water')
    i18n: 'js.color_presets.water',
    stroke: '85, 134, 182, 1',
    fill: '170, 211, 223, 1',
    width: 1,
    strokeStyle: null,
  },
  {
    // I18n use $t('js.color_presets.grass')
    i18n: 'js.color_presets.grass',
    stroke: '85, 182, 34, 1',
    fill: '170, 223, 111, 1',
    width: 1,
    strokeStyle: null,
  },
  {
    // I18n use $t('js.color_presets.forest')
    i18n: 'js.color_presets.forest',
    stroke: '147, 182, 133, 1',
    fill: '173, 209, 158, 1',
    width: 1,
    strokeStyle: null,
  },
  {
    // I18n use $t('js.color_presets.shadow')
    i18n: 'js.color_presets.shadow',
    stroke: '69, 69, 69, 0.5',
    fill: '69, 69, 69, 0.2',
    width: 2,
    strokeStyle: 'dotted',
  },
  {
    // I18n use $t('js.color_presets.important')
    i18n: 'js.color_presets.important',
    stroke: '225, 82, 126, 1',
    fill: '225, 82, 126, 0.5',
    width: 3,
    strokeStyle: null,
  },
  {
    // I18n use $t('js.color_presets.concrete')
    i18n: 'js.color_presets.concrete',
    stroke: '168, 168, 169, 1',
    fill: '168, 168, 169, 1',
    width: 1,
    strokeStyle: null,
  },
]

import { config, shallowMount, Wrapper } from '@vue/test-utils'
import Element from '../../../../classes/models/Element'

import ActionButtonsComponent from './_ActionButtons'

/**
 * @typedef {import('vue')} Vue
 */

const now = new Date()
const later = new Date()
later.setDate(later.getDate() + 2)
const before = new Date()
before.setDate(before.getDate() - 2)

/**
 * Builds an Element
 *
 * @param   {object}  config                        - Element configuration
 * @param   {Date}    config.implantedAt            - Implantation date
 * @param   {Date}    config.removedAt              - Removal date
 * @param   {Date}    config.implantationPlannedFor - Planned date for implantation
 * @param   {Date}    config.removalPlannedFor      - Planned date for removal
 * @returns {Element}                               Element to use in tests
 */
function makeElement ({ implantedAt = null, removedAt = null, implantationPlannedFor = null, removalPlannedFor = null }) {
  let status = 'planned'
  if (implantedAt && !removedAt) status = 'implanted'
  else if (removedAt) status = 'removed'
  return new Element({
    id: 1,
    implanted_at: implantedAt ? implantedAt.toISOString() : null,
    resource_id: 1,
    patch_id: 1,
    created_at: now.toISOString(),
    updated_at: now.toISOString(),
    removed_at: removedAt ? removedAt.toISOString() : null,
    implantation_planned_for: implantationPlannedFor ? implantationPlannedFor.toISOString() : null,
    removal_planned_for: removalPlannedFor ? removalPlannedFor.toISOString() : null,
    status,
  })
}

/**
 * Wrapper to mount component with correct data
 *
 * @param   {Element}      element      - Element to use
 * @param   {boolean}      destructible - Destructible state
 * @returns {Wrapper<Vue>}              Mounted wrapper for the test
 */
function makeWrapper (element, destructible = true) {
  return shallowMount(ActionButtonsComponent, { propsData: { element, destructible } })
}

jest.mock('../../../../helpers/i18n', () => ({}))

config.mocks.$t = string => string
config.mocks.$d = date => date.toString()
config.mocks.$store = {
  actions: {},
  getters: {},
  dispatch: () => Promise.resolve(),
}

describe('interface', () => {
  describe('with button to destroy element', () => {
    const wrapper = makeWrapper(makeElement({}))

    test('has a button to destroy the element', () => {
      expect(wrapper.find('button[title="js.elements.action_buttons.destroy_element"]').exists()).toBeTruthy()
    })
  })

  describe('with no button to destroy element', () => {
    const wrapper = makeWrapper(makeElement({}), false)

    test('don\'t have button to destroy the element', () => {
      expect(wrapper.find('button[title="js.elements.action_buttons.destroy_element"]').exists()).toBeFalsy()
    })
  })

  describe('with planned implantation date', () => {
    const wrapper = makeWrapper(makeElement({ implantationPlannedFor: later }))

    test('displays the planned date', () => {
      expect(wrapper.text()).toContain('js.elements.action_buttons.waiting_for_implantation_on')
    })
    test('don\'t have other actions', () => {
      // Only check for one action
      expect(wrapper.text()).not.toContain('js.elements.activities.water')
      // History + destroy
      expect(wrapper.findAll('button')).toHaveLength(2)
    })
    test('has a disabled history button', () => {
      const button = wrapper.find('button:disabled[title="js.elements.action_buttons.show_activity"]')
      expect(button.exists()).toBeTruthy()
    })
  })

  describe('with implanted element', () => {
    const wrapper = makeWrapper(makeElement({ implantedAt: before }))

    test('has actions', () => {
      // Only check for one action
      expect(wrapper.text()).toContain('js.elements.activities.water')
      // 5 actions + history/destroy + custom
      expect(wrapper.findAll('button')).toHaveLength(8)
    })
    test('has an enabled history button', () => {
      const button = wrapper.find('button:not(:disabled)[title="js.elements.action_buttons.show_activity"]')
      expect(button.exists()).toBeTruthy()
    })
  })

  describe('with removed element', () => {
    const wrapper = makeWrapper(makeElement({ implantedAt: before, removedAt: now }))

    test('displays the removal date', () => {
      expect(wrapper.text()).toContain('js.elements.action_buttons.removed_at')
    })
    test('don\'t have other actions', () => {
      // Only check for one action
      expect(wrapper.text()).not.toContain('js.elements.activities.water')
      // History + destroy
      expect(wrapper.findAll('button')).toHaveLength(2)
    })
    test('has an enabled history button', () => {
      const button = wrapper.find('button:not(:disabled)[title="js.elements.action_buttons.show_activity"]')
      expect(button.exists()).toBeTruthy()
    })
  })
})

// TODO: Test interactions

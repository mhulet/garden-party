<template>
  <div class="resources_list__resource">
    <div class="resources_list__resource__header">
      <img :src="resource.getPicturePath()" alt="">
      <h4 :class="{active: selectedId === resource.id}"
          @click="$emit('select', resource.id)">
        {{ resource.name }}
      </h4>
      <div>
        <button v-if="children.length > 0" class="button--icon button--text" @click="expanded =! expanded">
          <icon v-if="expanded" name="caret-circle-up" />
          <icon v-else name="caret-circle-down" />
        </button>
        <button class="button--icon button--text text--info" @click="showResourceDescription(resource)"><icon name="info" /></button>
      </div>
    </div>

    <div v-show="expanded" class="resources_list__resource__content">
      <side-panel-child-resource v-for="child in children"
                                 :key="child.id"
                                 :resource="child"
                                 :selected-id="selectedId"
                                 @select="bubbleSelect" />
    </div>
  </div>
</template>

<script>
import EventBus from '../../../../../../tools/event-bus'
import Resource from '../../../../../../classes/models/Resource'
import SidePanelChildResource from './_ChildResource'
import Icon from '../../../../../../common/Icon'

/**
 * @fires  select
 * @fires  show-info
 */
export default {
  name: 'SidePanelParentResource',
  components: { Icon, SidePanelChildResource },
  props: {
    resource: { required: true, type: Resource },
    selectedId: { required: false, type: Number, default: null },
  },
  data () {
    return {
      expanded: false,
    }
  },
  computed: {
    children () { return this.$store.getters.childResources(this.$props.resource.id) },
  },
  methods: {
    bubbleSelect (resourceId) { this.$emit('select', resourceId) },
    showResourceDescription (resource) {
      EventBus.$emit('show-resource-description', resource)
    },
  },
}
</script>

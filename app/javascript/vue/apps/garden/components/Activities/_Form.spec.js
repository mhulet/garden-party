import { config, shallowMount } from '@vue/test-utils'
import FormComponent from './_Form'

config.mocks.$t = string => string
config.mocks.$d = date => date.toString()

window.ACTIVITIES_ALLOWED_MIME_TYPES = 'image/jpeg, image/png'

describe('interface', () => {
  describe('with a note action', () => {
    const wrapper = shallowMount(FormComponent, {
      propsData: {
        elementId: 1,
        action: 'note',
      },
    })

    test('has a required notes field', () => {
      expect(wrapper.find('textarea[required="required"]').exists()).toBe(true)
    })
  })

  describe('with an action which is not a note', () => {
    const wrapper = shallowMount(FormComponent, {
      propsData: {
        elementId: 1,
        action: 'water',
      },
    })

    test('has a non-required notes field', () => {
      expect(wrapper.find('textarea:not([required="required"]').exists()).toBe(true)
    })
  })
})

// TODO: Test interactions

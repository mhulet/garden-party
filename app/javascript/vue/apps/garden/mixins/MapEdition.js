import StaticSource from 'ol/source/ImageStatic'
import DrawInteraction from 'ol/interaction/Draw'
import { unByKey } from 'ol/Observable'
import Feature from 'ol/Feature'
import PointGeom from 'ol/geom/Point'
import { getCenter } from 'ol/extent'
import Projection from 'ol/proj/Projection'
import ImageLayer from 'ol/layer/Image'
import VectorSource from 'ol/source/Vector'
import VectorLayer from 'ol/layer/Vector'
import View from 'ol/View'
import Map from 'ol/Map'
import { ScaleLine } from 'ol/control'
import { getLength } from 'ol/sphere'
import TileLayer from 'ol/layer/Tile'
import OSM from 'ol/source/OSM'
import LineString from 'ol/geom/LineString'

const baseScale = 100

export default {
  data () {
    return this.defaultState()
  },
  computed: {
    distance () {
      if (this.scalePoints.length < 2) return 0

      return getLength(new LineString(this.scalePoints))
    },
    scale () { return this.scaleReference / this.distance },
  },
  methods: {
    defaultState () {
      return {
        filePreview: null,
        pictureSize: null,

        // Map
        olMap: null,
        view: null,
        vectorLayer: null,
        interaction: null,

        // Scaling
        baseScale,
        scaleReference: 1,
        previousScale: baseScale,
        scalePoints: [],

        // State
        extentChanged: false,
        centerChanged: false,
      }
    },
    resetMap () {
      if (!this.olMap) return
      this.olMap.setTarget(null)
    },
    resetState () {
      const state = this.defaultState()
      Object.keys(state).forEach(k => { this[k] = state[k] })
    },
    scaleExtent (scale = 1) {
      const baseScale = this.previousScale * scale
      this.previousScale = baseScale
      return [0, 0, this.pictureSize[0] * baseScale, this.pictureSize[1] * baseScale]
    },
    imageLayerSource (projection, extent) {
      return new StaticSource({
        url: this.filePreview,
        projection,
        imageExtent: extent,
      })
    },
    clearFeatures () {
      const source = this.vectorLayer.getSource()
      source.getFeatures().forEach(f => { source.removeFeature(f) })
    },
    clearInteractions () {
      if (!this.interaction) return

      this.olMap.removeInteraction(this.interaction)
      this.interaction = null
    },
    enterMeasureMode () {
      const that = this
      this.clearFeatures()
      this.clearInteractions()

      let points = []
      const source = this.vectorLayer.getSource()

      this.interaction = new DrawInteraction({ source, type: 'Point' })
      this.olMap.addInteraction(this.interaction)

      let listener

      this.interaction.on('drawend', function (event) {
        points.push(event.feature.getGeometry().flatCoordinates)
        unByKey(listener)

        if (points.length === 2) {
          // Remove points
          that.$nextTick(() => { that.clearFeatures() })
          that.scalePoints = points
          that.extentChanged = true
          // Update extent
          const newExtent = that.scaleExtent(that.scale)
          that.imageLayer.setSource(that.imageLayerSource(that.view.getProjection(), newExtent))
          that.view.fit(newExtent, that.olMap.getSize())

          points = []
        }
      })
    },
    enterCenterSelectionMode () {
      const that = this
      this.clearFeatures()
      this.clearInteractions()

      const feature = new Feature({ geometry: new PointGeom([0, 0]) })

      this.olMap.on('click', (event) => {
        feature.setGeometry(new PointGeom(event.coordinate))
        if (!that.centerChanged) that.vectorLayer.getSource().addFeature(feature)

        this.inputs.center = event.coordinate
        this.centerChanged = true
      })
    },
    loadAndRenderMap (reader, onReady = () => {}) {
      reader.addEventListener('load', (event) => {
        this.filePreview = event.target.result
        const image = new Image()
        image.addEventListener('load', () => {
          this.pictureSize = [image.width, image.height]
          const extent = this.scaleExtent()
          const imageCenter = getCenter(extent)

          const projection = new Projection({
            code: window.MAPS_PROJECTION,
            unit: 'meter',
            extent,
          })

          // Background
          const imageSource = this.imageLayerSource(projection, extent)
          const imageLayer = new ImageLayer({ source: imageSource })
          this.imageLayer = imageLayer

          // Interactions layer
          const source = new VectorSource({})
          this.vectorLayer = new VectorLayer({ source })

          this.view = new View({
            projection,
            center: imageCenter,
            resolution: 1,
          })

          this.olMap = new Map({
            target: 'map',
            layers: [imageLayer, this.vectorLayer],
            view: this.view,
          })

          this.olMap.addControl(new ScaleLine({ units: 'metric' }))

          this.view.fit(extent, this.olMap.getSize())
          onReady()
        })

        // Load image
        image.src = this.filePreview
      })
    },
    loadOSMMap () {
      let center = [0, 0]
      let zoom = 2
      if (this.map) {
        center = this.map.center
        zoom = 20
      }

      const tileLayer = new TileLayer({ source: new OSM({}) })
      this.vectorLayer = new VectorLayer({ source: new VectorSource({}) })

      this.olMap = new Map({
        target: 'map',
        layers: [tileLayer, this.vectorLayer],
        view: new View({
          center,
          zoom,
        }),
      })
    },
  },
}

import Vue from 'vue'
import Vuex from 'vuex'

import ActivityModule from '../../stores/modules/ActivityModule'
import ElementModule from '../../stores/modules/ElementModule'
import GenusModule from '../../stores/modules/GenusModule'
import LayerModule from '../../stores/modules/LayerModule'
import MapModule from '../../stores/modules/MapModule'
import PatchModule from '../../stores/modules/PatchModule'
import PathModule from '../../stores/modules/PathModule'
import ResourceInteractionModule from '../../stores/modules/ResourceInteractionModule'
import ResourceModule from '../../stores/modules/ResourceModule'
import UserModule from '../../stores/modules/UserModule'

import Model from '../../classes/models/Model'

Vue.use(Vuex)

const Store = new Vuex.Store({
  state: {},
  mutations: {
    resetMapData (state) {
      state.ActivityModule.activities = []
      state.ActivityModule.activityNames = []
      state.ElementModule.elements = []
      state.LayerModule.layers = []
      state.PatchModule.patches = []
      state.PathModule.paths = []
    },
  },
  actions: {
    resetMapData ({ commit }) { commit('resetMapData') },
  },
  getters: {},
  modules: {
    ActivityModule,
    ElementModule,
    GenusModule,
    LayerModule,
    MapModule,
    PatchModule,
    PathModule,
    ResourceInteractionModule,
    ResourceModule,
    UserModule,
  },
})

Model.store = Store

export default Store

import Vue from 'vue'
import VueRouter from 'vue-router'
import Maps from './components/Maps/Index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'maps',
    component: Maps,
  },
  {
    path: '/library/:resourceId?',
    name: 'genera',
    component: () => import(/* webpackChunkName: "genera" */ './components/Library.vue'),
  },
  // Maps
  // --------------------------------------------------------------------------
  {
    // All child routes should have the "inMap" metadata set to true, as long as
    // they are related to current map.
    path: '/maps/new',
    component: () => import(/* webpackChunkName: "new_map_form" */ './components/Maps/New.vue'),
    children: [
      {
        path: '/maps/new/picture',
        name: 'newPictureMap',
        component: () => import(/* webpackChunkName: "new_map_form_picture" */ './components/Maps/_PictureForm.vue'),
      },
      {
        path: '/maps/new/osm',
        name: 'newOSMMap',
        component: () => import(/* webpackChunkName: "new_map_form_picture" */ './components/Maps/_OsmForm.vue'),
      },
    ],
  },
  {
    path: '/maps/:mapId(\\d+)/edit',
    name: 'editMap',
    component: () => import(/* webpackChunkName: "new_map_form_picture" */ './components/Maps/EditForm'),
  },
  {
    path: '/maps/:mapId(\\d+)',
    component: () => import(/* webpackChunkName: "map_show" */ './components/Layouts/_Garden'),
    children: [
      {
        path: '',
        name: 'map',
        component: () => import(/* webpackChunkName: "map" */ './components/Garden.vue'),
        meta: { inMap: true },
      },
      {
        path: '/maps/:mapId(\\d+)/patches',
        name: 'patches',
        component: () => import(/* webpackChunkName: "map_overview" */ './components/Overview.vue'),
        meta: { inMap: true },
      },
      {
        path: '/maps/:mapId(\\d+)/todo',
        name: 'todo',
        component: () => import(/* webpackChunkName: "map_form" */ './components/Tasks.vue'),
        meta: { inMap: true },
      },
    ],
  },
]

const router = new VueRouter({
  routes,
})

export default router

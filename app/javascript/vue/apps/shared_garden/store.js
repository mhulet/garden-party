import Vue from 'vue'
import Vuex from 'vuex'

import ActivityModule from '../../stores/modules/ActivityModule'
import ElementModule from '../../stores/modules/ElementModule'
import GenusModule from '../../stores/modules/GenusModule'
import LayerModule from '../../stores/modules/LayerModule'
import MapModule from '../../stores/modules/MapModule'
import PatchModule from '../../stores/modules/PatchModule'
import PathModule from '../../stores/modules/PathModule'
import ResourceInteractionModule from '../../stores/modules/ResourceInteractionModule'
import ResourceModule from '../../stores/modules/ResourceModule'
import UserModule from '../../stores/modules/UserModule'

import Model from '../../classes/models/Model'
import Api from '../../helpers/Api'
import Layer from '../../classes/models/Layer'
import Map from '../../classes/models/Map'
import Patch from '../../classes/models/Patch'
import Path from '../../classes/models/Path'
import Element from '../../classes/models/Element'
import Resource from '../../classes/models/Resource'

Vue.use(Vuex)

const Store = new Vuex.Store({
  state: {},
  actions: {
    loadSharedGarden ({ commit }, mapId) {
      return Api('get', `/api/shared/maps/${mapId}`)
        .then(({ map, patches, paths, elements, layers, resources }) => {
          commit('setMap', new Map(map))
          patches.forEach((p) => { commit('setPatch', new Patch(p)) })
          paths.forEach((p) => { commit('setPath', new Path(p)) })
          elements.forEach((e) => { commit('setElement', new Element(e)) })
          layers.forEach((l) => { commit('setLayer', new Layer(l)) })
          resources.forEach((r) => { commit('setResource', new Resource(r)) })
        })
    },
  },
  getters: {},
  modules: {
    ActivityModule,
    ElementModule,
    GenusModule,
    LayerModule,
    MapModule,
    PatchModule,
    PathModule,
    ResourceInteractionModule,
    ResourceModule,
    UserModule,
  },
})

Model.store = Store

export default Store

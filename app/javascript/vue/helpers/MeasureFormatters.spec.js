import { formatLength, formatArea } from './MeasureFormatters'

describe('formatLength', () => {
  it('adds unit to meters', () => {
    expect(formatLength(10)).toEqual('10m')
    expect(formatLength(999)).toEqual('999m')
  })

  it('converts large values to kilometers', () => {
    expect(formatLength(1000)).toEqual('1km')
    expect(formatLength(100000)).toEqual('100km')
  })
})

describe('formatArea', () => {
  it('adds unit to square meters', () => {
    expect(formatArea(1)).toEqual('1m<sup>2</sup>')
  })

  it('rounds meters with a precision of 2 if needed', () => {
    expect(formatArea(9999.1234)).toEqual('9999.12m<sup>2</sup>')
  })

  it('converts large unit to square kilometers', () => {
    expect(formatArea(100000)).toEqual('0.1km<sup>2</sup>')
  })

  it('rounds kilometers with a precision of 2 if needed', () => {
    expect(formatArea(1123400)).toEqual('1.12km<sup>2</sup>')
  })
})

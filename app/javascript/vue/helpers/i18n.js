import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const dateTimeFormats = {}
// Extract time formats as VueI18n needs a separate entry
dateTimeFormats[window.I18n.locale] = window.I18n.translations[window.I18n.locale].js_dates

// Only the current locale is loaded in window, so we don't care about fallbacks or other things.
export default new VueI18n({ locale: window.I18n.locale, messages: window.I18n.translations, dateTimeFormats })

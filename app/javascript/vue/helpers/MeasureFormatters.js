import { getArea, getLength } from 'ol/sphere'
import { Polygon, LineString } from 'ol/geom'

/**
 * @typedef {import('../classes/models/Path')}  Path
 * @typedef {import('../classes/models/Patch')} Patch
 */

/**
 * Measures a given geometry depending on its type
 *
 * @param   {Patch|Path} entity - Geometry from Patch or Path entity
 * @returns {string}            - Formatted measure (length or area)
 */
export function formatGeometryMeasure (entity) {
  switch (entity.geometry.geometry.type) {
    case 'LineString':
      return formatLength(getLength(new LineString(entity.geometry.geometry.coordinates)))
    case 'Polygon':
      return formatArea(getArea(new Polygon(entity.geometry.geometry.coordinates)))
    case 'Point':
      if (entity.geometry.properties && entity.geometry.properties.radius) {
        // Convert radius in a line to determine its real length
        const radiusLine = new LineString([[0, 0], [0, entity.geometry.properties.radius]])
        const radius = getLength(radiusLine)
        return formatArea(Math.PI * Math.pow(radius, 2))
      }
  }
}

/**
 * Returns a human readable length
 *
 * @param   {number} length - Length in meters
 * @returns {string}        - Formatted length
 */
export function formatLength (length) {
  if (length >= 1000) return Math.round(length / 1000 * 100) / 100 + 'km'
  return Math.round(length * 100) / 100 + 'm'
}

/**
 * Returns a human readable area
 *
 * @param   {number} area - Area in square meters
 * @returns {string}      - Formatted area
 */
export function formatArea (area) {
  if (area > 10000) return Math.round((area / 1000000) * 100) / 100 + 'km<sup>2</sup>'
  return Math.round(area * 100) / 100 + 'm<sup>2</sup>'
}

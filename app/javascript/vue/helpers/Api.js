import i18n from './i18n'
import handleAPIError from '../tools/error_handler'
import Toaster from '../helpers/toaster'

/**
 * Makes an Api call and returns a promise with data
 *
 * @param   {string}          method - Request verb
 * @param   {string}          url    - Target URL
 * @param   {object}          [data] - Payload
 * @returns {Promise<object>}        Response content, or an error
 */
export default function (method, url, data = {}) {
  if (!data) data = {} // When a null value is passed
  const request = new window.XMLHttpRequest()

  request.open(method, url, true)
  request.setRequestHeader('Accept', 'application/json')
  if ((typeof data.append) !== 'function') {
    request.setRequestHeader('Content-Type', 'application/json')
    data = JSON.stringify(data)
  }

  return new Promise((resolve, reject) => {
    request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        resolve(request.response ? JSON.parse(request.response) : null)
      } else {
        return reject(handleAPIError(request, { url: url }))
      }
    }
    // FIXME: Handle connectivity issues
    request.onerror = function () {
      // i18n-tasks-use t('js.api.unreachable_server')
      Toaster.error(i18n.t('js.api.unreachable_server'))
      return reject(request)
    }

    request.send(data)
  })
}

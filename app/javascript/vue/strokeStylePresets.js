export default {
  dotted: {
    // I18n use $t('js.stroke_style_presets.dotted')
    i18n: 'js.stroke_style_presets.dotted',
    style: (width) => [width, width],
  },
  dashed: {
    // I18n use $t('js.stroke_style_presets.dashed')
    i18n: 'js.stroke_style_presets.dashed',
    style: (width) => [4 * width, 4 * width],
  },
  long_dashes: {
    // I18n use $t('js.stroke_style_presets.long_dashes')
    i18n: 'js.stroke_style_presets.long_dashes',
    style: (width) => [8 * width, 4 * width],
  },
  long_dashes2: {
    // I18n use $t('js.stroke_style_presets.long_dashes2')
    i18n: 'js.stroke_style_presets.long_dashes2',
    style: (width) => [4 * width, 8 * width],
  },
  long_dashes3: {
    // I18n use $t('js.stroke_style_presets.long_dashes3')
    i18n: 'js.stroke_style_presets.long_dashes3',
    style: (width) => [4 * width, 4 * width, 8 * width, 4 * width],
  },
  dashed2: {
    // I18n use $t('js.stroke_style_presets.dashed2')
    i18n: 'js.stroke_style_presets.dashed2',
    style: (width) => [4 * width, 4 * width, width, 4 * width],
  },
  dashed3: {
    // I18n use $t('js.stroke_style_presets.dashed3')
    i18n: 'js.stroke_style_presets.dashed3',
    style: (width) => [4 * width, 4 * width, width, 2 * width, width, 4 * width],
  },
}

import Base from './Base'
import ResourceInteraction from '../../classes/models/ResourceInteraction'

export default {
  state: {
    /** @type {ResourceInteraction[]} */
    resourceInteractions: [],
  },
  mutations: {
    setResourceInteraction: Base.mutations.setEntity('resourceInteractions'),
    deleteResourceInteraction: Base.mutations.deleteEntity('resourceInteractions'),
  },
  actions: {
    loadResourceInteractions: Base.actions.loadEntities(ResourceInteraction, 'setResourceInteraction'),
    loadResourceInteraction: Base.actions.loadEntity(ResourceInteraction, 'setResourceInteraction'),
    createResourceInteraction: Base.actions.createEntity(ResourceInteraction, 'setResourceInteraction', 'resourceInteraction'),
    updateResourceInteraction: Base.actions.updateEntity(ResourceInteraction, 'setResourceInteraction', 'resourceInteraction'),
    saveResourceInteraction: Base.actions.saveEntity(ResourceInteraction, 'createResourceInteraction', 'updateResourceInteraction'),
    destroyResourceInteraction: Base.actions.destroyEntity(ResourceInteraction, 'deleteResourceInteraction'),
  },
  getters: {
    resourceInteractions: Base.getters.entities('resourceInteractions', 'name'),
    resourceInteraction: Base.getters.entity('resourceInteractions'),
    interactionsByResource: state => resourceId => state.resourceInteractions
      .filter(i => i.targetId === resourceId || i.subjectId === resourceId),
    likesInteractionsByResource: (state, getters) => resourceId => getters.interactionsByResource(resourceId)
      .filter(i => i.nature === 'mutualism'),
    dislikesInteractionsByResource: (state, getters) => resourceId => getters.interactionsByResource(resourceId)
      .filter(i => i.nature === 'competition'),
  },
}

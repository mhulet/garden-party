import Base from './Base'
import Resource from '../../classes/models/Resource'

export default {
  state: {
    /** @type {Resource[]} */
    resources: [],
  },
  mutations: {
    setResource: Base.mutations.setEntity('resources'),
    deleteResource: Base.mutations.deleteEntity('resources'),
  },
  actions: {
    loadResources: Base.actions.loadEntities(Resource, 'setResource'),
    loadResource: Base.actions.loadEntity(Resource, 'setResource'),
    createResource: Base.actions.createEntity(Resource, 'setResource', 'resource'),
    updateResource: Base.actions.updateEntity(Resource, 'setResource', 'resource'),
    saveResource: Base.actions.saveEntity(Resource, 'createResource', 'updateResource'),
    destroyResource: Base.actions.destroyEntity(Resource, 'deleteResource'),
  },
  getters: {
    resources: Base.getters.entities('resources', 'name'),
    resource: Base.getters.entity('resources'),
    resourcesWithoutChild: (state, getters) => getters.resources.filter(r => r.parentId === null),
    parentResources: state => state.resources.filter(p => !p.parentId)
      .sort((a, b) => a.name.localeCompare(b.name)),
    searchedResources: state => term => state.resources.filter((r) => {
      term = term.toLowerCase()
      if (r.name.toLowerCase().includes(term)) return true
      return r.commonNames.join('').includes(term)
    }),
    childResources: state => parentId => state.resources.filter(p => p.parentId === parentId)
      .sort((a, b) => a.name.localeCompare(b.name)),
    resourcesByLayer: state => layerId => state.resources.filter(p => p.layerId === layerId)
      .sort((a, b) => a.name.localeCompare(b.name)),
    resourcesByGenus: state => genusId => state.resources.filter(p => p.genusId === genusId)
      .sort((a, b) => a.name.localeCompare(b.name)),
  },
}

import Base from './Base'
import Element from '../../classes/models/Element'

export default {
  state: {
    /** @type {Element[]} */
    elements: [],
  },
  mutations: {
    setElement: Base.mutations.setEntity('elements'),
    deleteElement: Base.mutations.deleteEntity('elements'),
  },
  actions: {
    loadElements: Base.actions.loadEntities(Element, 'setElement'),
    loadElement: Base.actions.loadEntity(Element, 'setElement'),
    createElement: Base.actions.createEntity(Element, 'setElement', 'element'),
    updateElement: Base.actions.updateEntity(Element, 'setElement', 'element'),
    saveElement: Base.actions.saveEntity(Element, 'createElement', 'updateElement'),
    destroyElement: Base.actions.destroyEntity(Element, 'deleteElement'),
    loadPatchElements ({ commit }, patchId) {
      return Element.getPatchIndex(patchId)
        .then(items => { items.forEach(item => { commit('setElement', item) }) })
    },
    implantElement ({ dispatch }, { id, date }) {
      const now = new Date()
      const dateObj = new Date(date)
      const payload = { id }
      if (dateObj > now) payload.implantation_planned_for = date
      else payload.implanted_at = date
      return dispatch('updateElement', payload)
    },
    removeElement ({ dispatch }, { id, date }) {
      const now = new Date()
      const dateObj = new Date(date)
      const payload = { id }
      if (dateObj > now) payload.removal_planned_for = date
      else payload.removed_at = date
      return dispatch('updateElement', payload)
    },
  },
  getters: {
    elements: state => state.elements,
    element: Base.getters.entity('elements'),
    elementsByPatchId: state => patchId => state.elements.filter(c => c.patchId === patchId),
    nonPlantedElements: state => state.elements.filter(c => c.implantedAt === null),
  },
}

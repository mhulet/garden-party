import Base from './Base'
import Path from '../../classes/models/Path'

export default {
  state: {
    /** @type {Path[]} */
    paths: [],
  },
  mutations: {
    setPath: Base.mutations.setEntity('paths'),
    deletePath: Base.mutations.deleteEntity('paths'),
  },
  actions: {
    loadPaths: Base.actions.loadEntities(Path, 'setPath'),
    loadPath: Base.actions.loadEntity(Path, 'setPath'),
    updatePath: Base.actions.updateEntity(Path, 'setPath', 'path'),
    savePath: Base.actions.saveEntity(Path, 'createPath', 'updatePath'),
    destroyPath: Base.actions.destroyEntity(Path, 'deletePath'),
    createPath ({ commit, getters }, { payload, feature }) {
      return Path.create(payload)
        .then(item => {
          commit('setPath', item)
          // Return new instance
          const path = getters.path(item.id)
          path.setFeature(feature)
          return Promise.resolve(path)
        })
    },
  },
  getters: {
    paths: Base.getters.entities('paths', 'name'),
    path: Base.getters.entity('paths'),
  },
}

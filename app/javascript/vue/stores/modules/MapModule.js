import Base from './Base'
import Map from '../../classes/models/Map'

export default {
  state: {
    /** @type {Map[]} */
    maps: [],
  },
  mutations: {
    setMap: Base.mutations.setEntity('maps'),
    deleteMap: Base.mutations.deleteEntity('maps'),
  },
  actions: {
    loadMaps: Base.actions.loadEntities(Map, 'setMap'),
    loadMap: Base.actions.loadEntity(Map, 'setMap'),
    createMap: Base.actions.createEntity(Map, 'setMap', 'map'),
    updateMap: Base.actions.updateEntity(Map, 'setMap', 'map'),
    saveMap: Base.actions.saveEntity(Map, 'createMap', 'updateMap'),
    destroyMap: Base.actions.destroyEntity(Map, 'deleteMap'),
  },
  getters: {
    maps: Base.getters.entities('maps', 'name'),
    map: Base.getters.entity('maps'),
  },
}

export default {
  mutations: {
    setEntity (stateKey) {
      return (state, item) => {
        const index = state[stateKey].findIndex(i => i.id === item.id)
        if (index > -1) state[stateKey][index].updateAttributes(item)
        else state[stateKey].push(item)
      }
    },
    deleteEntity (stateKey) {
      return (state, id) => {
        const index = state[stateKey].findIndex(i => i.id === id)
        if (index > -1) state[stateKey].splice(index, 1)
        else throw new Error(`can't remove entity with id ${id} (${stateKey})`)
      }
    },
  },
  actions: {
    loadEntities (klass, mutationName) {
      return ({ commit }, elementId) => klass.getIndex(elementId)
        .then(items => { items.forEach(item => { commit(mutationName, item) }) })
    },
    loadEntity (klass, mutationName) {
      return ({ commit }, id) => klass.get(id)
        .then(item => { commit(mutationName, item) })
    },
    createEntity (klass, mutationName, getterForOne) {
      return ({ commit, getters }, payload) => klass.create(payload)
        .then(item => {
          commit(mutationName, item)
          // Return new instance
          return Promise.resolve(getters[getterForOne](item.id))
        })
    },
    updateEntity (klass, mutationName, getterForOne) {
      return ({ commit, getters }, payload) => klass.update({ payload })
        .then(item => {
          commit(mutationName, item)
          // Return updated instance
          return Promise.resolve(getters[getterForOne](item.id))
        })
    },
    saveEntity (klass, createAction, updateAction) {
      return ({ dispatch }, payload) => {
        let action = createAction
        if (payload.id) action = updateAction

        return dispatch(action, payload)
      }
    },
    destroyEntity (klass, mutationName) {
      return ({ commit }, id) => klass.destroy(id)
        .then(() => {
          commit(mutationName, id)
          return Promise.resolve()
        })
    },
  },
  getters: {
    entities (storeKey, sortAttribute) {
      return (state) => state[storeKey]
        .sort((a, b) => a[sortAttribute].localeCompare(b[sortAttribute]))
    },
    entity (storeKey) {
      return (state) => id => state[storeKey].find(e => e.id === id)
    },
  },
}

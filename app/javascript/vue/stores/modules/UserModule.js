import Base from './Base'
import User from '../../classes/models/User'

export default {
  state: {
    /** @type {User[]} */
    users: [],
  },
  mutations: {
    setUser: Base.mutations.setEntity('users'),
    deleteUser: Base.mutations.deleteEntity('users'),
  },
  actions: {
    loadUsers: Base.actions.loadEntities(User, 'setUser'),
    loadUser: Base.actions.loadEntity(User, 'setUser'),
    createUser: Base.actions.createEntity(User, 'setUser', 'user'),
    updateUser: Base.actions.updateEntity(User, 'setUser', 'user'),
    saveUser: Base.actions.saveEntity(User, 'createUser', 'updateUser'),
    destroyUser: Base.actions.destroyEntity(User, 'deleteUser'),
  },
  getters: {
    users: Base.getters.entities('users', 'username'),
    user: Base.getters.entity('users'),
  },
}

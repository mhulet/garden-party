import Base from './Base'
import Patch from '../../classes/models/Patch'

export default {
  state: {
    /** @type {Patch[]} */
    patches: [],
  },
  mutations: {
    setPatch: Base.mutations.setEntity('patches'),
    deletePatch: Base.mutations.deleteEntity('patches'),
  },
  actions: {
    loadPatches: Base.actions.loadEntities(Patch, 'setPatch'),
    loadPatch: Base.actions.loadEntity(Patch, 'setPatch'),
    updatePatch: Base.actions.updateEntity(Patch, 'setPatch', 'patch'),
    savePatch: Base.actions.saveEntity(Patch, 'createPatch', 'updatePatch'),
    destroyPatch: Base.actions.destroyEntity(Patch, 'deletePatch'),
    createPatch ({ commit, dispatch, getters }, { payload, feature }) {
      return Patch.create(payload)
        .then(item => {
          return dispatch('loadPatchElements', item.id)
            // Add and return the new patch once its elements are loaded
            .then(() => {
              commit('setPatch', item)
              // Return new instance
              const patch = getters.patch(item.id)
              patch.setFeature(feature)
              return Promise.resolve(patch)
            })
        })
    },
  },
  getters: {
    patch: Base.getters.entity('patches'),
    patches: state => state.patches,
    activePatches: state => state.patches.filter(p => !p.isRemoved()),
    patchesByIds: state => ids => state.patches.filter(p => ids.indexOf(p.id) > -1),
    patchesInLayer: state => layerId => state.patches.filter(p => p.layerId === layerId),
  },
}

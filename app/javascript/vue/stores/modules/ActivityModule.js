import Base from './Base'
import Activity from '../../classes/models/Activity'

/**
 * Compares activities by date of achievement. If item is not yet done, planning
 * date is used.
 *
 * @param   {Activity} a - First activity to compare
 * @param   {Activity} b - Second activity to compare
 * @returns {number}     Sort order
 */
function sortActivities (a, b) {
  const date1 = a.doneAt || a.plannedFor
  const date2 = b.doneAt || b.plannedFor
  return date2 - date1
}

export default {
  state: {
    /** @type {Activity[]} */
    activities: [],
    activityNames: [],
  },
  mutations: {
    setActivity: Base.mutations.setEntity('activities'),
    deleteActivity: Base.mutations.deleteEntity('activities'),
    setActivityNames: (state, names) => { state.activityNames = names },
    addActivityName: (state, name) => { if (state.activityNames.indexOf(name) === -1) state.activityNames.push(name) },
  },
  actions: {
    loadActivities: Base.actions.loadEntities(Activity, 'setActivity'),
    loadActivity: Base.actions.loadEntity(Activity, 'setActivity'),
    createActivity: Base.actions.createEntity(Activity, 'setActivity', 'activity'),
    updateActivity: Base.actions.updateEntity(Activity, 'setActivity', 'activity'),
    saveActivity: Base.actions.saveEntity(Activity, 'createActivity', 'updateActivity'),
    destroyActivity: Base.actions.destroyEntity(Activity, 'deleteActivity'),
    loadPendingActivities ({ commit }, mapId) {
      return Activity.getPending(mapId)
        .then(items => { items.forEach(item => { commit('setActivity', item) }) })
    },
    finishActivity ({ dispatch }, id) {
      return dispatch('updateActivity', { id, done_at: new Date().toISOString() })
    },
    loadActivityNames: ({ commit }, mapId) => Activity.getNames(mapId)
      .then(data => commit('setActivityNames', data)),
    addActivityName: ({ commit }, name) => { commit('addActivityName', name) },
  },
  getters: {
    activity: Base.getters.entity('activities'),
    activities: state => state.activities
      .sort((a, b) => sortActivities(a, b)),
    activityNames: state => state.activityNames.sort((a, b) => a.localeCompare(b)),
    activitiesByElement: state => elementId => state.activities.filter(a => a.subjectId === elementId && a.subjectType === 'Element')
      .sort((a, b) => sortActivities(a, b)),
    pendingActivities: state => state.activities.filter(a => a.doneAt === null)
      .sort((a, b) => sortActivities(a, b))
      .reverse(),
    lastTimeDone: (state, getters) => ({ elementId, name }) => getters.activitiesByElement(elementId)
      .filter(a => a.name === name && !!a.doneAt),
    nextTimePlanned: (state, getters) => ({ elementId, name }) => getters.activitiesByElement(elementId)
      .filter(a => a.name === name && !a.doneAt && !!a.plannedFor),
    activitySubject: (state, getters) => id => {
      const activity = getters.activity(id)
      if (!activity) return null

      switch (activity.subjectType) {
        case 'Element':
          return getters.element(activity.subjectId)
        default:
          return null
      }
    },
  },
}

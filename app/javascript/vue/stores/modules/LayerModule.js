import Base from './Base'
import Layer from '../../classes/models/Layer'

export default {
  state: {
    /** @type {Layer[]} */
    layers: [],
  },
  mutations: {
    setLayer: Base.mutations.setEntity('layers'),
    deleteLayer: Base.mutations.deleteEntity('layers'),
  },
  actions: {
    loadLayers: Base.actions.loadEntities(Layer, 'setLayer'),
    loadLayer: Base.actions.loadEntity(Layer, 'setLayer'),
    createLayer: Base.actions.createEntity(Layer, 'setLayer', 'layer'),
    updateLayer: Base.actions.updateEntity(Layer, 'setLayer', 'layer'),
    saveLayer: Base.actions.saveEntity(Layer, 'createLayer', 'updateLayer'),
    destroyLayer: Base.actions.destroyEntity(Layer, 'deleteLayer'),
  },
  getters: {
    layer: Base.getters.entity('layers'),
    layers: state => state.layers
      .sort((a, b) => b.position - a.position),
  },
}

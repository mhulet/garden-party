import Base from './Base'
import Genus from '../../classes/models/Genus'

export default {
  state: {
    /** @type {Genus[]} */
    genera: [],
  },
  mutations: {
    setGenus: Base.mutations.setEntity('genera'),
    deleteGenus: Base.mutations.deleteEntity('genera'),
  },
  actions: {
    loadGenera: Base.actions.loadEntities(Genus, 'setGenus'),
    loadGenus: Base.actions.loadEntity(Genus, 'setGenus'),
    createGenus: Base.actions.createEntity(Genus, 'setGenus', 'genus'),
    updateGenus: Base.actions.updateEntity(Genus, 'setGenus', 'genus'),
    saveGenus: Base.actions.saveEntity(Genus, 'createGenus', 'updateGenus'),
    destroyGenus: Base.actions.destroyEntity(Genus, 'deleteGenus'),
  },
  getters: {
    genera: Base.getters.entities('genera', 'name'),
    genus: Base.getters.entity('genera'),
  },
}

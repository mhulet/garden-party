export default {
  water: {
    icon: 'drop',
    // I18n use $t('js.elements.activities.water')
    i18n: 'js.elements.activities.water',
    // I18n use $t('js.elements.activities.watered')
    i18n_done: 'js.elements.activities.watered',
  },
  fertilize: {
    icon: 'lightning',
    // I18n use $t('js.elements.activities.fertilize')
    i18n: 'js.elements.activities.fertilize',
    // I18n use $t('js.elements.activities.fertilized')
    i18n_done: 'js.elements.activities.fertilized',
  },
  trim: {
    icon: 'scissors',
    // I18n use $t('js.elements.activities.trim')
    i18n: 'js.elements.activities.trim',
    // I18n use $t('js.elements.activities.trimmed')
    i18n_done: 'js.elements.activities.trimmed',
  },
  note: {
    icon: 'note',
    // I18n use $t('js.elements.activities.note')
    i18n: 'js.elements.activities.note',
    // I18n use $t('js.elements.activities.noted')
    i18n_done: 'js.elements.activities.noted',
  },
}

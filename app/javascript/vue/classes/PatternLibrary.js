import { DEVICE_PIXEL_RATIO } from 'ol/has'

/**
 * @typedef {import('./models/Resource')} Resource
 */

export default class PatternLibrary {
  constructor () {
    this.preloaded = {}
  }

  /**
   * Preload resources patterns and pattern for "multi-resource" patches.
   * If not done or done too late, elements backgrounds will be plain red.
   *
   * @param   {Resource[]} resources List of resources for which to create the patterns
   * @returns {Promise<*>}           Promise to wait before creating resources in maps
   */
  preloadPatterns (resources) {
    const promises = []
    // Multi resource pattern
    promises.push(this.createPicture('multiPattern', 40, '/pictures/multi_pattern.svg'))
    promises.push(this.createPicture('emptyPattern', 40, '/pictures/empty_pattern.svg'))

    // Resources patterns
    resources.forEach(resource => {
      promises.push(this.preloadPattern(resource))
    })

    return Promise.all(promises)
  }

  /**
   * Load a pattern for a given Resource
   *
   * @param   {Resource}   resource - Resource to load the pattern for
   * @returns {Promise<*>}          Promise that resolves when pattern is ready to use
   */
  preloadPattern (resource) {
    return this.createPicture(resource.id, 24, resource.getPatternPicturePath())
  }

  /**
   * Creates a pattern image
   *
   * @param   {string|number} patternKey - The key to use in patterns list
   * @param   {number}        size       - Pattern side size
   * @param   {string}        source     - URL for the image
   * @returns {Promise<*>}               Promise that resolves when pattern is ready to use
   */
  createPicture (patternKey, size, source) {
    return new Promise((resolve, reject) => {
      const canvas = document.createElement('canvas')
      const context = canvas.getContext('2d')
      canvas.width = size * DEVICE_PIXEL_RATIO
      canvas.height = size * DEVICE_PIXEL_RATIO
      const picture = document.createElement('img')
      picture.setAttribute('src', source)

      picture.addEventListener('load', () => {
        // Actually create the pattern when image is loaded
        this.preloaded[patternKey] = (function () {
          context.fillStyle = context.createPattern(picture, 'no-repeat')
          context.fillRect(0, 0, canvas.width, canvas.height)
          return context.createPattern(canvas, 'repeat')
        })()

        // Pattern is created
        resolve()
      })
      picture.addEventListener('error', (e) => reject(e))
    })
  }
}

import Model from './Model'
import callApi from '../../helpers/Api'
import Activity from './Activity'

/**
 * @typedef {import('./Resource')} Resource
 * @typedef {import('./Patch')}    Patch
 */

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['implantedAt', 'resourceId', 'patchId', 'createdAt', 'updatedAt', 'removedAt', 'implantationPlannedFor', 'removalPlannedFor', 'status']

class Element extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload                          - Data from API
   * @param {null|number} payload.id
   * @param {Date|string} payload.implanted_at
   * @param {number}      payload.resource_id
   * @param {number}      payload.patch_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {Date|string} payload.removed_at
   * @param {Date|string} payload.implantation_planned_for
   * @param {Date|string} payload.removal_planned_for
   * @param {string}      payload.status
   **/
  constructor ({ id = null, implanted_at, resource_id, patch_id, created_at, updated_at, removed_at, implantation_planned_for, removal_planned_for, status }) {
    super()
    this.id = id
    this.implantedAt = implanted_at ? new Date(implanted_at) : null
    this.resourceId = resource_id
    this.patchId = patch_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.removedAt = removed_at ? new Date(removed_at) : null
    this.implantationPlannedFor = implantation_planned_for ? new Date(implantation_planned_for) : null
    this.removalPlannedFor = removal_planned_for ? new Date(removal_planned_for) : null
    this.status = status
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /** @returns {Resource} Resource from VueX store */
  getResource () { return this.constructor.store.getters.resource(this.resourceId) }
  /** @returns {Activity[]} Related activities from VueX store */
  getActivities () { return this.constructor.store.getters.activitiesByElement(this.id) }
  /** @returns {Patch|null} Related patch from VueX store */
  getPatch () { return this.constructor.store.getters.patch(this.patchId) }
  /**
   * @param   {string[]}            actions - List of actions
   * @returns {Promise<Activity[]>}         Related actions from VueX store
   */
  getLastActions (actions) { return Activity.getActions(this.id, actions) }

  /**
   * Removes the element from the OpenLayers map
   */
  removeFromOLMap () {
    const patch = this.constructor.store.getters.patch(this.patchId)
    if (patch.geometryType === 'Point') patch.removeFromOLMap()
  }

  isRemoved () { return this.status === 'removed' }
  isImplanted () { return this.status === 'implanted' }
  isPlanned () { return this.status === 'planned' }
  isImplantationPlanned () { return this.isPlanned() && !!this.implantationPlannedFor }
  isRemovalPlanned () { return this.isImplanted() && !!this.removalPlannedFor }

  /**
   * @returns {string}  Element resource's name
   */
  getName () { return this.getResource().name }

  /**
   * Fetches the list for a given patch
   *
   * @param   {number}             patchId - Target patch identifier
   * @returns {Promise<Element[]>}         List of Element instances
   */
  static getPatchIndex (patchId) {
    return callApi('get', `/api/patches/${patchId}/elements`)
      .then(elements => elements.map(data => new Element(data)))
  }

  /**
   * @returns {string}  The hardcoded class name
   */
  static getClassName () { return 'Element' }
}

Element.updatableAttributes = ATTRIBUTES
Element.urls = {
  index (mapId) { return `/api/maps/${mapId}/elements` },
  show (id) { return `/api/elements/${id}` },
  create () { return '/api/elements' },
  update ({ payload }) { return `/api/elements/${payload.id}` },
  destroy (id) { return `/api/elements/${id}` },
}

export default Element

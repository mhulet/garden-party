import Model from './Model'

/**
 * @typedef {import('./Layer')}         Layer
 * @typedef {import('ol/Feature')}      Feature
 * @typedef {import('ol/layer/Vector')} VectorLayer
 */

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'geometry', 'mapId', 'createdAt', 'updatedAt', 'layerId', 'geometryType']

class Patch extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload               - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {object}      payload.geometry
   * @param {number}      payload.map_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {number}      payload.layer_id
   * @param {string}      payload.geometry_type
   **/
  constructor ({ id = null, name, geometry, map_id, created_at, updated_at, layer_id, geometry_type }) {
    super()
    this.id = id
    this.name = name
    this.geometry = geometry
    this.mapId = map_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.layerId = layer_id
    this.geometryType = geometry_type
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * Assigns an OpenLayer feature to the patch
   *
   * @param {Feature} feature - OpenLayer feature to assign
   */
  setFeature (feature) {
    feature.setId(this.id)
    this.feature = feature
  }

  /**
   * Removes the assigned feature from OpenLayer's map
   * FIXME: Is this used?
   */
  removeFromMapLayer () {
    if (!this.feature) return

    this.getMapLayer().getSource().removeFeature(this.feature)
    this.feature = null
  }

  /**
   * @returns {boolean}  True if removed or contains no element.
   */
  isRemoved () {
    if (this.geometryType !== 'Point') return false

    const element = this.getElements()[0]
    return element.isRemoved()
  }

  /**
   * Gets the patch elements from VueX store
   *
   * @returns {Element[]}  List of elements on this patch
   */
  getElements () { return this.constructor.store.getters.elementsByPatchId(this.id) }

  /**
   * Gets the layer on which the patch is
   *
   * @returns {(Layer|null)}  The layer
   */
  getLayer () { return this.constructor.store.getters.layer(this.layerId) }

  /**
   * Gets the map layer on which the patch is
   *
   * @returns {(VectorLayer|null)}  The map layer
   */
  getMapLayer () { return this.getLayer().mapLayer }

  /**
   * Determines the patch name from patch type and content
   *
   * @returns {string}  Determined patch name
   */
  getName () {
    if (this.geometryType === 'Point') {
      const name = this.getElements()[0].getResource().name
      if (this.name && name) return `${this.name} (${name})`
      else return name
    }

    return this.name || I18n.t('js.models.patch.generic_name')
  }

  /**
   * @returns {string}  The hardcoded class name
   */
  static getClassName () { return 'Patch' }
}

Patch.updatableAttributes = ATTRIBUTES
Patch.urls = {
  index (mapId) { return `/api/maps/${mapId}/patches` },
  show (id) { return `/api/patches/${id}` },
  create () { return '/api/patches' },
  update ({ payload }) { return `/api/patches/${payload.id}` },
  destroy (id) { return `/api/patches/${id}` },
}

export default Patch

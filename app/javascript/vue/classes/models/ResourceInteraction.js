import Model from './Model'

/**
 * @typedef {import('./Resource')} Resource
 */

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['nature', 'notes', 'subjectId', 'targetId', 'createdAt', 'updatedAt']

class ResourceInteraction extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload            - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.nature
   * @param {string}      payload.notes
   * @param {number}      payload.subject_id
   * @param {number}      payload.target_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   **/
  constructor ({ id = null, nature, notes, subject_id, target_id, created_at, updated_at }) {
    super()
    this.id = id
    this.nature = nature
    this.notes = notes
    this.subjectId = subject_id
    this.targetId = target_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * Find the subject in VueX store
   *
   * @returns {Resource}  Subject resource
   */
  getSubject () { return this.constructor.store.getters.resource(this.subjectId) }

  /**
   * Find the target in VueX store
   *
   * @returns {Resource}  Target resource
   */
  getTarget () { return this.constructor.store.getters.resource(this.targetId) }

  /**
   * Returns the other resource in this interaction
   *
   * @param   {number}        resourceId - First resource identifier
   * @returns {Resource|null}            The other resource
   */
  getOtherResource (resourceId) {
    if (resourceId === this.subjectId) return this.getTarget()
    else if (resourceId === this.targetId) return this.getSubject()

    return null
  }

  /**
   * @returns {string}  The hardcoded class name
   */
  static getClassName () { return 'ResourceInteraction' }
}

ResourceInteraction.updatableAttributes = ATTRIBUTES
ResourceInteraction.urls = {
  index () { return '/api/resource_interactions' },
  show (id) { return `/api/resource_interactions/${id}` },
  create () { return '/api/resource_interactions' },
  update ({ payload }) { return `/api/resource_interactions/${payload.id}` },
  destroy (id) { return `/api/resource_interactions/${id}` },
}

export default ResourceInteraction

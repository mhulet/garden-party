import Model from './Model'

/**
 * @typedef {import('ol/Feature')} Feature
 * @typedef {import('./Layer')}    Layer
 */

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'strokeWidth', 'strokeColor', 'strokeStylePreset', 'backgroundColor', 'geometry', 'mapId', 'createdAt', 'updatedAt', 'layerId', 'geometryType']

class Path extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload                     - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {number}      payload.stroke_width
   * @param {string}      payload.stroke_color
   * @param {string}      payload.stroke_style_preset
   * @param {string}      payload.background_color
   * @param {object}      payload.geometry
   * @param {number}      payload.map_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {number}      payload.layer_id
   * @param {string}      payload.geometry_type
   **/
  constructor ({ id = null, name, stroke_width, stroke_color, stroke_style_preset, background_color, geometry, map_id, created_at, updated_at, layer_id, geometry_type }) {
    super()
    this.id = id
    this.name = name
    this.strokeWidth = stroke_width
    this.strokeColor = stroke_color
    this.strokeStylePreset = stroke_style_preset
    this.backgroundColor = background_color
    this.geometry = geometry
    this.mapId = map_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.layerId = layer_id
    this.geometryType = geometry_type
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * Assigns an OpenLayer feature to the path
   *
   * @param {Feature} feature - OpenLayer feature to assign
   */
  setFeature (feature) {
    feature.setId(this.id)
    this.feature = feature
  }

  /**
   * Gets the layer on which the path is
   *
   * @returns {(Layer|null)}  The layer
   */
  getLayer () { return this.constructor.store.getters.layer(this.layerId) }

  /**
   * @returns {string}  The hardcoded class name
   */
  static getClassName () { return 'Path' }
}

Path.updatableAttributes = ATTRIBUTES
Path.urls = {
  index (mapId) { return `/api/maps/${mapId}/paths` },
  show (id) { return `/api/paths/${id}` },
  create () { return '/api/paths' },
  update ({ payload }) { return `/api/paths/${payload.id}` },
  destroy (id) { return `/api/paths/${id}` },
}

export default Path

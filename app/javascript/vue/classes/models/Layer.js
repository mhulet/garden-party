import Model from './Model'

/**
 * @typedef {import('ol/layer/Vector')} VectorLayer
 */

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'visibleByDefault', 'position', 'createdAt', 'updatedAt']

class Layer extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload                    - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {boolean}     payload.visible_by_default
   * @param {number}      payload.map_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {number}      payload.position
   **/
  constructor ({ id = null, name, visible_by_default, position, map_id, created_at, updated_at }) {
    super()
    this.id = id
    this.name = name
    this.visibleByDefault = visible_by_default
    this.position = position
    this.mapId = map_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  getPatches () { return this.constructor.store.getters.patchesInLayer(this.id) }

  /**
   * Assigns an OpenLayers layer to the map
   *
   * @param {VectorLayer} layer - The layer to assign
   */
  setMapLayer (layer) {
    this.mapLayer = layer
    layer.set('layerId', this.id)
  }

  removeFromMap () {
    this.mapLayer.getSource().removeLayer(this.mapLayer)
    this.mapLayer = null
  }

  /**
   * @returns {string}  The hardcoded class name
   */
  static getClassName () { return 'Layer' }
}

Layer.updatableAttributes = ATTRIBUTES
Layer.urls = {
  index (mapId) { return `/api/maps/${mapId}/layers` },
  show (id) { return `/api/layers/${id}` },
  create () { return '/api/layers' },
  update ({ payload }) { return `/api/layers/${payload.id}` },
  destroy (id) { return `/api/layers/${id}` },
}

export default Layer

import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['role', 'email', 'username', 'resetPasswordSentAt', 'confirmedAt', 'confirmationSentAt', 'unconfirmedEmail', 'createdAt', 'updatedAt']

class User extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload                        - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.role
   * @param {string}      payload.email
   * @param {string}      payload.username
   * @param {Date|string} payload.reset_password_sent_at
   * @param {Date|string} payload.confirmed_at
   * @param {Date|string} payload.confirmation_sent_at
   * @param {string}      payload.unconfirmed_email
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   **/
  constructor ({ id = null, role, email, username, reset_password_sent_at, confirmed_at, confirmation_sent_at, unconfirmed_email, created_at, updated_at }) {
    super()
    this.id = id
    this.role = role
    this.email = email
    this.username = username
    this.resetPasswordSentAt = reset_password_sent_at ? new Date(reset_password_sent_at) : null
    this.confirmedAt = confirmed_at ? new Date(confirmed_at) : null
    this.confirmationSentAt = confirmation_sent_at ? new Date(confirmation_sent_at) : null
    this.unconfirmedEmail = unconfirmed_email
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * @returns {string}  The hardcoded class name
   */
  static getClassName () { return 'User' }
}

User.updatableAttributes = ATTRIBUTES
User.urls = {
  index () { return '/api/users' },
  show (id) { return `/api/users/${id}` },
  create () { return '/api/users' },
  update ({ payload }) { return `/api/users/${payload.id}` },
  destroy (id) { return `/api/users/${id}` },
}

export default User

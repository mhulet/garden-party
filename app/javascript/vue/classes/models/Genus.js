import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'description', 'kingdom', 'createdAt', 'updatedAt', 'source']

class Genus extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload             - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {string}      payload.description
   * @param {string}      payload.kingdom
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {string}      payload.source
   **/
  constructor ({ id = null, name, description, kingdom, created_at, updated_at, source }) {
    super()
    this.id = id
    this.name = name
    this.description = description
    this.kingdom = kingdom
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.source = source
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  getResources () { return this.constructor.store.getters.resourcesByGenus(this.id) }

  /**
   * @returns {string}  The hardcoded class name
   */
  static getClassName () { return 'Genus' }
}

Genus.updatableAttributes = ATTRIBUTES
Genus.urls = {
  index () { return '/api/genera' },
  show (id) { return `/api/genera/${id}` },
  create () { return '/api/genera' },
  update ({ payload }) { return `/api/genera/${payload.id}` },
  destroy (id) { return `/api/genera/${id}` },
}

export default Genus

import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'description', 'edible', 'parentId', 'createdAt', 'updatedAt', 'genusId', 'commonNames', 'source', 'color', 'pictureName', 'borderColor', 'fillColor']

class Resource extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload              - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {string}      payload.description
   * @param {boolean}     payload.edible
   * @param {number}      payload.parent_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {number}      payload.genus_id
   * @param {object}      payload.common_names
   * @param {string}      payload.source
   * @param {string}      payload.color
   * @param {string}      payload.picture_name
   * @param {string}      payload.border_color
   * @param {string}      payload.fill_color
   **/
  constructor ({ id = null, name, description, edible, parent_id, created_at, updated_at, genus_id, common_names, source, color, picture_name, border_color, fill_color }) {
    super()
    this.id = id
    this.name = name
    this.description = description
    this.edible = edible
    this.parentId = parent_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.genusId = genus_id
    this.commonNames = common_names
    this.source = source
    this.color = color
    this.pictureName = picture_name
    this.borderColor = border_color
    this.fillColor = fill_color
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * Gets child resources from VueX store
   *
   * @returns {Resource[]}  The resource's child resources
   */
  getChildren () { return this.constructor.store.getters.childResources(this.id) }

  /**
   * Gets parent resource from VueX store
   *
   * @returns {(Resource|null)}  The resource's parent resource
   */
  getParent () { return this.constructor.store.getters.resource(this.parentId) }

  /**
   * Builds a relative path to the resource's picture
   *
   * @returns {string}  Path to resource picture, to use as markers sources
   */
  getPicturePath () {
    return `/pictures/resources/${this.pictureName}_${this.id}.svg`
  }

  /**
   * Builds a relative path to the resource smaller picture
   *
   * @returns {string}  Path to resource smaller picture, to use as zone background pattern
   */
  getPatternPicturePath () {
    return `/pictures/resources/${this.pictureName}_${this.id}_pattern.svg`
  }

  /**
   * @returns {string}  The hardcoded class name
   */
  static getClassName () { return 'Resource' }
}

Resource.updatableAttributes = ATTRIBUTES
Resource.urls = {
  index () { return '/api/resources' },
  show (id) { return `/api/resources/${id}` },
  create () { return '/api/resources' },
  update ({ payload }) { return `/api/resources/${payload.id}` },
  destroy (id) { return `/api/resources/${id}` },
}

export default Resource

import callApi from '../../helpers/Api'
import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'center', 'userId', 'createdAt', 'updatedAt', 'extentWidth', 'extentHeight', 'publiclyAvailable', 'picture']

class Map extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}                               payload                    - Data from API
   * @param {null|number}                          payload.id
   * @param {string}                               payload.name
   * @param {{x:number, y:number}}                 payload.center
   * @param {number}                               payload.user_id
   * @param {Date|string}                          payload.created_at
   * @param {Date|string}                          payload.updated_at
   * @param {number}                               payload.extent_width
   * @param {number}                               payload.extent_height
   * @param {boolean}                              payload.publicly_available
   * @param {({url: string, size: number[]}|null)} payload.picture
   **/
  constructor ({ id = null, name, center, user_id, created_at, updated_at, extent_width, extent_height, publicly_available, picture }) {
    super()
    this.id = id
    this.name = name
    this.center = [center.x, center.y]
    this.userId = user_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.extentWidth = extent_width
    this.extentHeight = extent_height
    this.publiclyAvailable = publicly_available
    this.picture = picture
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  isOSM () {
    return this.picture === null
  }

  /**
   * Creates a new map
   *
   * @param   {object}       payload - Object with underscored keys
   * @returns {Promise<Map>}         Map instance
   */
  static create (payload) {
    const formData = new FormData()
    formData.append('map[name]', payload.name)
    formData.append('map[center][]', payload.center[0])
    formData.append('map[center][]', payload.center[1])
    formData.append('map[picture]', payload.picture)
    formData.append('map[extent_height]', payload.extent_height)
    formData.append('map[extent_width]', payload.extent_width)
    formData.append('map[publicly_available]', payload.publicly_available)

    return callApi('post', '/api/maps', formData)
      .then(map => new Map(map))
  }

  /**
   * @returns {string}  The hardcoded class name
   */
  static getClassName () { return 'Map' }
}

Map.updatableAttributes = ATTRIBUTES
Map.urls = {
  index () { return '/api/maps' },
  show (id) { return `/api/maps/${id}` },
  update ({ payload }) { return `/api/maps/${payload.id}` },
  destroy (id) { return `/api/maps/${id}` },
}

export default Map

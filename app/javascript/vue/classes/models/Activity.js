import callApi from '../../helpers/Api'
import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'notes', 'createdAt', 'updatedAt', 'doneAt', 'plannedFor', 'subjectType', 'subjectId', 'pictures']

class Activity extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}                                payload              - Data from API
   * @param {null|number}                           payload.id
   * @param {string}                                payload.name
   * @param {string}                                payload.notes
   * @param {Date|string}                           payload.created_at
   * @param {Date|string}                           payload.updated_at
   * @param {Date|string}                           payload.done_at
   * @param {Date|string}                           payload.planned_for
   * @param {string}                                payload.subject_type
   * @param {number}                                payload.subject_id
   * @param {{source: string, thumbnail: string}[]} payload.pictures
   **/
  constructor ({ id = null, name, notes, created_at, updated_at, done_at, planned_for, subject_type, subject_id, pictures }) {
    super()
    this.id = id
    this.name = name
    this.notes = notes
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.doneAt = done_at ? new Date(done_at) : null
    this.plannedFor = planned_for ? new Date(planned_for) : null
    this.subjectType = subject_type
    this.subjectId = subject_id
    this.pictures = pictures
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  getSubject () { return this.constructor.store.getters.activitySubject(this.id) }

  /**
   * Fetches last activities filtered by action for given element
   *
   * @param   {number}            mapId - Target map identifier
   * @returns {Promise<string[]>}       List of used activities names
   */
  static getNames (mapId) {
    return callApi('get', `/api/maps/${mapId}/activities/names`)
  }

  /**
   * Fetches last activities filtered by action for given element
   *
   * @param   {number}              elementId - Target element identifier
   * @param   {string[]}            actions   - List of actions to filter
   * @returns {Promise<Activity[]>}           Filtered list of Activity instances
   */
  static getActions (elementId, actions = []) {
    return callApi('get', `/api/elements/${elementId}/activities`, { last_actions: actions })
      .then(activities => activities.map(data => new Activity(data)))
  }

  /**
   * Fetches pending activities for given map
   *
   * @param   {number}              mapId - Target map identifier
   * @returns {Promise<Activity[]>}       Filtered list of Activity instances
   */
  static getPending (mapId) {
    return callApi('get', `/api/maps/${mapId}/activities?pending=true`)
      .then(activities => activities.map(data => new Activity(data)))
  }

  /**
   * Creates a new activity
   *
   * @param   {object}            payload - Object with underscored keys
   * @returns {Promise<Activity>}         Activity instance
   */
  static create (payload) {
    const formData = new FormData()
    for (const key of Object.keys(payload)) {
      if (key === 'pictures') {
        for (const file of payload[key]) formData.append(`activity[${key}][]`, file)
      } else { formData.append(`activity[${key}]`, payload[key]) }
    }

    return callApi('post', '/api/activities', formData)
      .then(activity => new Activity(activity))
  }

  /**
   * @returns {string}  The hardcoded class name
   */
  static getClassName () { return 'Activity' }
}

Activity.updatableAttributes = ATTRIBUTES
Activity.urls = {
  index (elementId) { return `/api/elements/${elementId}/activities` },
  show (id) { return `/api/activities/${id}` },
  update ({ payload }) { return `/api/activities/${payload.id}` },
  destroy (id) { return `/api/activities/${id}` },
}

export default Activity

import callApi from '../../helpers/Api'

/**
 * @typedef {import('vuex').Store} VueXStore
 */

export default class Model {
  /**
   * Update from another instance
   *
   * @param {Map} newInstance - Instance to merge data from
   */
  updateAttributes (newInstance) {
    this.constructor.updatableAttributes.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  static getClassName () { throw new Error('Class name not defined in model') }

  static set updatableAttributes (attributes) { this.UPDATABLE_ATTRIBUTES = attributes }

  static get updatableAttributes () { return this.UPDATABLE_ATTRIBUTES }

  /* eslint-disable accessor-pairs */
  static set urls (urls) {
    /**
     * @type {object}
     */
    this.urlsList = urls
  }
  /* eslint-enable accessor-pairs */

  /**
   * Assigns VueX Store to Store for usage in methods
   *
   * @param {VueXStore} store VueX Store instance
   */
  static set store (store) { this.VueXStore = store }

  /**
   * Reach the store
   *
   * @returns {VueXStore}  store VueX Store instance
   */
  static get store () { return this.VueXStore }

  /**
   * Fetches the list
   *
   * @param   {*}                params - Parameters for API call
   * @returns {Promise<Model[]>}        List of entities
   */
  static getIndex (params) {
    return callApi('get', this.urlsList.index(params))
      .then(entities => entities.map(entity => new this(entity)))
  }

  /**
   * Fetches one map
   *
   * @param   {*}              params - Parameters for API call
   * @returns {Promise<Model>}        Model instance
   */
  static get (params) {
    return callApi('get', this.urlsList.show(params))
      .then(entity => new this(entity))
  }

  /**
   * Creates a new entity
   *
   * @param   {*}              params - Parameters for API call
   * @returns {Promise<Model>}        Model instance
   */
  static create (params) {
    return callApi('post', this.urlsList.create(params), params)
      .then(entity => new this(entity))
  }

  /**
   * Updates one entity
   *
   * @param   {*}              params - Parameters for API call
   * @returns {Promise<Model>}        Model instance
   */
  static update (params) {
    return callApi('put', this.urlsList.update(params), params.payload)
      .then(entity => new this(entity))
  }

  /**
   * Destroys one entity
   *
   * @param   {*}             params - Call parameters
   * @returns {Promise<null>}        Nothing on success
   */
  static destroy (params) {
    return callApi('delete', this.urlsList.destroy(params))
  }
}

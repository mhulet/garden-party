// Base map requirements
import OlMap from 'ol/Map'
import View from 'ol/View'
import Projection from 'ol/proj/Projection'
import { ScaleLine } from 'ol/control'
// For user-submitted background layer
import ImageLayer from 'ol/layer/Image'
import StaticSource from 'ol/source/ImageStatic'
// For OSM map
import TileLayer from 'ol/layer/Tile'
import OSMSource from 'ol/source/OSM'
// For vector layers (plantationLayer representations)
import VectorLayer from 'ol/layer/Vector'
import VectorSource from 'ol/source/Vector'
// For map interactions
import DrawInteraction from 'ol/interaction/Draw'
import ModifyInteraction from 'ol/interaction/Modify'
import RotateFeatureInteraction from 'ol-rotate-feature'
import TranslateInteraction from 'ol/interaction/Translate'
// For feature insertions ("markers", "shapes", ...)
import Feature from 'ol/Feature'
import Collection from 'ol/Collection'
// For drawing shapes
import CircleGeom from 'ol/geom/Circle'
import LineStringGeom from 'ol/geom/LineString'
import { getArea, getLength } from 'ol/sphere'
// For features styling
import Style from 'ol/style/Style'
import FillStyle from 'ol/style/Fill'
import StrokeStyle from 'ol/style/Stroke'
import IconStyle from 'ol/style/Icon'
import CircleStyle from 'ol/style/Circle'
import PatternLibrary from './PatternLibrary'
// Points representations (primarily for styles)
import MultiPoint from 'ol/geom/MultiPoint'
import PointGeom from 'ol/geom/Point'
// For map data import/export
import GeoJSON from 'ol/format/GeoJSON'
// For popups
import Overlay from 'ol/Overlay'
// VueX store
import Store from '../apps/garden/store'
// Models
import Patch from './models/Patch'
import Path from './models/Path'
// Other
import strokeStylePresets from '../strokeStylePresets'
import { formatLength, formatArea } from '../helpers/MeasureFormatters'

import { unByKey } from 'ol/Observable'

/**
 * @typedef {import('ol/interaction/Interaction')} Interaction
 * @typedef {import('./models/Resource')}          Resource
 * @typedef {import('./models/Layer')}             Layer
 */

const ALLOWED_SHAPES = {
  patch: ['Point', 'Circle', 'Polygon'],
  path: ['LineString', 'Circle', 'Polygon'],
}

/** @type {Style[]} Style for elements in an interaction */
const INTERACTION_STYLE = [
  new Style({
    stroke: new StrokeStyle({ color: 'rgb(0,153,255)', width: 3 }),
    fill: new FillStyle({ color: 'rgba(255,255,255, 0.4)' }),
  }),
  new Style({
    image: new CircleStyle({
      radius: 5,
      fill: new FillStyle({ color: 'rgb(255,0,153)' }),
    }),
    geometry: function (feature) {
      const coordinates = feature.getGeometry().getCoordinates()
      if (!coordinates) return null
      // Polygon
      if (coordinates[0][0] instanceof Array) return new MultiPoint(coordinates[0])
      // LineString
      else if (coordinates[0] instanceof Array) return new MultiPoint(coordinates)
      // Point
      return new PointGeom(coordinates)
    },
  }),
]

export default class GardenMap {
  /**
   * Prepares a new map to display.
   *
   * @param {Map}    mapConfig        - Map configuration from API
   * @param {string} overlayElementId - DOM element ID of the popup box
   * @param {string} elementId        - Target DOM element ID
   */
  constructor (mapConfig, overlayElementId, elementId) {
    /** @type {Map} **/
    this.mapConfig = mapConfig
    /** @type {HTMLElement} Map element, to bind events */
    this.mapElement = document.getElementById(elementId)

    /** @type {string|null} Current interaction mode */
    this.mode = null
    /** @type {Feature|null} The pointed feature */
    this.pointedFeature = null

    /** @type {Interaction|null} Main current interaction */
    this.interaction = null

    /** @type {string} Overlay component for patch/path information popup */
    this.overlayElementId = overlayElementId
    /** @type {Overlay|null} The map overlay for popups */
    this.overlay = null

    /** @type {HTMLElement} Tooltip element, to bind events */
    this.measureTooltipElement = null
    /** @type {Overlay|null} The map overlay for popups */
    this.measureTooltipOverlay = null

    /** @type {PatternLibrary} Patterns to use as shapes backgrounds */
    this.patterns = new PatternLibrary()

    this._createMap(elementId)
    this._createLayers()

    const resources = Store.getters.resources
    this.patterns.preloadPatterns(resources)
      .then(() => {
        for (const path of Store.getters.paths) this._addEntity(path)
        for (const patch of Store.getters.activePatches) this._addEntity(patch)
      })
  }

  /**
   * Sets map in a "drawing mode", to add points and geometries
   *
   * @param {string}   shape    - Shape type. Check code for allowed values
   * @param {Resource} resource - Target resource type, to define target layer and style
   * @param {Layer}    layer    - Layer on which to draw
   */
  drawPatch (shape, resource, layer) {
    this._drawEntity({
      type: 'patch',
      shape,
      layer,
      backgroundColor: resource.fillColor,
      strokeColor: resource.borderColor,
      strokeWidth: 1,
      patternId: resource.id,
      pointSource: resource.getPicturePath(),
      onEnd: (feature) => {
        this._notify('add-patch', {
          resourceId: resource.id,
          geoJSON: this._geoJSONFromFeature(feature),
          feature,
        })
      },
    })
  }

  /**
   * Sets map in a "drawing path mode", to add geometries
   *
   * @param {string} shape                   - Shape type. Check code for allowed values
   * @param {object} style                   - Path style
   * @param {string} style.backgroundColor   - rgba like '123,123,123,1'
   * @param {string} style.strokeColor       - rgba like '123,123,123,1'
   * @param {number} style.strokeWidth       - Thickness
   * @param {number} style.strokeStylePreset - Preset for stroke style
   * @param {Layer}  layer                   - Target layer
   */
  drawPath (shape, { backgroundColor, strokeColor, strokeWidth, strokeStylePreset }, layer) {
    this._drawEntity({
      type: 'path',
      shape,
      layer,
      backgroundColor: `rgba(${backgroundColor})`,
      strokeColor: `rgba(${strokeColor})`,
      strokeWidth,
      strokeStylePreset,
      onEnd: (feature) => {
        this._notify('add-path', {
          geoJSON: this._geoJSONFromFeature(feature),
          feature,
          backgroundColor,
          strokeColor,
          strokeWidth,
          strokeStylePreset,
        })
      },
    })
  }

  /**
   * Stops drawing mode
   */
  stopInteraction () {
    // Hide popup
    this.overlay.setPosition(null)

    // Style back the pointed feature
    if (this.interactionStyle) {
      if (!this.pointedFeature) throw new Error('No feature to revert style')

      this.pointedFeature.setStyle(this.interactionStyle.clone())
      this.interactionStyle = null
    }

    // Stop pointing something
    if (this.pointedFeature) {
      this.pointedFeature = null
      this._notify('point-entity', null)
    }

    // Remove interaction
    if (this.interaction) {
      this.gardenMap.removeInteraction(this.interaction)
      this.interaction = null
    }
  }

  /**
   * Puts map in review mode: click somewhere and get info
   */
  review () {
    this.stopInteraction()

    this.mode = 'review'
    this.gardenMap.addOverlay(this.overlay)
  }

  /**
   * Moves a feature from its declared mapLayer to its current layer
   *
   * @param {Layer}      fromLayer - Original layer
   * @param {Path|Patch} entity    - The updated patch to move
   */
  moveEntityToOtherLayer (fromLayer, entity) {
    fromLayer.mapLayer.getSource().removeFeature(entity.feature)
    if (entity instanceof Patch) this._addEntity(entity, 'patch')
    else if (entity instanceof Path) this._addEntity(entity, 'path')
  }

  /**
   * Start rotating an element
   *
   * @param {Patch|Path} element - Element to rotate
   */
  rotateElement (element) {
    this._interact(element, RotateFeatureInteraction, 'rotateend')
  }

  /**
   * Start transforming an element
   *
   * @param {Patch|Path} element - Element to transform
   */
  transformElement (element) {
    this._interact(element, ModifyInteraction, 'modifyend')
  }

  /**
   * Start translating an element
   *
   * @param {Patch|Path} element - Element to translate
   */
  translateElement (element) {
    this._interact(element, TranslateInteraction, 'translateend')
  }

  /**
   * Removes a given feature
   *
   * @param {Path|Patch} entity - Path or Patch to remove
   */
  removeEntityFeature (entity) {
    entity.getLayer().mapLayer.getSource().removeFeature(entity.feature)
  }

  /**
   * Redraws a given feature from its entity
   *
   * @param {Path|Patch} entity - Entity to redraw
   */
  redrawEntity (entity) {
    entity.feature.setStyle(this._styleFromEntity(entity))
  }

  /**
   * Toggle layers visibility
   *
   * @param {number[]} ids - List of enabled layer IDs
   */
  setVisibleLayers (ids) {
    for (const plantationLayer of Store.getters.layers) {
      plantationLayer.mapLayer.setVisible(ids.indexOf(plantationLayer.id) > -1)
    }
  }

  /**
   * Adds a layer to the map from a Layer instance
   *
   * @param {Layer} layer - The layer
   */
  addLayer (layer) {
    const source = new VectorSource({ wrapX: false })
    const vectorLayer = new VectorLayer({ source })
    vectorLayer.setZIndex(layer.position)

    this.gardenMap.addLayer(vectorLayer)

    // Assigns the map layer to layer for reference
    layer.setMapLayer(vectorLayer)
  }

  /**
   * Removes a layer from the map
   *
   * @param {Layer} patchLayer - The layer to remove
   */
  removeLayer (patchLayer) { this.gardenMap.removeLayer(patchLayer.mapLayer) }

  /**
   * Removes map from element, can be nulled after
   */
  destroy () {
    this.gardenMap.setTarget(null)
  }

  /**
   * Initializes the map and insert it in DOM
   *
   * @param   {string} elementId - Target DOM element ID
   * @private
   */
  _createMap (elementId) {
    const viewOptions = {
      center: [this.mapConfig.center[0], this.mapConfig.center[1]],
    }

    let extent
    const layers = []
    this.projection = new Projection({ code: window.MAPS_PROJECTION, unit: 'meter' })

    // Background layers
    if (this.mapConfig.isOSM()) {
      viewOptions.zoom = 20
      layers.push(new TileLayer({ source: new OSMSource(), projection: this.projection, extent }))
    } else {
      extent = [0, 0, this.mapConfig.extentWidth, this.mapConfig.extentHeight]
      this.projection.setExtent(extent)
      viewOptions.zoom = 2
      layers.push(new ImageLayer({
        source: new StaticSource({
          url: this.mapConfig.picture.url,
          imageExtent: extent,
        }),
      }))
      viewOptions.projection = this.projection
    }

    this.gardenMap = new OlMap({
      target: elementId,
      layers,
      view: new View(viewOptions),
    })

    this.gardenMap.addControl(new ScaleLine({ units: 'metric' }))

    // Create the overlay
    this.overlay = new Overlay({
      element: document.getElementById(this.overlayElementId),
      positioning: 'bottom-center',
      stopEvent: false,
      offset: [0, 50],
    })

    // Attach overlay handler
    this.gardenMap.on('singleclick', (event) => {
      if (event.originalEvent.target.tagName !== 'CANVAS') return
      if (this.mode !== 'review') return

      const feature = this.gardenMap.forEachFeatureAtPixel(event.pixel, feature => feature)

      // Do nothing if no feature is selected
      if (feature) {
        // Stop here if feature is already selected
        if (this._isPointedFeature(feature)) return
        this.stopInteraction()

        const id = feature.getId()
        const type = feature.getProperties().type
        this.pointedFeature = feature

        if (this.mode === 'review') this.overlay.setPosition(event.coordinate)
        this._notify('point-entity', { type, id })
      } else {
        this.stopInteraction()
      }
    })
  }

  /**
   * Checks if the given feature is already pointed
   *
   * @param   {Feature} feature - Feature to compare
   * @returns {boolean}         True when its... true
   * @private
   */
  _isPointedFeature (feature) {
    if (!this.pointedFeature) return false

    return this.pointedFeature.getId() === feature.getId() && this.pointedFeature.getProperties().type === feature.getProperties().type
  }

  /**
   * Creates the layers from Vuex store
   *
   * @private
   */
  _createLayers () {
    for (const patchLayer of Store.getters.layers) {
      this.addLayer(patchLayer)
    }
  }

  /**
   * Adds a geometry from an entity to the map
   *
   * @param   {Patch|Path} entity - Entity to add
   * @private
   */
  _addEntity (entity) {
    const feature = this._featureFromGeometry(entity.geometry, entity.geometryType)

    const type = entity.constructor.getClassName().toLowerCase()
    feature.setStyle(this._styleFromEntity(entity))
    feature.setId(entity.id)
    feature.setProperties({ type })

    entity.getLayer().mapLayer.getSource().addFeature(feature)
    entity.setFeature(feature)
  }

  /**
   * Sets the map in drawing mode for a given shape and parameters
   *
   * @param   {object}            parameters                   - Configuration values
   * @param   {string}            parameters.type              - Type of entity (path/patch)
   * @param   {string}            parameters.shape             - Shape of the thing to draw
   * @param   {Layer}             parameters.layer             - Target Layer in garden
   * @param   {string}            parameters.backgroundColor   - Color string like `rgb(x,y,z)` or `rgba(x,y,z,a)`
   * @param   {string}            parameters.strokeColor       - Color string like `rgb(x,y,z)` or `rgba(x,y,z,a)`
   * @param   {number}            parameters.strokeWidth       - Outer line width
   * @param   {number|null}       parameters.patternId         - Pattern id in patterns library, if any
   * @param   {string|null}       parameters.pointSource       - Source URL when adding points
   * @param   {function(feature)} parameters.onEnd             - Callback called when user finished its art
   * @param   {string|null}       parameters.strokeStylePreset - Preset name for stroke style
   * @private
   */
  _drawEntity ({ type, shape, layer, backgroundColor, strokeColor, strokeWidth, strokeStylePreset = null, patternId = null, pointSource = null, onEnd = () => {} }) {
    this.stopInteraction()
    if (!type || !ALLOWED_SHAPES[type] || ALLOWED_SHAPES[type].indexOf(shape) === -1) throw new Error(`Unsupported shape ${shape} for a ${type}`)
    const that = this

    this.mode = `draw-${type}`

    const targetLayer = layer.mapLayer
    if (!targetLayer) throw new Error('Target layer not found')
    if (shape === 'Point' && !pointSource) throw new Error('No source provided for Point')

    let style
    if (shape === 'Point') style = this._styleForPoint(pointSource)
    else style = this._styleForShape(backgroundColor, strokeColor, strokeWidth, strokeStylePreset, patternId)

    const source = targetLayer.getSource()

    this.interaction = new DrawInteraction({ source, type: shape, style })
    let listener

    this.interaction.on('drawend', (event) => {
      const feature = event.feature
      feature.setStyle(style)
      feature.setProperties({ type })

      onEnd(feature)
      unByKey(listener)
      this.gardenMap.removeOverlay(this.measureTooltipOverlay)
    })

    this.gardenMap.addInteraction(this.interaction)

    if (shape === 'Point') return
    this._createMeasureTooltip()

    // Measurement tooltip for anything but points
    this.interaction.on('drawstart', (event) => {
      let tooltipCoord = event.coordinate
      const sketch = event.feature
      listener = sketch.getGeometry().on('change', function (event) {
        const geom = event.target
        let measure

        if (shape === 'LineString') {
          measure = formatLength(getLength(geom))
          tooltipCoord = geom.getLastCoordinate()
        } else if (shape === 'Polygon') {
          measure = formatArea(getArea(geom))
          tooltipCoord = geom.getInteriorPoint().getCoordinates()
        } else if (shape === 'Circle') {
          const radius = getLength(new LineStringGeom([[0, 0], [0, geom.getRadius()]]))
          measure = formatArea(Math.PI * Math.pow(radius, 2))
          tooltipCoord = geom.getLastCoordinate()
        } else {
          throw new Error(`Can't measure a ${shape}`)
        }

        that.measureTooltipElement.innerHTML = measure
        that.measureTooltipOverlay.setPosition(tooltipCoord)
      })
    })
  }

  /**
   * Creates the measurement tooltip
   *
   * @private
   */
  _createMeasureTooltip () {
    if (this.measureTooltipElement) {
      this.measureTooltipElement.parentNode.removeChild(this.measureTooltipElement)
    }
    this.measureTooltipElement = document.createElement('div')
    this.measureTooltipElement.className = 'ol-tooltip hidden'
    this.measureTooltipOverlay = new Overlay({
      element: this.measureTooltipElement,
      offset: [15, 0],
      positioning: 'center-left',
    })
    this.gardenMap.addOverlay(this.measureTooltipOverlay)
  }

  /**
   * Creates an interaction on the given element
   *
   * @param   {Patch|Path}  element          - Element to interact with
   * @param   {Interaction} InteractionClass - Target interaction class
   * @param   {string}      endEvent         - Interaction event to listen for
   * @private
   */
  _interact (element, InteractionClass, endEvent) {
    this.stopInteraction()

    const feature = element.feature
    const properties = feature.getProperties()
    this.interactionStyle = feature.getStyle().clone()
    feature.setStyle(INTERACTION_STYLE)
    this.pointedFeature = feature

    const interaction = new InteractionClass({ features: new Collection([feature]) })
    interaction.on(endEvent, () => {
      const eventName = properties.type === 'patch' ? 'update-patch' : 'update-path'
      this._notify(eventName, { id: feature.getId(), feature, geoJSON: this._geoJSONFromFeature(feature) })
    })

    this.interaction = interaction
    this.gardenMap.addInteraction(this.interaction)
  }

  /**
   * @param   {Patch|Path}    entity - Entity from which to define the style
   * @returns {Style|Style[]}        Style to apply
   * @private
   */
  _styleFromEntity (entity) {
    if (entity instanceof Path) {
      return this._styleForShape(`rgba(${entity.backgroundColor})`, `rgba(${entity.strokeColor})`, entity.strokeWidth, entity.strokeStylePreset)
    } else {
      const elements = entity.getElements()
      const amount = elements.length

      if (entity.geometryType === 'Point') {
        const resource = elements[0].getResource()
        return this._styleForPoint(resource.getPicturePath())
      } else {
        let patternId, borderColor
        let fillColor = 'transparent'
        if (amount === 1) {
          const resource = elements[0].getResource()
          patternId = resource.id
          borderColor = resource.borderColor
          fillColor = resource.fillColor
        } else if (amount > 1) {
          patternId = 'multiPattern'
          borderColor = 'gray'
        } else {
          patternId = 'emptyPattern'
          borderColor = 'lightgrey'
        }

        return this._styleForShape(fillColor, borderColor, 1, null, patternId)
      }
    }
  }

  /**
   * @param   {string} src - Picture source URL
   * @returns {Style}      Style to apply to a point
   * @private
   */
  _styleForPoint (src) {
    return new Style({
      image: new IconStyle({
        anchor: [0.5, 0.5],
        anchorXUnits: 'fraction',
        anchorYUnits: 'fraction',
        src,
      }),
    })
  }

  /**
   * @param   {string}      fillColor           - Fill color
   * @param   {string}      borderColor         - Border color
   * @param   {number}      strokeWidth         - Stroke width
   * @param   {string|null} [strokeStylePreset] - Style preset for stroke line
   * @param   {number|null} [patternId]         - Pattern index in preloaded patterns
   * @returns {Style}                           Style to apply to a fillable patch
   * @private
   */
  _styleForShape (fillColor, borderColor, strokeWidth = 1, strokeStylePreset = null, patternId = null) {
    const color = patternId ? this.patterns.preloaded[patternId] : fillColor

    const lineDash = (strokeStylePreset && strokeStylePresets[strokeStylePreset]) ? strokeStylePresets[strokeStylePreset].style(strokeWidth) : null

    const styleFill = new FillStyle({ color })
    const strokeStyle = new StrokeStyle({ color: borderColor, width: strokeWidth, lineDash, lineCap: 'butt' })
    return new Style({
      fill: styleFill,
      stroke: strokeStyle,
    })
  }

  /**
   * @param   {string} event - Event to dispatch
   * @param   {*}      data  - Event additional payload
   * @private
   */
  _notify (event, data) {
    this.mapElement.dispatchEvent(new CustomEvent(event, { detail: data }))
  }

  /**
   * @typedef                                        GeoJSONObject
   * @member  {{coordinate: number[], type: string}} geometry
   * @member  {string}                               type
   * @member  {{radius: number}}                     properties
   */
  /**
   * @param   {Feature}       feature - Map feature
   * @returns {GeoJSONObject}         GeoJSON representation
   * @private
   */
  _geoJSONFromFeature (feature) {
    let geoJSON
    if (feature.getGeometry().getType() === 'Circle') {
      const geometry = feature.getGeometry().getFlatCoordinates()
      geoJSON = {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [geometry[0], geometry[1]],
        },
        properties: { radius: feature.getGeometry().getRadius() },
      }
    } else {
      const formatter = new GeoJSON()
      geoJSON = formatter.writeFeatureObject(feature)
    }

    return geoJSON
  }

  /**
   * @param   {object}  geometry - Geometry from entity
   * @param   {string}  shape    - Desired shape
   * @returns {Feature}          The feature object
   * @private
   */
  _featureFromGeometry (geometry, shape) {
    if (shape === 'Circle') {
      return new Feature(new CircleGeom([geometry.geometry.coordinates[0], geometry.geometry.coordinates[1]], geometry.properties.radius))
    } else {
      return new GeoJSON().readFeature(geometry)
    }
  }
}

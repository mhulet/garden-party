import { config, mount } from '@vue/test-utils'
import ModalComponent from './Modal'

config.mocks.$t = (string) => { return string }

describe('interface', () => {
  describe('with only title prop', () => {
    const wrapper = mount(ModalComponent, {
      propsData: { title: 'Hello world' },
    })

    test('displays title', () => {
      expect(wrapper.text()).toContain('Hello world')
    })

    test('has only the validation button by default', () => {
      expect(wrapper.text()).toContain('generic.ok')
      expect(wrapper.findAll('.modal__footer button')).toHaveLength(1)
    })
  })

  describe('with cancelable option', () => {
    const wrapper = mount(ModalComponent, {
      propsData: {
        title: 'Hello world',
        cancellable: true,
      },
    })

    test('has a cancel button', () => {
      expect(wrapper.text()).toContain('generic.cancel')
    })
  })

  describe('without footer', () => {
    const wrapper = mount(ModalComponent, {
      propsData: {
        title: 'Hello world',
        hasFooter: false,
      },
    })

    test('has no footer', () => {
      expect(wrapper.text()).not.toContain('generic.cancel')
      expect(wrapper.text()).not.toContain('generic.ok')
    })
  })

  describe('with a slot', () => {
    const slotContent = 'This is the slot content'
    const wrapper = mount(ModalComponent, {
      propsData: {
        title: 'Hello world',
        hasFooter: false,
      },
      slots: { default: slotContent },
    })

    test('renders slot', () => {
      expect(wrapper.text()).toContain(slotContent)
    })
  })
})

// TODO: Test interactions

const base = {
  Point: 'point',
  Polygon: 'polygon',
  Circle: 'circle',
  LineString: 'multiline',
}

const references = {}

Object.keys(base).forEach(k => {
  references[k] = {
    base: base[k],
    add: `${base[k]}-add`,
    edit: `${base[k]}-edit`,
  }
})

export default references

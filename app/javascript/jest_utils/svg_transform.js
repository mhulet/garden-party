// Mocks SVG files with a simple string
module.exports = {
  process () {
    return 'module.exports = {};'
  },
  getCacheKey () {
    // The output is always the same.
    return 'svg_transform'
  },
}

import Vue from 'vue'
import App from '../vue/apps/garden/App'
import router from '../vue/apps/garden/router'
import store from '../vue/apps/garden/store'
import i18n from '../vue/helpers/i18n'
import VueBar from 'vuebar'

document.addEventListener('DOMContentLoaded', () => {
  Vue.config.productionTip = false

  Vue.use(VueBar)

  new Vue({
    router,
    i18n,
    store,
    render: h => h(App),
  }).$mount('#app')
})

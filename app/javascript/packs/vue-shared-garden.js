import Vue from 'vue'
import App from '../vue/apps/shared_garden/App'
import store from '../vue/apps/shared_garden/store'
import i18n from '../vue/helpers/i18n'
import VueBar from 'vuebar'

window.sharedMapApp = function (mapId) {
  document.addEventListener('DOMContentLoaded', () => {
    Vue.config.productionTip = false

    Vue.use(VueBar)

    Vue.prototype.$mapId = mapId

    new Vue({
      i18n,
      store,
      render: h => h(App),
    }).$mount('#app')
  })
}

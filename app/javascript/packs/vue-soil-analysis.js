import Vue from 'vue'
import App from '../vue/apps/soil_analysis/App'
import i18n from '../vue/helpers/i18n'

document.addEventListener('DOMContentLoaded', () => {
  Vue.config.productionTip = false

  new Vue({
    i18n,
    render: h => h(App),
  }).$mount('#app')
})

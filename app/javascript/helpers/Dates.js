/**
 * Checks if a date is the same day as another
 *
 * @param   {Date}    firstDate  - First date
 * @param   {Date}    secondDate - Second date
 * @returns {boolean}            True if dates are in the same day, false otherwise
 */
export function isSameDay (firstDate, secondDate = new Date()) {
  return (firstDate.getFullYear() === secondDate.getFullYear()) &&
    (firstDate.getMonth() === secondDate.getMonth()) &&
    (firstDate.getDate() === secondDate.getDate())
}

/**
 * Checks if a date is the same month as another
 *
 * @param   {Date}    firstDate  - First date
 * @param   {Date}    secondDate - Second date
 * @returns {boolean}            True if dates are in the same month, false otherwise
 */
export function isSameMonth (firstDate, secondDate = new Date()) {
  return (firstDate.getFullYear() === secondDate.getFullYear()) &&
    (firstDate.getMonth() === secondDate.getMonth())
}

/**
 * Checks if a date is after another (day-based comparison)
 *
 * @param   {Date}    date      - Date to check
 * @param   {Date}    reference - Reference date
 * @returns {boolean}           True if date is after reference, false otherwise
 */
export function isOver (date, reference = new Date()) {
  // not today and date before reference
  return !isSameDay(reference, date) && date < reference
}

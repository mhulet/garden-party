import randomString from './RandomString'

describe('randomString', () => {
  it('does not create the same string two times', () => {
    const string = randomString()
    const otherString = randomString()
    expect(string).not.toEqual(otherString)
  })

  it('generates strings of custom lengths', () => {
    const string = randomString(10)
    expect(string).toHaveLength(10)
  })
})

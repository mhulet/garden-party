const units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

/**
 * Returns a human readable size value
 *
 * @param   {number}  num         - The amount of bytes
 * @param   {number}  [precision] - Desired precision
 * @param   {boolean} [addSpace]  - Adds a space between number and units when true
 * @returns {string}              Something like "4.31 MB"
 */
export default function (num, precision = 3, addSpace = true) {
  if (Math.abs(num) < 1) return num + (addSpace ? ' ' : '') + units[0]
  const exponent = Math.min(Math.floor(Math.log10(num < 0 ? -num : num) / 3), units.length - 1)
  const n = Number(((num < 0 ? -num : num) / 1000 ** exponent).toPrecision(precision))
  return (num < 0 ? '-' : '') + n + (addSpace ? ' ' : '') + units[exponent]
};

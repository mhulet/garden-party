class LinkMailer < ApplicationMailer
  def notify_email
    @recipient = params[:recipient]
    @action    = params[:action]
    @link      = params[:link]

    mail subject: subject, to: @recipient.email
  end

  private

  def subject
    subject_prefix = "[#{Rails.configuration.garden_party.instance_name}][#{t('activerecord.models.link', count: 2)}]"
    subject = I18n.t 'link_mailer.notify_email.subject', author: @link.user.username, title: @link.title

    "#{subject_prefix} #{subject}"
  end
end

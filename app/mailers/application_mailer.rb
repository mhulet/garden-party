class ApplicationMailer < ActionMailer::Base
  default from: Rails.configuration.garden_party.default_email_options[:from]
  layout 'mailer'
end

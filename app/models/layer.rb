class Layer < ApplicationRecord
  validates :name, presence: true
  validates :position, with: :validate_position

  belongs_to :map
  has_many :patches, dependent: :restrict_with_exception
  has_many :paths, dependent: :destroy

  before_validation :set_position, on: :create
  after_save :update_positions

  private

  def validate_position
    max = map.layers.count
    max += 1 if new_record?
    range = (1..max)

    errors.add :position, I18n.t('activerecord.errors.is_out_of_range', start: 0, end: max) unless range.include? position
  end

  def set_position
    self.position ||= map.layers.count + 1
  end

  def update_positions
    return unless saved_change_to_position?

    previous = saved_change_to_position[0] || (map.layers.count + 1)
    new      = saved_change_to_position[1]
    return if previous == new

    if previous > new
      amount = +1
      comparator = '>='
    elsif previous < new
      amount = -1
      comparator = '<='
    end

    move_layers(from: new, amount: amount, comparator: comparator)
  end

  def move_layers(from:, amount:, comparator:)
    layers = map.layers.order(position: :asc)
    # Exclude current layer if already persisted
    layers = layers.where.not(id: id) if id
    index  = from + amount

    layers.where("position #{comparator} ?", from).find_each do |layer|
      layer.update_column :position, index # rubocop:disable Rails/SkipsModelValidations
      index += amount
    end
  end
end

class Link < ApplicationRecord
  validates :title, presence: true
  validates :url, presence: true
  validates :description, presence: true

  belongs_to :user
  belongs_to :approved_by, optional: true, class_name: 'User'

  has_paper_trail

  before_create :auto_approve
  after_create :notify_admins!

  scope :approved, -> { where.not(approved_by: nil) }
  scope :approved_or_owned, ->(user_id) { approved.or(where(user_id: user_id)) }

  def approved?
    !pending_approval?
  end

  def pending_approval?
    approved_by_id.nil?
  end

  private

  def auto_approve
    return unless user.admin?

    self.approved_by = user
  end

  def notify_admins!
    return if user.admin?

    User.admins.find_each do |admin|
      LinkMailer.with(recipient: admin, link: self).notify_email.deliver_later
    end
  end
end

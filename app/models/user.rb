class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :username, presence: true, uniqueness: true, format: { with: /\A[a-z0-9\-_]{3,}\z/i }

  has_many :maps, dependent: :destroy
  has_many :links, dependent: :restrict_with_exception

  scope :confirmed, -> { where.not(confirmed_at: nil) }

  def confirmed?
    confirmed_at.present?
  end

  scope :admins, -> { where(role: 'admin') }
  scope :users, -> { where(role: 'user') }

  def admin?
    role == 'admin'
  end

  def user?
    role == 'user'
  end
end

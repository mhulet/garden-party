class Element < ApplicationRecord
  validates :implanted_at, presence: true, if: :removed_at
  validates :implanted_at, presence: true, if: :removal_planned_for
  validate :valid_removal_plan_date?
  validate :valid_removal_date?

  belongs_to :resource
  belongs_to :patch
  has_many :activities, as: :subject, dependent: :destroy

  scope :still_implanted, -> { where(removed_at: nil).where.not(implanted_at: nil) }
  scope :removed, -> { where.not(removed_at: nil) }
  scope :planned, -> { where(implanted_at: nil) }

  def map_owner
    patch.map.user
  end

  def status
    return :removed if removed_at.present?
    return :implanted if implanted_at.present?

    :planned
  end

  def valid_removal_plan_date?
    return if removal_planned_for.blank? || implanted_at.blank?

    errors.add :removal_planned_for, I18n.t('activerecord.errors.element.date_before_implantation') if removal_planned_for < implanted_at
  end

  def valid_removal_date?
    return if removed_at.blank? || implanted_at.blank?

    errors.add :removed_at, I18n.t('activerecord.errors.element.date_before_implantation') if removed_at < implanted_at
  end
end

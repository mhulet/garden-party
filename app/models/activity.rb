class Activity < ApplicationRecord
  validates :name, presence: true
  validates :planned_for, presence: true, unless: :done_at
  validates :done_at, presence: true, unless: :planned_for
  validates :subject_type, inclusion: { in: %w[Element] }
  validate :pictures_format

  belongs_to :subject, polymorphic: true
  has_many_attached :pictures

  before_save :resize_pictures

  scope :for_map, lambda { |map_id|
    patch_ids   = Patch.where(map_id: map_id).pluck :id
    element_ids = Element.where(patch_id: patch_ids).pluck :id
    where subject_id: element_ids, subject_type: 'Element'
  }
  scope :for_element, ->(element_id) { where(subject_id: element_id, subject_type: 'Element') }

  private

  def pictures_format
    mimes_types = Rails.configuration.garden_party.activities[:allowed_pictures_mime_types]
    pictures.each do |picture|
      next if picture.persisted?

      errors.add(:picture, I18n.t('activerecord.errors.bad_image_format', formats: mimes_types.join(', '))) unless mimes_types.include? picture.blob.content_type
    end
  end

  def resize_pictures # rubocop:disable Metrics/AbcSize
    return unless pictures.count.positive?

    largest_side = Rails.configuration.garden_party.activities[:largest_pictures_size]
    pictures.each_index do |index|
      next if pictures[index].persisted?

      file = attachment_changes['pictures'].attachables[index]
      processed = ImageProcessing::MiniMagick.source(file.path)
                                             .resize_to_limit!(largest_side, largest_side)
      FileUtils.mv processed, file.path

      pictures[index].unfurl processed
    end
  end
end

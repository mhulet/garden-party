require 'garden_party/geometry'

class Map < ApplicationRecord
  validates :name, presence: true
  validates :extent_height, presence: true, if: :picture_map?
  validates :extent_width, presence: true, if: :picture_map?
  validate :center_format
  validate :picture_format

  belongs_to :user
  # Order matters as dependent entities are destroyed in this order
  has_many :patches, dependent: :destroy
  has_many :paths, dependent: :destroy
  has_many :layers, dependent: :destroy

  has_one_attached :picture

  before_save :cleanup_picture
  before_create :build_first_layer
  after_update :rescale_related

  scope :publicly_available, -> { where publicly_available: true }

  def picture_size
    return unless picture.attached?

    # ActiveStorages analyzes file and metadata in a job, so we don't have any
    # information unless it's done.
    # As a workaround, we force the analysis, possibly doing it twice with the job.
    picture.analyze unless picture.analyzed?

    [picture.metadata[:width], picture.metadata[:height]]
  end

  def picture=(value)
    # FIXME: Super effective but super ugly
    super value if value.is_a?(ActionDispatch::Http::UploadedFile) ||
                   value.is_a?(Rack::Test::UploadedFile)
  end

  # Re-define setter as bad values are fed to ActiveRecord::Point otherwise, raising
  # exception before a chance to validate. The drawback is that error message
  # won't be consistent ("empty value", instead of "incorrect value")
  #
  # FIXME: Use a better way to avoid early exceptions
  def center=(value)
    value = nil unless value.is_a?(Array) && value.size == 2
    value&.map!(&:to_f)
    super value
  end

  private

  def rescale_related
    return unless extent_width_previously_changed?

    scale = previous_changes['extent_width'][1] / previous_changes['extent_width'][0]
    [:patches, :paths].each do |collection|
      send(collection).find_each do |item|
        item.update geometry: GardenParty::Geometry.rescale(item.geometry, scale)
      end
    end
  end

  def picture_map?
    picture.attached?
  end

  def cleanup_picture
    # Undocumented Rails API, may change or break in future updates
    return unless attachment_changes['picture']

    path = attachment_changes['picture'].attachable.path

    optimizer = ImageOptim.new(svgo: { enable_plugins: %w[removeScriptElement removeStyleElement removeOffCanvasPaths removeRasterImages] })
    optimizer.optimize_image! path

    # Update the ActiveStorage::Attachment's checksum and other data.
    # Check ActiveStorage::Blob#unfurl
    picture.unfurl File.open path
  end

  def center_format
    errors.add(:center, I18n.t('activerecord.errors.bad_point_format')) unless center.is_a? ActiveRecord::Point
  end

  def picture_format
    return unless picture.attached?

    allowed_mime_types = Rails.configuration.garden_party[:maps][:allowed_picture_mime_types]

    errors.add(:picture, I18n.t('activerecord.errors.bad_image_format', formats: allowed_mime_types.join(', '))) unless allowed_mime_types.include? picture.blob.content_type
  end

  def build_first_layer
    self.layers = [Layer.new(name: I18n.t('models.layer.default_name'))] if layers.count.zero?
  end
end

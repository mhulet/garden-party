class Patch < GeometryRecord
  validates :geometry, feature: { types: %w[Circle Point Polygon] }
  validate :elements_presence

  belongs_to :map
  belongs_to :layer
  has_many :elements, dependent: :destroy

  private

  def elements_presence
    errors.add :elements, I18n.t('activerecord.errors.models.patch.elements.element_missing') if elements.size.zero?
    errors.add :elements, I18n.t('activerecord.errors.models.patch.elements.too_much_elements') if point? && elements.size > 1
  end
end

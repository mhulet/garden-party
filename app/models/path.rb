class Path < GeometryRecord
  validates :geometry, feature: { types: %w[Circle LineString Polygon] }
  validates :name, presence: true
  validates :stroke_width, presence: true
  validates :stroke_color, presence: true, alpha_color_string: true
  validates :background_color, presence: true, alpha_color_string: true
  validates :stroke_style_preset, presence: false, inclusion: { in: [nil, 'dotted', 'dashed', 'long_dashes', 'long_dashes2', 'long_dashes3', 'dashed2', 'dashed3'] }

  belongs_to :map
  belongs_to :layer
end

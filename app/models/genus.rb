class Genus < ApplicationRecord
  enum kingdom: { plant: 0, animal: 1 }

  validates :name, presence: true
  validates :description, presence: true

  has_many :resources, dependent: :restrict_with_exception

  has_paper_trail
end

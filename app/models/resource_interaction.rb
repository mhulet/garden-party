class ResourceInteraction < ApplicationRecord
  enum nature: { competition: 0, mutualism: 1, predation: 2, parasitism: 3, pathogens: 4, herbivory: 5 }

  validates :nature, presence: true
  validates :target, uniqueness: { scope: [:subject_id, :target_id] }

  belongs_to :subject, class_name: 'Resource'
  belongs_to :target, class_name: 'Resource'

  has_paper_trail

  scope :with_resource, ->(resource_id) { where(subject_id: resource_id).or(where(target_id: resource_id)) }
end

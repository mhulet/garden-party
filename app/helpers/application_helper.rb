module ApplicationHelper
  def owner?(entity, field: :user_id)
    entity[field] == current_user&.id
  end
end
